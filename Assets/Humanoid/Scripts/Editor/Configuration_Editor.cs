﻿using System.IO;
using UnityEditor;
using UnityEngine;
#if UNITY_2019_1_OR_NEWER
using System.Collections.Generic;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
#endif
#if !UNITY_2019_1_OR_NEWER
#endif
#if hPHOTON2
using Photon.Pun;
#endif

namespace Passer.Humanoid {

    [InitializeOnLoad]
    public class ConfigurationCheck {
        static ConfigurationCheck() {
            CheckXrSdks();

#if UNITY_2019_1_OR_NEWER
            RetrievePackageList();
#endif
        }

        public static void CheckXrSdks() {
            Configuration_Editor.FindHumanoidFolder();
            //string configurationString = EditorPrefs.GetString("HumanoidConfigurationKey", "DefaultConfiguration");

            Configuration configuration = Configuration_Editor.GetConfiguration();

            bool anyChanged = false;

#if (UNITY_STANDALONE_WIN || UNITY_ANDROID)
            bool oculusSupported = Oculus_Editor.OculusSupported();
            if (oculusSupported && !configuration.oculusSupport) {
                configuration.oculusSupport = true;
                anyChanged = true;
            }
#if !hOCULUS
            if (oculusSupported)
                anyChanged = true;
#endif
#endif

#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
            bool steamVrSupported = OpenVR_Editor.OpenVRSupported();
            if (steamVrSupported && !configuration.openVRSupport) {
                configuration.openVRSupport = true;
                anyChanged = true;
            }
#if !hOPENVR
            if (steamVrSupported)
                anyChanged = true;
#endif
#endif

#if (UNITY_2017_2_OR_NEWER && UNITY_WSA_10_0)
            bool windowsMrSupported = WindowsMR_Editor.MixedRealitySupported();
            if (windowsMrSupported && !configuration.windowsMRSupport) {
                configuration.windowsMRSupport = true;
                anyChanged = true;
            }
#if !hWINDOWSMR
            if (windowsMrSupported)
                anyChanged = true;
#endif
#endif

            if (anyChanged)
                Configuration_Editor.CheckExtensions(configuration);
        }

#if UNITY_2019_1_OR_NEWER
        protected static ListRequest request;
        public static List<string> packageNameList;

        public static void RetrievePackageList() {
            request = Client.List();    // List packages installed for the Project
            EditorApplication.update += Progress;
        }

        public static void Progress() {
            if (request.IsCompleted) {
                if (request.Status == StatusCode.Success) {
                    packageNameList = new List<string>();
                    foreach (UnityEditor.PackageManager.PackageInfo package in request.Result)
                        packageNameList.Add(package.name);
                }
                else if (request.Status >= StatusCode.Failure)
                    Debug.Log(request.Error.message);

                EditorApplication.update -= Progress;
            }
        }
#endif
    }

    [CustomEditor(typeof(Configuration))]
    public class Configuration_Editor : Editor {
        private Configuration configuration;

        private static string humanoidPath;

        private const string vivetrackerPath = "Extensions/ViveTrackers/ViveTracker.cs";
        private const string openVRPath = "Extensions/OpenVR/OpenVR.cs";
        private const string oculusPath = "Extensions/Oculus/Oculus.cs";
        private const string oculusHandPath = "Extensions/Oculus/OculusHandSkeleton.cs";
        private const string windowsMRPath = "Extensions/WindowsMR/WindowsMR.cs";
        private const string vrtkPath = "Extensions/VRTK/Vrtk.cs";
        private const string neuronPath = "Extensions/PerceptionNeuron/PerceptionNeuron.cs";
        private const string realsensePath = "Extensions/IntelRealsense/IntelRealsense.cs";
        private const string leapPath = "Extensions/LeapMotion/LeapMotion.cs";
        private const string kinect1Path = "Extensions/MicrosoftKinect1/MicrosoftKinect1.cs";
        private const string kinect2Path = "Extensions/MicrosoftKinect2/MicrosoftKinect2.cs";
        private const string astraPath = "Extensions/OrbbecAstra/OrbbecAstra.cs";
        private const string hydraPath = "Extensions/RazerHydra/RazerHydra.cs";
        private const string tobiiPath = "Extensions/Tobii/Tobii.cs";
        public const string arkitPath = "Extensions/Arkit/ArKit.cs";
        private const string pupilPath = "Extensions/Pupil/PupilTracker.cs";
        private const string optitrackPath = "Extensions/OptiTrack/OptiTrack.cs";

        private const string facePath = "FaceControl/EyeTarget.cs";

        public static Configuration CreateDefaultConfiguration() {
            Configuration configuration;

            Debug.Log("Created new Default Configuration");
            // Create new Default Configuration
            configuration = CreateInstance<Configuration>();
            configuration.oculusSupport = true;
            configuration.openVRSupport = true;
            configuration.windowsMRSupport = true;

            string path = humanoidPath.Substring(0, humanoidPath.Length - 1); // strip last /
            path = path.Substring(0, path.LastIndexOf('/') + 1); // strip Scripts;
            path = "Assets" + path + "DefaultConfiguration.asset";
            AssetDatabase.CreateAsset(configuration, path);
            AssetDatabase.SaveAssets();
            return configuration;
        }

        public static Configuration GetConfiguration() {
            string humanoidPath = FindHumanoidFolder();
            humanoidPath = humanoidPath.Substring(0, humanoidPath.Length - 1); // strip last /
            humanoidPath = humanoidPath.Substring(0, humanoidPath.LastIndexOf('/') + 1); // strip Scripts;

            string configurationString = EditorPrefs.GetString("HumanoidConfigurationKey", "DefaultConfiguration");
#if UNITY_2018_3_OR_NEWER
            var settings = AssetDatabase.LoadAssetAtPath<HumanoidSettings>("Assets" + humanoidPath + "HumanoidSettings.asset");
            if (settings == null) {
                settings = CreateInstance<HumanoidSettings>();

                AssetDatabase.CreateAsset(settings, HumanoidSettings.settingsPath);
                AssetDatabase.SaveAssets();
            }

            SerializedObject serializedSettings = new SerializedObject(settings);
            SerializedProperty configurationProp = serializedSettings.FindProperty("configuration");
            if (settings.configuration == null) {
#endif
            Configuration configuration = LoadConfiguration(configurationString);
            if (configuration == null) {
                configurationString = "DefaultConfiguration";
                LoadConfiguration(configurationString);
                if (configuration == null) {
                    Debug.Log("Created new Default Configuration");
                    // Create new Default Configuration
                    configuration = CreateInstance<Configuration>();
                    configuration.oculusSupport = true;
                    configuration.openVRSupport = true;
                    configuration.windowsMRSupport = true;
                    string path = "Assets" + humanoidPath + configurationString + ".asset";
                    AssetDatabase.CreateAsset(configuration, path);
                    AssetDatabase.SaveAssets();
                }
            }
#if UNITY_2018_3_OR_NEWER
                configurationProp.objectReferenceValue = configuration;
            }
            serializedSettings.ApplyModifiedProperties();
            return (Configuration)configurationProp.objectReferenceValue;
#else

            return configuration;
#endif
        }

        #region Enable 
        public void OnEnable() {
            configuration = (Configuration)target;

            humanoidPath = FindHumanoidFolder();
        }

        public static string FindHumanoidFolder() {
            // Path is correct
            if (IsFileAvailable("HumanoidControl.cs"))
                return humanoidPath;

            // Determine in which (sub)folder HUmanoid Control has been placed
            // This makes it possible to place Humanoid Control is a different folder
            string[] hcScripts = AssetDatabase.FindAssets("HumanoidControl");
            for (int i = 0; i < hcScripts.Length; i++) {
                string assetPath = AssetDatabase.GUIDToAssetPath(hcScripts[i]);
                if (assetPath.Length > 36 && assetPath.Substring(assetPath.Length - 27, 27) == "/Scripts/HumanoidControl.cs") {
                    humanoidPath = assetPath.Substring(6, assetPath.Length - 24);
                    return humanoidPath;
                }
            }

            // Defaulting to standard folder
            humanoidPath = "/Humanoid/Scripts/";
            return humanoidPath;
        }
        #endregion

        public override void OnInspectorGUI() {
            serializedObject.Update();

            bool anyChanged = ConfigurationGUI(serializedObject);
            if (GUI.changed || anyChanged) {
                EditorUtility.SetDirty(configuration);
            }

            serializedObject.ApplyModifiedProperties();
        }

        public static bool ConfigurationGUI(SerializedObject serializedConfiguration) {
            bool anyChanged = false;

            anyChanged |= SteamVRSettingUI(serializedConfiguration);
            anyChanged |= OculusSettingUI(serializedConfiguration);
            anyChanged |= WindowsMRSettingUI(serializedConfiguration);
            anyChanged |= VrtkSettingUI(serializedConfiguration);

            anyChanged |= LeapSettingUI(serializedConfiguration);
            anyChanged |= Kinect1SettingUI(serializedConfiguration);
            anyChanged |= Kinect2SettingUI(serializedConfiguration);
            anyChanged |= AstraSettingUI(serializedConfiguration);

            anyChanged |= RealsenseSettingUI(serializedConfiguration);
            anyChanged |= HydraSettingUI(serializedConfiguration);
            anyChanged |= TobiiSettingUI(serializedConfiguration);
            anyChanged |= ArkitSettingUI(serializedConfiguration);
            anyChanged |= PupilSettingUI(serializedConfiguration);
            anyChanged |= NeuronSettingUI(serializedConfiguration);
            anyChanged |= OptitrackSettingUI(serializedConfiguration);

            anyChanged |= NetworkingSettingUI(serializedConfiguration);
            return anyChanged;
        }

        #region SettingUI

        public static bool SteamVRSettingUI(SerializedObject serializedConfiguration) {
            bool anyChanged = false;

            SerializedProperty openVRSupportProp = serializedConfiguration.FindProperty("openVRSupport");

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
            bool steamVrSupported = OpenVR_Editor.OpenVRSupported();
            if (steamVrSupported) {
                openVRSupportProp.boolValue = isOpenVrSupportAvailable();
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.Toggle("SteamVR Support", openVRSupportProp.boolValue);
                EditorGUI.EndDisabledGroup();
            }
            else if (!isOpenVrSupportAvailable())
                openVRSupportProp.boolValue = false;
            else
                openVRSupportProp.boolValue = EditorGUILayout.Toggle("SteamVR Support", openVRSupportProp.boolValue);

            ViveTrackerSettingUI(serializedConfiguration);
#else
            SerializedProperty viveTrackerSupportProp = serializedConfiguration.FindProperty("viveTrackerSupport");
            if (openVRSupportProp.boolValue | viveTrackerSupportProp.boolValue)
                anyChanged = true;
            openVRSupportProp.boolValue = false;
            viveTrackerSupportProp.boolValue = false;
#endif
            return anyChanged;
        }

        public static bool ViveTrackerSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty openVRSupportProp = serializedConfiguration.FindProperty("openVRSupport");
            SerializedProperty viveTrackerSupportProp = serializedConfiguration.FindProperty("viveTrackerSupport");

            EditorGUI.BeginDisabledGroup(openVRSupportProp.boolValue == false);
            viveTrackerSupportProp.boolValue = isViveTrackerSupportAvailable() && EditorGUILayout.Toggle("Vive Tracker Support", viveTrackerSupportProp.boolValue);
            EditorGUI.EndDisabledGroup();
            return false;
        }

        public static bool OculusSettingUI(SerializedObject serializedConfiguration) {
            bool anyChanged = false;

            SerializedProperty oculusSupportProp = serializedConfiguration.FindProperty("oculusSupport");

#if UNITY_STANDALONE_WIN || UNITY_ANDROID
            bool oculusSupported = Oculus_Editor.OculusSupported();
            if (oculusSupported) {
                oculusSupportProp.boolValue = isOculusSupportAvailable();
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.Toggle("Oculus Support", oculusSupportProp.boolValue);
                EditorGUI.EndDisabledGroup();
            }
            else if (!isOculusSupportAvailable())
                oculusSupportProp.boolValue = false;
            else
                oculusSupportProp.boolValue = EditorGUILayout.Toggle("Oculus Support", oculusSupportProp.boolValue);
#else
            if (oculusSupportProp.boolValue)
                anyChanged = true;
            oculusSupportProp.boolValue = false;
#endif
            return anyChanged;
        }

        public static bool WindowsMRSettingUI(SerializedObject serializedConfiguration) {
            bool anyChanged = false;

            SerializedProperty windowsMRSupportProp = serializedConfiguration.FindProperty("windowsMRSupport");

#if UNITY_WSA_10_0
            bool windowsMrSupported = WindowsMR_Editor.MixedRealitySupported();
            if (windowsMrSupported) {
                windowsMRSupportProp.boolValue = isWindowsMrAvailable();
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.Toggle("Windows MR Support", windowsMRSupportProp.boolValue);
                EditorGUI.EndDisabledGroup();
            }
            else if (!isWindowsMrAvailable())
                windowsMRSupportProp.boolValue = false;
            else
                windowsMRSupportProp.boolValue = EditorGUILayout.Toggle("Windows MR Support", windowsMRSupportProp.boolValue);
#else
            if (windowsMRSupportProp.boolValue)
                anyChanged = true;
            windowsMRSupportProp.boolValue = false;
#endif
            return anyChanged;
        }

        public static bool VrtkSettingUI(SerializedObject serializedConfiguration) {

            SerializedProperty vrtkSupportProp = serializedConfiguration.FindProperty("vrtkSupport");

            if (!isVrtkSupportAvailable())
                vrtkSupportProp.boolValue = false;

            else if (!isVrtkAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.Toggle("VRTK Supoort", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("VRTK is not available. Please download it from the Asset Store.", MessageType.Warning, true);
            }
            else
                vrtkSupportProp.boolValue = EditorGUILayout.Toggle("VRTK Support", vrtkSupportProp.boolValue);
            return false;
        }

        public static bool LeapSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty leapSupportProp = serializedConfiguration.FindProperty("leapSupport");

            bool oldLeapSupport = leapSupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isLeapSupportAvailable())
                leapSupportProp.boolValue = false;

            else if (!isLeapAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                leapSupportProp.boolValue = EditorGUILayout.Toggle("Leap Motion Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Leap Motion Core Assets are not available. Please download the Core Assets using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Leap Motion Unity Core Assets"))
                    Application.OpenURL("https://developer.leapmotion.com/unity");
            }

            else
                leapSupportProp.boolValue = EditorGUILayout.Toggle("Leap Motion Support", leapSupportProp.boolValue);
#else
            leapSupportProp.boolValue = false;
#endif
            return (leapSupportProp.boolValue != oldLeapSupport);
        }

        public static bool Kinect1SettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty kinect1SupportProp = serializedConfiguration.FindProperty("kinect1Support");

            bool oldKinectSupport = kinect1SupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isKinect1SupportAvailable())
                kinect1SupportProp.boolValue = false;
            else
                kinect1SupportProp.boolValue = EditorGUILayout.Toggle("Kinect 1 Support", kinect1SupportProp.boolValue);
#else
            kinect1SupportProp.boolValue = false;
#endif
            return (kinect1SupportProp.boolValue != oldKinectSupport);
        }

        public static bool Kinect2SettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty kinect2SupportProp = serializedConfiguration.FindProperty("kinectSupport");

            bool oldKinectSupport = kinect2SupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isKinect2SupportAvailable())
                kinect2SupportProp.boolValue = false;
            else
                kinect2SupportProp.boolValue = EditorGUILayout.Toggle("Kinect 2 Support", kinect2SupportProp.boolValue);
#else
            kinect2SupportProp.boolValue = false;
#endif
            return (kinect2SupportProp.boolValue != oldKinectSupport);
        }

        public static bool AstraSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty astraSupportProp = serializedConfiguration.FindProperty("astraSupport");

            bool oldAstraSupport = astraSupportProp.boolValue;
#if UNITY_STANDALONE_WIN
            if (!isAstraSupportAvailable())
                astraSupportProp.boolValue = false;

            else if (!isAstraAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                astraSupportProp.boolValue = EditorGUILayout.Toggle("Orbbec Astra Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Astra SDK is not available. Please download the Astra Unity SDK using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Orbbec Astra SDK"))
                    Application.OpenURL("https://orbbec3d.com/develop/");
            }
            else
                astraSupportProp.boolValue = EditorGUILayout.Toggle("Orbbec Astra Support", astraSupportProp.boolValue);
#else
            astraSupportProp.boolValue = false;
#endif
            return (astraSupportProp.boolValue != oldAstraSupport);
        }

        public static bool RealsenseSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty realsenseSupportProp = serializedConfiguration.FindProperty("realsenseSupport");

            bool oldRealsenseSupport = realsenseSupportProp.boolValue;
#if UNITY_STANDALONE_WIN
            if (!isRealsenseSupportAvailable())
                realsenseSupportProp.boolValue = false;
            else
                realsenseSupportProp.boolValue = EditorGUILayout.Toggle("Intel RealSense Support", realsenseSupportProp.boolValue);
#else
            realsenseSupportProp.boolValue = false;
#endif
            return (realsenseSupportProp.boolValue != oldRealsenseSupport);
        }

        public static bool HydraSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty hydraSupportProp = serializedConfiguration.FindProperty("hydraSupport");

            bool oldHydraSupport = hydraSupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isHydraSupportAvailable())
                hydraSupportProp.boolValue = false;
            else
                hydraSupportProp.boolValue = EditorGUILayout.Toggle("Hydra Support", hydraSupportProp.boolValue);
#else
            hydraSupportProp.boolValue = false;
#endif
            return (hydraSupportProp.boolValue != oldHydraSupport);
        }

        public static bool TobiiSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty tobiiSupportProp = serializedConfiguration.FindProperty("tobiiSupport");

            bool oldTobiiSupport = tobiiSupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isFaceSupportAvailable() || !isTobiiSupportAvailable())
                tobiiSupportProp.boolValue = false;

            else if (!isTobiiAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                tobiiSupportProp.boolValue = EditorGUILayout.Toggle("Tobii Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Tobii Framework is not available. Please download the Tobii Unity SDK using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Tobii Unity SDK"))
                    Application.OpenURL("http://developer.tobii.com/tobii-unity-sdk/");
            }
            else
                tobiiSupportProp.boolValue = EditorGUILayout.Toggle("Tobii Support", tobiiSupportProp.boolValue);
#else
            tobiiSupportProp.boolValue = false;
#endif
            return (tobiiSupportProp.boolValue != oldTobiiSupport);
        }

        public static bool ArkitSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty arkitSupportProp = serializedConfiguration.FindProperty("arkitSupport");

            bool oldArkitSupport = arkitSupportProp.boolValue;
#if UNITY_IOS && UNITY_2019_1_OR_NEWER
            if (!isFaceSupportAvailable() || !isArkitSupportAvailable())
                arkitSupportProp.boolValue = false;
            else if (!IsArkitAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                arkitSupportProp.boolValue = EditorGUILayout.Toggle("ArKit Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox(
                    "Required packages are not installed. " +
                    "Please open the Unity Package Manager and install 'AR Foundation', 'ARKit Face Tracking' and 'XR Legacy Input Helpers'.",
                    MessageType.Warning, true);
                if (GUILayout.Button("Open Package Manager"))
                    EditorApplication.ExecuteMenuItem("Window/Package Manager");
            }
            else
                arkitSupportProp.boolValue = EditorGUILayout.Toggle("ArKit Support", arkitSupportProp.boolValue);
#else
            arkitSupportProp.boolValue = false;
#endif
            return (arkitSupportProp.boolValue != oldArkitSupport);
        }

        public static bool PupilSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty pupilSupportProp = serializedConfiguration.FindProperty("pupilSupport");

            bool oldPupilSupport = pupilSupportProp.boolValue;
#if UNITY_STANDALONE_WIN
            if (!isPupilSupportAvailable())
                pupilSupportProp.boolValue = false;

            else if (!isPupilAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                pupilSupportProp.boolValue = EditorGUILayout.Toggle("Pupil Labs Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Pupil Labs plugin is not available. Please download the plugin using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Pupil Unity Plugin"))
                    Application.OpenURL("https://github.com/pupil-labs/hmd-eyes/releases/tag/v0.5.1");
            }
            else
                pupilSupportProp.boolValue = EditorGUILayout.Toggle("Pupil Labs Support", pupilSupportProp.boolValue);
#else
            pupilSupportProp.boolValue = false;
#endif
            return (pupilSupportProp.boolValue != oldPupilSupport);
        }

        public static bool NeuronSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty neuronSupportProp = serializedConfiguration.FindProperty("neuronSupport");

            bool oldNeuronSupport = neuronSupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isNeuronSupportAvailable())
                neuronSupportProp.boolValue = false;

            else
                neuronSupportProp.boolValue = EditorGUILayout.Toggle("Perception Neuron Support", neuronSupportProp.boolValue);
#else
            neuronSupportProp.boolValue = false;
#endif
            return (neuronSupportProp.boolValue != oldNeuronSupport);
        }

        public static bool OptitrackSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty optitrackSupportProp = serializedConfiguration.FindProperty("optitrackSupport");

            bool oldOptitrackSupport = optitrackSupportProp.boolValue;
#if UNITY_STANDALONE_WIN || UNITY_WSA_10_0
            if (!isOptitrackSupportAvailable())
                optitrackSupportProp.boolValue = false;

            else if (!isOptitrackAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                optitrackSupportProp.boolValue = EditorGUILayout.Toggle("OptiTrack Support", false);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("OptiTrack Unity plugin is not available. Please download the plugin using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download OptiTrack Unity Plugin"))
                    Application.OpenURL("https://optitrack.com/downloads/plugins.html#unity-plugin");
            }
            else
                optitrackSupportProp.boolValue = EditorGUILayout.Toggle("OptiTrack Support", optitrackSupportProp.boolValue);
#else
            optitrackSupportProp.boolValue = false;
#endif
            return (optitrackSupportProp.boolValue != oldOptitrackSupport);
        }

        private static bool NetworkingSettingUI(SerializedObject serializedConfiguration) {
            SerializedProperty networkingSupportProp = serializedConfiguration.FindProperty("networkingSupport");
            int oldNetworkingSupport = networkingSupportProp.intValue;
            networkingSupportProp.intValue = (int)(NetworkingSystems)EditorGUILayout.EnumPopup("Networking Support", (NetworkingSystems)networkingSupportProp.intValue);
            return (oldNetworkingSupport != networkingSupportProp.intValue);
        }

        #endregion

        private static bool IsFileAvailable(string filePath) {
            string path = Application.dataPath + humanoidPath + filePath;
            bool fileAvailable = File.Exists(path);
            return fileAvailable;
        }

        #region Configurations

        public static bool NeuronConfiguration(bool neuronSupport) {
            return EditorGUILayout.Toggle("Perception Neuron Support", neuronSupport);
        }

        public static bool LeapConfiguration(bool leapSupport) {
            if (!isLeapAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                leapSupport = false;
                leapSupport = EditorGUILayout.Toggle("Leap Motion Support", leapSupport);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Leap Motion Core Assets are not available. Please download the Core Assets using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Leap Motion Unity Core Assets"))
                    Application.OpenURL("https://developer.leapmotion.com/unity");
            }
            else
                leapSupport = EditorGUILayout.Toggle("Leap Motion Support", leapSupport);
            return leapSupport;
        }

        public static bool Kinect1Configuration(bool kinect1Support) {
            return EditorGUILayout.Toggle("Kinect 1 Support", kinect1Support);
        }

        public static bool KinectConfiguration(bool kinectSupport) {
            return EditorGUILayout.Toggle("Kinect 2 Support", kinectSupport);
        }

        public static bool AstraConfiguration(bool astraSupport) {
            if (!isAstraAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                astraSupport = false;
                astraSupport = EditorGUILayout.Toggle("Orbbec Astra Support", astraSupport);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Astra SDK is not available. Please download the Astra Unity SDK using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Orbbec Astra SDK"))
                    Application.OpenURL("https://orbbec3d.com/develop/");
            }
            else
                astraSupport = EditorGUILayout.Toggle("Orbbec Astra Support", astraSupport);
            return astraSupport;
        }

        public static bool HydraConfiguration(bool hydraSupport) {
            return EditorGUILayout.Toggle("Hydra Support", hydraSupport);
        }
        public static bool TobiiConfiguration(bool tobiiSupport) {
            if (!isTobiiAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                tobiiSupport = false;
                tobiiSupport = EditorGUILayout.Toggle("Tobii Support", tobiiSupport);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("Tobii Framework is not available. Please download the Tobii Unity SDK using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download Tobii Unity SDK"))
                    Application.OpenURL("http://developer.tobii.com/tobii-unity-sdk/");
            }
            else if (IsFileAvailable(facePath)) //(isFaceTrackingAvailable())
                tobiiSupport = EditorGUILayout.Toggle("Tobii Support", tobiiSupport);
            return tobiiSupport;
        }

        public static bool OptitrackConfiguration(bool optitrackSupport) {
            if (!isOptitrackAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                optitrackSupport = false;
                optitrackSupport = EditorGUILayout.Toggle("OptiTrack Support", optitrackSupport);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("OptiTrack Unity plugin is not available. Please download the plugin using the button below and import them into this project.", MessageType.Warning, true);
                if (GUILayout.Button("Download OptiTrack Unity Plugin"))
                    Application.OpenURL("https://optitrack.com/downloads/plugins.html#unity-plugin");
            }
            else
                optitrackSupport = EditorGUILayout.Toggle("OptiTrack Support", optitrackSupport);
            return optitrackSupport;
        }

        public static bool PupilConfiguration(bool pupilSupport) {
            if (!isPupilAvailable()) {
                pupilSupport = false;
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Pupil Labs Support", GUILayout.Width(197));
                if (GUILayout.Button("Download Unity Plugin"))
                    Application.OpenURL("https://github.com/pupil-labs/hmd-eyes/releases/tag/v0.5.1");
                EditorGUILayout.EndHorizontal();
            }
            else
                pupilSupport = EditorGUILayout.Toggle("Pupil Labs Support", pupilSupport);
            return pupilSupport;
        }

        public static bool VrtkConfiguration(bool vrtkSupport) {
            if (!isVrtkAvailable()) {
                EditorGUI.BeginDisabledGroup(true);
                vrtkSupport = false;
                EditorGUILayout.Toggle("VRTK Supoort", vrtkSupport);
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.HelpBox("VRTK is not available. Please download it from the Asset Store.", MessageType.Warning, true);
            }
            else
                vrtkSupport = EditorGUILayout.Toggle("VRTK Support", vrtkSupport);
            return vrtkSupport;
        }

        #endregion

        #region Availability

        //private static bool isSteamVrSupportAvailable() {
        //    return IsFileAvailable(steamVRPath);
        //}

        private static bool isOpenVrSupportAvailable() {
            return IsFileAvailable(openVRPath);
        }

        private static bool isViveTrackerSupportAvailable() {
            return IsFileAvailable(vivetrackerPath);
        }

        private static bool isOculusSupportAvailable() {
            return IsFileAvailable(oculusPath);
        }

        private static bool isWindowsMrAvailable() {
            return IsFileAvailable(windowsMRPath);
        }

        private static bool isVrtkSupportAvailable() {
            return IsFileAvailable(vrtkPath);
        }

        private static bool isVrtkAvailable() {
            string path1 = Application.dataPath + "/VRTK/Scripts/Utilities/SDK/VRTK_SDKManager.cs"; // v3.2.0
            string path2 = Application.dataPath + "/VRTK/Source/Scripts/Utilities/SDK/VRTK_SDKManager.cs"; // v3.3.0
            return File.Exists(path1) || File.Exists(path2);
        }

        private static bool isLeapAvailable() {
            // Location for the Leap Core Assets < v4.2
            string path1 = Application.dataPath + "/Plugins/x86/LeapC.dll";
            string path2 = Application.dataPath + "/Plugins/x86_64/LeapC.dll";
            if (File.Exists(path1) || File.Exists(path2))
                return true;

            // Location for Leap Core Assets >= v4.2
            path1 = Application.dataPath + "/LeapMotion/Core/Plugins/x86/LeapC.dll";
            path2 = Application.dataPath + "/LeapMotion/Core/Plugins/x86_64/LeapC.dll";
            return (File.Exists(path1) || File.Exists(path2));
        }

        private static bool isLeapSupportAvailable() {
            return IsFileAvailable(leapPath);
        }

        private static bool isKinect1SupportAvailable() {
            return IsFileAvailable(kinect1Path);
        }

        private static bool isKinect2SupportAvailable() {
            return IsFileAvailable(kinect2Path);
        }

        public static bool isAstraSupportAvailable() {
            return IsFileAvailable(astraPath);
        }

        public static bool isAstraAvailable() {
            string path1 = Application.dataPath + "/Plugins/x86_64/astra.dll";
            return File.Exists(path1);
        }

        public static bool isRealsenseSupportAvailable() {
            return IsFileAvailable(realsensePath);
        }

        private static bool isHydraSupportAvailable() {
            return IsFileAvailable(hydraPath);
        }

        private static bool isTobiiSupportAvailable() {
            return IsFileAvailable(tobiiPath);
        }

        private static bool isTobiiAvailable() {
            string path1 = Application.dataPath + "/Tobii/Framework/TobiiAPI.cs";
            string path2 = Application.dataPath + "/Tobii/Plugins/x64/Tobii.GameIntegration.dll";
            return File.Exists(path1) && File.Exists(path2);
        }

        public static bool isArkitSupportAvailable() {
            return IsFileAvailable(arkitPath);
        }

        public static bool IsArkitAvailable() {
#if UNITY_IOS && UNITY_2019_1_OR_NEWER
            if (ConfigurationCheck.packageNameList == null)
                return false;

            bool arFoundationAvailable = ConfigurationCheck.packageNameList.Contains("com.unity.xr.arfoundation");
            bool arKitFaceTrackingAvailable = ConfigurationCheck.packageNameList.Contains("com.unity.xr.arkit-face-tracking");
            bool xrLegacyInputHelpersAvailable = ConfigurationCheck.packageNameList.Contains("com.unity.xr.legacyinputhelpers");
            return arFoundationAvailable && arKitFaceTrackingAvailable && xrLegacyInputHelpersAvailable;
#else
            return false;
#endif
        }

        private static bool isPupilSupportAvailable() {
            return IsFileAvailable(pupilPath);
        }

        private static bool isPupilAvailable() {
            string path1 = Application.dataPath + "/pupil_plugin/Scripts/Networking/PupilTools.cs";
            string path2 = Application.dataPath + "/pupil_plugin/Plugins/x86_64/NetMQ.dll";
            return File.Exists(path1) && File.Exists(path2);
        }

        public static bool isNeuronSupportAvailable() {
            return IsFileAvailable(neuronPath);
        }

        private static bool isOptitrackSupportAvailable() {
            return Configuration_Editor.IsFileAvailable(optitrackPath);
        }

        private static bool isOptitrackAvailable() {
            string path1 = Application.dataPath + "/OptiTrack/Scripts/OptitrackStreamingClient.cs";
            string path2 = Application.dataPath + "/OptiTrack/Plugins/x86_64/NatNetLib.dll";
            return File.Exists(path1) && File.Exists(path2);
        }

        private static bool isPhotonAvailable() {
            string path = Application.dataPath + "/Plugins/Photon3Unity3D.dll";
            return File.Exists(path);
        }

        private static bool isPhoton2Available() {
            string path = Application.dataPath + "/Photon/PhotonUnityNetworking/Code/PunClasses.cs";
            return File.Exists(path);
        }

        public static bool isFaceSupportAvailable() {
            return IsFileAvailable(facePath);
        }

        #endregion

        #region Extension Checks   

        public static void CheckExtensions(Configuration configuration) {
            configuration.openVRSupport = CheckExtensionOpenVR(configuration);
            configuration.viveTrackerSupport = CheckExtensionViveTracker(configuration);
            configuration.oculusSupport = CheckExtensionOculus(configuration);
            configuration.windowsMRSupport = CheckExtensionWindowsMR(configuration);
            configuration.vrtkSupport = CheckExtensionVRTK(configuration);
            configuration.neuronSupport = CheckExtensionNeuron(configuration);
            configuration.realsenseSupport = CheckExtensionRealsense(configuration);
            configuration.leapSupport = CheckExtensionLeap(configuration);
            configuration.kinect1Support = CheckExtensionKinect1(configuration);
            configuration.kinectSupport = CheckExtensionKinect2(configuration);
            configuration.astraSupport = CheckExtensionAstra(configuration);
            configuration.hydraSupport = CheckExtensionHydra(configuration);
            configuration.tobiiSupport = CheckExtensionTobii(configuration);
            configuration.arkitSupport = CheckExtensionArkit(configuration);
            configuration.pupilSupport = CheckExtensionPupil(configuration);
            configuration.optitrackSupport = CheckExtensionOptitrack(configuration);

            CheckExtensionNetworking(configuration);
            CheckFaceTracking(configuration);
        }

        public static bool CheckExtensionOpenVR(Configuration configuration) {
            bool enabled = configuration.openVRSupport;
            return CheckExtension(enabled, openVRPath, "hOPENVR");
        }

        public static bool CheckExtensionViveTracker(Configuration configuration) {
            bool enabled = configuration.viveTrackerSupport;
            return CheckExtension(enabled, vivetrackerPath, "hVIVETRACKER");
        }

        public static bool CheckExtensionOculus(Configuration configuration) {
            bool enabled = configuration.oculusSupport;
            CheckExtension(true, oculusHandPath, "hOCHAND");
            return CheckExtension(enabled, oculusPath, "hOCULUS");
        }

        public static bool CheckExtensionWindowsMR(Configuration configuration) {
            bool enabled = configuration.windowsMRSupport;
            return CheckExtension(enabled, windowsMRPath, "hWINDOWSMR");
        }

        public static bool CheckExtensionVRTK(Configuration configuration) {
            bool enabled = configuration.vrtkSupport && isVrtkAvailable();
            return CheckExtension(enabled, vrtkPath, "hVRTK");
        }

        public static bool CheckExtensionNeuron(Configuration configuration) {
            bool enabled = configuration.neuronSupport;
            return CheckExtension(enabled, neuronPath, "hNEURON");
        }

        public static bool CheckExtensionRealsense(Configuration configuration) {
            bool enabled = configuration.realsenseSupport;
            return CheckExtension(enabled, realsensePath, "hREALSENSE");
        }

        public static bool CheckExtensionLeap(Configuration configuration) {
            bool enabled = configuration.leapSupport && isLeapAvailable();
            return CheckExtension(enabled, leapPath, "hLEAP");
        }

        public static bool CheckExtensionKinect1(Configuration configuration) {
            bool enabled = configuration.kinect1Support;
            return CheckExtension(enabled, kinect1Path, "hKINECT1");
        }

        public static bool CheckExtensionKinect2(Configuration configuration) {
            bool enabled = configuration.kinectSupport;
            return CheckExtension(enabled, kinect2Path, "hKINECT2");
        }

        public static bool CheckExtensionAstra(Configuration configuration) {
            bool enabled = configuration.astraSupport && isAstraAvailable();
            return CheckExtension(enabled, astraPath, "hORBBEC");
        }

        public static bool CheckExtensionHydra(Configuration configuration) {
            bool enabled = configuration.hydraSupport;
            return CheckExtension(enabled, hydraPath, "hHYDRA");
        }

        public static bool CheckExtensionTobii(Configuration configuration) {
            bool enabled = configuration.tobiiSupport && isTobiiAvailable();
            return CheckExtension(enabled, tobiiPath, "hTOBII");
        }

        public static bool CheckExtensionArkit(Configuration configuration) {
            bool enabled = configuration.arkitSupport && IsArkitAvailable();
            return CheckExtension(enabled, arkitPath, "hARKIT");
        }

        public static bool CheckExtensionPupil(Configuration configuration) {
            bool enabled = configuration.pupilSupport && isPupilAvailable();
            return CheckExtension(enabled, pupilPath, "hPUPIL");
        }


        public static bool CheckExtensionOptitrack(Configuration configuration) {
            bool enabled = configuration.optitrackSupport && isOptitrackAvailable();
            return CheckExtension(enabled, optitrackPath, "hOPTITRACK");
        }

        private static void CheckExtensionNetworking(Configuration configuration) {
            if (isPhoton2Available()) {
                GlobalDefine("hPHOTON2");
                GlobalUndefine("hPHOTON1");
            }
            else if (isPhotonAvailable()) {
                GlobalDefine("hPHOTON1");
                GlobalUndefine("hPHOTON2");
            }
            else {
                GlobalUndefine("hPHOTON1");
                GlobalUndefine("hPHOTON2");
            }

            if (HumanoidMirror.IsAvailable())
                GlobalDefine("hMIRROR");
            else
                GlobalUndefine("hMIRROR");

            if (HumanoidBolt.IsAvailable())
                GlobalDefine("hBOLT");
            else
                GlobalUndefine("hBOLT");


#if !UNITY_2019_1_OR_NEWER
            if (configuration.networkingSupport == NetworkingSystems.UnityNetworking)
                GlobalDefine("hNW_UNET");
            else
                GlobalUndefine("hNW_UNET");
#endif
#if hPHOTON1 || hPHOTON2
            if (configuration.networkingSupport == NetworkingSystems.PhotonNetworking)
                GlobalDefine("hNW_PHOTON");
            else
                GlobalUndefine("hNW_PHOTON");
#endif
#if hMIRROR
            if (configuration.networkingSupport == NetworkingSystems.MirrorNetworking)
                GlobalDefine("hNW_MIRROR");
            else
                GlobalUndefine("hNW_MIRROR");
#endif
#if hBOLT
            if (configuration.networkingSupport == NetworkingSystems.PhotonBolt)
                GlobalDefine("hNW_BOLT");
            else
                GlobalUndefine("hNW_BOLT");
#endif
        }
        private static void CheckFaceTracking(Configuration configuration) {
            if (IsFileAvailable(facePath)) {
                GlobalDefine("hFACE");
            }
            else {
                GlobalUndefine("hFACE");
            }
        }

        public static bool CheckExtension(bool enabled, string filePath, string define) {
            if (enabled) {
                if (IsFileAvailable(filePath)) {
                    GlobalDefine(define);
                    return true;
                }
                else {
                    GlobalUndefine(define);
                    return false;
                }

            }
            else {
                GlobalUndefine(define);
                return false;
            }
        }

        public static bool CheckExtension(string filePath, string define) {
            if (IsFileAvailable(filePath)) {
                GlobalDefine(define);
                return true;
            }
            else {
                GlobalUndefine(define);
                return false;
            }
        }

        public static void GlobalDefine(string name) {
            //Debug.Log("Define " + name);
            string scriptDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            if (!scriptDefines.Contains(name)) {
                string newScriptDefines = scriptDefines + " " + name;
                if (EditorUserBuildSettings.selectedBuildTargetGroup != 0)
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newScriptDefines);
            }
        }

        public static void GlobalUndefine(string name) {
            //Debug.Log("Undefine " + name);
            string scriptDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            if (scriptDefines.Contains(name)) {
                int playMakerIndex = scriptDefines.IndexOf(name);
                string newScriptDefines = scriptDefines.Remove(playMakerIndex, name.Length);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newScriptDefines);
            }

        }

        public static Configuration LoadConfiguration(string configurationName) {
            string[] foundAssets = AssetDatabase.FindAssets(configurationName + " t:Configuration");
            if (foundAssets.Length == 0)
                return null;

            string path = AssetDatabase.GUIDToAssetPath(foundAssets[0]);
            Configuration configuration = AssetDatabase.LoadAssetAtPath<Configuration>(path);
            return configuration;
        }

        #endregion
    }

    public static class CustomAssetUtility {
        public static void CreateAsset<T>() where T : ScriptableObject {
            T asset = ScriptableObject.CreateInstance<T>();

            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "") {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "") {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            string assetTypeName = typeof(T).ToString();
            int dotIndex = assetTypeName.LastIndexOf('.');
            if (dotIndex >= 0)
                assetTypeName = assetTypeName.Substring(dotIndex + 1); // leave just text behind '.'
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + assetTypeName + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
    }

}