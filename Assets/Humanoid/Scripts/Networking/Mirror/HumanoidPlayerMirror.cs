﻿using System.IO;
using UnityEngine;
#if hNW_MIRROR
using System.Collections.Generic;
using Mirror;
#endif

namespace Passer.Humanoid {

    public static class HumanoidMirror {
        public static bool IsAvailable() {
            string path = Application.dataPath + "/Mirror/Runtime/NetworkBehaviour.cs";
            return File.Exists(path);
        }
    }

#if hNW_MIRROR
    [RequireComponent(typeof(NetworkIdentity))]
    public partial class HumanoidPlayer : PawnMirror, IHumanoidNetworking {

        public ulong nwId {
            get { return identity.netId; }
        }

        public List<HumanoidControl> humanoids { get; set; } = new List<HumanoidControl>();

        protected NetworkIdentity identity;

        public ulong GetObjectIdentity(GameObject obj) {
            NetworkIdentity identity = obj.GetComponent<NetworkIdentity>();
            if (identity == null)
                return 0;

            return identity.netId;
        }

        public GameObject GetGameObject(ulong objIdentity) {
            NetworkIdentity identity = NetworkIdentity.spawned[(uint)objIdentity];
            if (identity == null)
                return null;

            return identity.gameObject;
        }

    #region Init

        override public void Awake() {
            DontDestroyOnLoad(this);

            identity = GetComponent<NetworkIdentity>();
        }

        public override void OnStartServer() {
            NetworkServer.RegisterHandler<HumanoidNetworking.InstantiateHumanoid>(ForwardInstantiateHumanoid);
            NetworkServer.RegisterHandler<HumanoidNetworking.DestroyHumanoid>(ForwardDestroyHumanoid);

            NetworkServer.RegisterHandler<HumanoidNetworking.HumanoidPose>(ForwardHumanoidPose);

            NetworkServer.RegisterHandler<HumanoidNetworking.Grab>(ForwardGrab);
            NetworkServer.RegisterHandler<HumanoidNetworking.LetGo>(ForwardLetGo);

            NetworkServer.RegisterHandler<HumanoidNetworking.ChangeAvatar>(ForwardChangeAvatar);

            NetworkServer.RegisterHandler<HumanoidNetworking.SyncTrackingSpace>(ForwardSyncTrackingSpace);
        }

        public override void OnStartClient() {
            name = name + " " + netId;

            NetworkClient.RegisterHandler<HumanoidNetworking.InstantiateHumanoid>(ReceiveInstantiateHumanoid);
            NetworkClient.RegisterHandler<HumanoidNetworking.DestroyHumanoid>(ReceiveDestroyHumanoid);

            NetworkClient.RegisterHandler<HumanoidNetworking.HumanoidPose>(ReceiveHumanoidPose);

            NetworkClient.RegisterHandler<HumanoidNetworking.Grab>(ReceiveGrab);
            NetworkClient.RegisterHandler<HumanoidNetworking.LetGo>(ReceiveLetGo);

            NetworkClient.RegisterHandler<HumanoidNetworking.ChangeAvatar>(ReceiveChangeAvatar);

            NetworkClient.RegisterHandler<HumanoidNetworking.SyncTrackingSpace>(ReceiveSyncTrackingSpace);

            if (identity.isServer) {
                IHumanoidNetworking[] nwHumanoids = FindObjectsOfType<HumanoidPlayer>();
                foreach (IHumanoidNetworking nwHumanoid in nwHumanoids) {
                    foreach (HumanoidControl humanoid in nwHumanoid.humanoids) {
                        HumanoidNetworking.InstantiateHumanoid instantiateHumanoid = new HumanoidNetworking.InstantiateHumanoid(humanoid);
                        NetworkServer.SendToClientOfPlayer(identity, instantiateHumanoid);
                    }
                }
            }
        }

    #endregion

    #region Start
        public override void OnStartLocalPlayer() {
            isLocal = true;
            name = "HumanoidPlayer(Local)";

            humanoids = HumanoidNetworking.FindLocalHumanoids();
            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog("Found " + humanoids.Count + " Humanoids");

            for (int i = 0; i < humanoids.Count; i++) {
                HumanoidControl humanoid = humanoids[i];
                if (humanoid.isRemote)
                    continue;

                humanoid.nwId = (ulong)netId;
                humanoid.humanoidNetworking = this;

                if (debug <= PawnNetworking.DebugLevel.Info)
                    DebugLog("Send Start Humanoid " + humanoid.humanoidId);

                ((IHumanoidNetworking)this).InstantiateHumanoid(humanoid);
            }
            NetworkingSpawner spawner = FindObjectOfType<NetworkingSpawner>();
            if (spawner != null)
                spawner.OnNetworkingStarted();
        }

    #endregion

    #region Update

        override public void LateUpdate() {
            if (!isLocalPlayer)
                return;

            if (Time.time > lastSend + 1 / sendRate) {
                foreach (HumanoidControl humanoid in humanoids) {
                    if (!humanoid.isRemote) {
                        UpdateHumanoidPose(humanoid);
                        if (syncTracking)
                            SyncTrackingSpace(humanoid);
                    }
                }
                lastSend = Time.time;
            }
        }

    #endregion

    #region Stop

        override public void OnDestroy() {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log((int)netId + ": Destroy Remote Humanoid");

            foreach (HumanoidControl humanoid in humanoids) {
                if (humanoid == null)
                    continue;

                if (humanoid.isRemote) {
                    if (humanoid.gameObject != null)
                        Destroy(humanoid.gameObject);
                }
                else
                    humanoid.nwId = 0;
            }
        }
    #endregion

    #region Instantiate Humanoid

        public void InstantiateHumanoid(HumanoidControl humanoid) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog("Instantiate Humanoid " + humanoid.humanoidId);

            HumanoidNetworking.InstantiateHumanoid instantiateHumanoid = new HumanoidNetworking.InstantiateHumanoid(humanoid);

            NetworkIdentity identity = GetComponent<NetworkIdentity>();
            if (identity.isServer)
                this.Receive(instantiateHumanoid);
            else
                identity.connectionToServer.Send(instantiateHumanoid, Channels.DefaultReliable);
        }

        public void ForwardInstantiateHumanoid(NetworkConnection nwConnection, HumanoidNetworking.InstantiateHumanoid instantiateHumanoid) {
            NetworkServer.SendToAll(instantiateHumanoid, Channels.DefaultUnreliable);
        }

        protected void ReceiveInstantiateHumanoid(NetworkConnection nwConnection, HumanoidNetworking.InstantiateHumanoid instantiateHumanoid) {
            this.Receive(instantiateHumanoid);
        }


    #endregion

    #region Destroy Humanoid

        public void DestroyHumanoid(HumanoidControl humanoid) {
            if (humanoid == null)
                return;

            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog(netId + ": Destroy Humanoid " + humanoid.humanoidId);

            HumanoidNetworking.DestroyHumanoid destroyHumanoid = new HumanoidNetworking.DestroyHumanoid(humanoid);

            if (identity.isServer)
                this.Receive(destroyHumanoid);
            else
                identity.connectionToServer.Send(destroyHumanoid, Channels.DefaultReliable);
        }

        public void ForwardDestroyHumanoid(NetworkConnection nwConnection, HumanoidNetworking.DestroyHumanoid destroyHumanoid) {
            NetworkServer.SendToAll(destroyHumanoid, Channels.DefaultReliable);
        }

        protected void ReceiveDestroyHumanoid(NetworkConnection nwConnection, HumanoidNetworking.DestroyHumanoid destroyHumanoid) {
            this.Receive(destroyHumanoid);
        }

    #endregion

    #region Pose

        public HumanoidNetworking.HumanoidPose lastHumanoidPose { get; set; }

        public virtual void UpdateHumanoidPose(HumanoidControl humanoid) {
            if (humanoid == null)
                return;

            HumanoidNetworking.HumanoidPose humanoidPose = new HumanoidNetworking.HumanoidPose(humanoid, Time.time, true);

            if (debug <= PawnNetworking.DebugLevel.Debug)
                DebugLog("Send Pose Humanoid " + humanoidPose.humanoidId + " nwId: " + humanoidPose.nwId);

            if (identity.isServer) {
                this.Receive(humanoidPose);
                NetworkServer.SendToAll(humanoidPose, Channels.DefaultUnreliable);
            }
            else
                identity.connectionToServer.Send(humanoidPose, Channels.DefaultUnreliable);
        }

        protected virtual void ForwardHumanoidPose(NetworkConnection nwConnection, HumanoidNetworking.HumanoidPose humanoidPose) {
            if (debug <= PawnNetworking.DebugLevel.Debug)
                DebugLog("Forward Pose Humanoid: " + humanoidPose.humanoidId + " nwId: " + humanoidPose.nwId);

            NetworkServer.SendToAll(humanoidPose, Channels.DefaultUnreliable);
        }

        protected virtual void ReceiveHumanoidPose(NetworkConnection nwConnection, HumanoidNetworking.HumanoidPose humanoidPose) {
            // NetworkMessages are not send to a specific player, but are received by the last
            // instantiated player. We need to find the player with the correct NetworkInstanceId
            // first and then let that player receive the message.
            NetworkIdentity nwId = NetworkIdentity.spawned[(uint)humanoidPose.nwId];
            if (nwId != null) {
                GameObject nwObject = nwId.gameObject;
                if (nwObject != null) {
                    HumanoidPlayer humanoidPlayer = nwObject.GetComponent<HumanoidPlayer>();
                    humanoidPlayer.Receive(humanoidPose);
                }
                else {
                    if (debug <= PawnNetworking.DebugLevel.Warning)
                        DebugWarning("Could not find HumanoidNetworking object");
                }
            }
        }

    #endregion

    #region Grab

        void IHumanoidNetworking.Grab(HandTarget handTarget, GameObject obj, bool rangeCheck, HandInteraction.GrabType grabType) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog("Grab " + obj);

//            NetworkIdentity nwIdentity = obj.GetComponent<NetworkIdentity>();

            ulong objIdentity = GetObjectIdentity(obj);
            if (objIdentity == 0) { 
//            if (nwIdentity == null) {
                if (debug <= PawnNetworking.DebugLevel.Error)
                    Debug.LogError("Grabbed object " + obj + " does not have a network identity");
                return;
            }

            HumanoidNetworking.Grab grab = new HumanoidNetworking.Grab(handTarget, objIdentity, rangeCheck, grabType);

            if (identity.isServer)
                this.Receive(grab);
            else
                identity.connectionToServer.Send(grab, Channels.DefaultReliable);

        }

        public void ForwardGrab(NetworkConnection nwConnection, HumanoidNetworking.Grab grab) {
            NetworkServer.SendToAll(grab, Channels.DefaultReliable);
        }

        protected void ReceiveGrab(NetworkConnection nwConnection, HumanoidNetworking.Grab grab) {
            this.Receive(grab);
        }

    #endregion

    #region Let Go

        void IHumanoidNetworking.LetGo(HandTarget handTarget) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog("LetGo");

            HumanoidNetworking.LetGo letGo = new HumanoidNetworking.LetGo(handTarget);

            if (identity.isServer)
                this.Receive(letGo);
            else
                identity.connectionToServer.Send(letGo, Channels.DefaultReliable);
        }

        public void ForwardLetGo(NetworkConnection nwConnection, HumanoidNetworking.LetGo letGo) {
            NetworkServer.SendToAll(letGo, Channels.DefaultReliable);
        }

        protected void ReceiveLetGo(NetworkConnection nwConnection, HumanoidNetworking.LetGo letGo) {
            this.Receive(letGo);
        }

    #endregion

    #region ChangeAvatar

        void IHumanoidNetworking.ChangeAvatar(HumanoidControl humanoid, string avatarPrefabName) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log("Change Avatar: " + avatarPrefabName);

            HumanoidNetworking.ChangeAvatar changeAvatar = new HumanoidNetworking.ChangeAvatar(humanoid, avatarPrefabName);

            if (identity.isServer)
                this.Receive(changeAvatar);
            else
                identity.connectionToServer.Send(changeAvatar, Channels.DefaultReliable);
        }

        public void ForwardChangeAvatar(NetworkConnection nwConnection, HumanoidNetworking.ChangeAvatar changeAvatar) {
            NetworkServer.SendToAll(changeAvatar, Channels.DefaultReliable);
        }

        protected void ReceiveChangeAvatar(NetworkConnection nwConnection, HumanoidNetworking.ChangeAvatar changeAvatar) {
            this.Receive(changeAvatar);
        }

    #endregion

    #region Tracking

        private Transform GetTrackingTransform(HumanoidControl humanoid) {
#if hOPENVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
            if (humanoid.openVR != null)
                return humanoid.openVR.GetTrackingTransform();
#endif
            return null;
        }

        public void SyncTrackingSpace(HumanoidControl humanoid) {
            if (humanoid == null)
                return;

            Transform trackingTransform = GetTrackingTransform(humanoid);
            if (trackingTransform == null)
                return;

            if (debug <= PawnNetworking.DebugLevel.Info)
                DebugLog("Sync Tracking Space " + humanoid.humanoidId + " " + trackingTransform.position + " " + trackingTransform.rotation);

            HumanoidNetworking.SyncTrackingSpace syncTrackingSpace = 
                new HumanoidNetworking.SyncTrackingSpace(humanoid, trackingTransform.position, trackingTransform.rotation);

            if (identity.isServer)
                this.Receive(syncTrackingSpace);
            else
                identity.connectionToServer.Send(syncTrackingSpace, Channels.DefaultReliable);
        }

        public void ForwardSyncTrackingSpace(NetworkConnection nwConnection, HumanoidNetworking.SyncTrackingSpace syncTrackingSpace) {
            NetworkServer.SendToAll(syncTrackingSpace, Channels.DefaultReliable);
        }

        protected void ReceiveSyncTrackingSpace(NetworkConnection nwConnection, HumanoidNetworking.SyncTrackingSpace syncTrackingSpace) {
            this.Receive(syncTrackingSpace);
        }

    #endregion

    #region Network Sync

        void IHumanoidNetworking.ReenableNetworkSync(GameObject obj) {
            ReenableNetworkSync(obj);
        }

        void IHumanoidNetworking.DisableNetworkSync(GameObject obj) {
            DisableNetworkSync(obj);
        }


        public static void DisableNetworkSync(GameObject obj) {
            NetworkTransform networkTransform = obj.GetComponent<NetworkTransform>();
            if (networkTransform != null) {
                networkTransform.enabled = false;
                networkTransform.syncInterval = 0;
            }
        }

        public static void ReenableNetworkSync(GameObject obj) {
            NetworkTransform networkTransform = obj.GetComponent<NetworkTransform>();
            if (networkTransform != null) {
                networkTransform.enabled = true;
                networkTransform.syncInterval = 0.1F;
            }
        }

    #endregion

    #region Debug

        public void DebugLog(string s) {
            Debug.Log(netId + ": " + s);
        }

        public void DebugWarning(string s) {
            Debug.LogWarning(netId + ": " + s);
        }

        public void DebugError(string s) {
            Debug.LogError(netId + ": " + s);
        }

    #endregion
    }
#endif
}