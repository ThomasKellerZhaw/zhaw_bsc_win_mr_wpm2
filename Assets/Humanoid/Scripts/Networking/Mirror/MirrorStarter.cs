﻿using UnityEngine;
#if hNW_MIRROR
using Mirror;
#endif

namespace Passer {
#if hNW_MIRROR    
    public class MirrorStarter : INetworkingStarter {
        GameObject INetworkingStarter.GetHumanoidPrefab() {
            GameObject humanoidPrefab = Resources.Load<GameObject>("HumanoidPlayer");
            return humanoidPrefab;
        }

        void INetworkingStarter.StartHost(NetworkingStarter nwStarter) {
            NetworkManager networkManager = nwStarter.GetComponent<NetworkManager>();
            if (networkManager == null) {
                Debug.LogError("Could not start host: NetworkManager is missing");
                return;
            }

            networkManager.StartHost();
        }

        void INetworkingStarter.StartClient(NetworkingStarter nwStarter) {
            NetworkManager networkManager = nwStarter.GetComponent<NetworkManager>();
            networkManager.networkAddress = nwStarter.serverIpAddress;
            networkManager.StartClient();
        }

        void INetworkingStarter.StartClient(NetworkingStarter nwStarter, string roomName, int gameVersion) {
            ((INetworkingStarter)this).StartClient(nwStarter);
        }
    }
#endif
}