﻿using UnityEngine;

namespace Passer.Humanoid {

    public class OnLoadHumanoidPlayerBolt {
        public static void CheckHumanoidPlayer() {
            string prefabPath = OnLoadHumanoidPlayer.GetHumanoidPlayerPrefabPath();
            GameObject playerPrefab = OnLoadHumanoidPlayer.GetHumanoidPlayerPrefab(prefabPath);

#if hBOLT
#if hNW_BOLT
            if (playerPrefab != null) {
                BoltEntity boltEntity = playerPrefab.GetComponent<BoltEntity>();
                if (boltEntity == null) {
                    boltEntity = playerPrefab.AddComponent<BoltEntity>();
                }
            }
#else
            if (playerPrefab != null) {               
                BoltEntity boltEntity = playerPrefab.GetComponent<BoltEntity>();
                if (boltEntity != null)
                    Object.DestroyImmediate(boltEntity, true);
            }
#endif
#endif
            OnLoadHumanoidPlayer.UpdateHumanoidPrefab(playerPrefab, prefabPath);
        }
    }
}
