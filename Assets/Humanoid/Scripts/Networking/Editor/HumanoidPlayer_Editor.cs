﻿using UnityEngine;
using UnityEditor;
#if !UNITY_2019_1_OR_NEWER
using UnityEngine.Networking;
#endif


namespace Passer.Humanoid {

    [InitializeOnLoad]
    public class OnLoadHumanoidPlayer {
        static OnLoadHumanoidPlayer() {
#if !UNITY_2018_3_OR_NEWER
            // Direct updating of the Prefabs is not possible (Unity Crash) in 2018.3+
            // so we signal that the HumanoidSettings should handle this.
            // This does not work in pre-2018.3, because that does not have a SettingsProvider

            OnLoadHumanoidPlayerUnet.CheckHumanoidPlayer();
            OnLoadHumanoidPlayerPun.CheckHumanoidPlayer();
            OnLoadHumanoidPlayerBolt.CheckHumanoidPlayer();
            OnLoadHumanoidPlayerMirror.CheckHumanoidPlayer();
#else
            HumanoidSettingsIMGUIRegister.reload = true;
#endif
        }

        public static string GetHumanoidPlayerPrefabPath() {
            string humanoidPath = Configuration_Editor.FindHumanoidFolder();
            string prefabPathWithoutScripts = humanoidPath.Substring(0, humanoidPath.Length - 8);
            string prefabPath = "Assets" + prefabPathWithoutScripts + "Prefabs/Networking/Resources/HumanoidPlayer.prefab";
            return prefabPath;
        }

        public static GameObject GetHumanoidPlayerPrefab(string prefabPath) {
#if UNITY_2018_3_OR_NEWER
            GameObject prefab = PrefabUtility.LoadPrefabContents(prefabPath);
            return prefab;
#else
            GameObject prefab = (GameObject)Resources.Load("HumanoidPlayer");
            return prefab;
#endif
        }

        public static void UpdateHumanoidPrefab(GameObject prefab, string prefabPath) {
#if UNITY_2018_3_OR_NEWER
            PrefabUtility.SaveAsPrefabAsset(prefab, prefabPath);
            PrefabUtility.UnloadPrefabContents(prefab);
#endif
        }

    }

    [CustomEditor(typeof(HumanoidPlayer))]
    public class HumanoidPlayer_Editor : HumanoidNetworking_Editor {
#if hNW_UNET
        public override void OnInspectorGUI() {
            serializedObject.Update();

            SendRateInspector();
            DebugLevelInspector();
            SmoothingInspector();
            SyncFingerSwingInspector();
            CreateLocalRemotesInspector();
            SyncTrackingInspector();

            serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}