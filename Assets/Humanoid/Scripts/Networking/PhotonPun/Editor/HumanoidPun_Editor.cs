﻿using UnityEngine;
#if hPHOTON2
using Photon.Pun;
#endif

namespace Passer.Humanoid {

    public class OnLoadHumanoidPlayerPun {

        public static void CheckHumanoidPlayer() {
            string prefabPath = OnLoadHumanoidPlayer.GetHumanoidPlayerPrefabPath();
            GameObject playerPrefab = OnLoadHumanoidPlayer.GetHumanoidPlayerPrefab(prefabPath);

#if hPHOTON1 || hPHOTON2
#if hNW_PHOTON
            if (playerPrefab != null) {
                PhotonView photonView = playerPrefab.GetComponent<PhotonView>();
                if (photonView == null) {
                    photonView = playerPrefab.AddComponent<PhotonView>();
                    photonView.ObservedComponents = new System.Collections.Generic.List<Component>();
#if hPHOTON2
                    photonView.Synchronization = ViewSynchronization.UnreliableOnChange;
#else
                    photonView.synchronization = ViewSynchronization.UnreliableOnChange;
#endif

                    HumanoidPlayer humanoidPun = playerPrefab.GetComponent<HumanoidPlayer>();
                    if (humanoidPun != null)
                        photonView.ObservedComponents.Add(humanoidPun);
                }
            }
#else
            if (playerPrefab != null) {
                PhotonView photonView = playerPrefab.GetComponent<PhotonView>();
                if (photonView != null)
                    Object.DestroyImmediate(photonView, true);
            }
#endif
#endif
            OnLoadHumanoidPlayer.UpdateHumanoidPrefab(playerPrefab, prefabPath);
        }
    }

}