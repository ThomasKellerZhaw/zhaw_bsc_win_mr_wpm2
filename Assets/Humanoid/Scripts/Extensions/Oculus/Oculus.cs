﻿#if hOCULUS && (UNITY_STANDALONE_WIN || UNITY_ANDROID)

using UnityEngine;

namespace Passer.Humanoid {
    using Humanoid.Tracking;

    [System.Serializable]
    public class OculusHumanoidTracker : HumanoidTracker {

        public bool handTracking = true;

        public OculusHumanoidTracker() {
            deviceView = new DeviceView();
        }

        public override string name {
            get { return OculusDevice.name; }
        }

        public override HeadSensor headSensor {
            get { return humanoid.headTarget.oculus; }
        }
        public override ArmSensor leftHandSensor {
            get { return humanoid.leftHandTarget.oculus; }
        }
        public override ArmSensor rightHandSensor {
            get { return humanoid.rightHandTarget.oculus; }
        }

        public TrackerComponent trackerComponent;

        [System.NonSerialized]
        private UnitySensor[] _sensors;
        public override UnitySensor[] sensors {
            get {
                if (_sensors == null)
                    _sensors = new UnitySensor[] {
                        headSensor,
                        leftHandSensor,
                        rightHandSensor
                    };

                return _sensors;
            }
        }

#if UNITY_ANDROID
        public enum AndroidDeviceType {
            GearVR,
            OculusGo,
            OculusQuest,
        }
        public AndroidDeviceType androidDeviceType = AndroidDeviceType.OculusQuest;
#endif

        #region Start

        public override void StartTracker(HumanoidControl _humanoid) {
            humanoid = _humanoid;

            if (humanoid.headTarget.unity.enabled && UnityVRDevice.xrDevice == UnityVRDevice.XRDeviceType.Oculus)
                enabled = true;

            if (!enabled || UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.Oculus)
                return;

            OculusDevice.Start();

            AddTracker(humanoid, "Oculus");
            CheckTrackerComponent();
            StartCameras(trackerTransform);
        }

        private void StartCameras(Transform trackerTransform) {
            //subTrackers = new OculusCameraComponent[(int)OculusDevice.Tracker.Count];
            for (int i = 0; i < OculusTrackingCamera.GetCount(); i++) {
                OculusTrackingCamera oculusCamera = OculusTrackingCamera.Create(this);
                oculusCamera.subTrackerId = i;
                subTrackers.Add(oculusCamera);
                //subTrackers[i] = OculusCameraComponent.Create(this);
                //subTrackers[i].subTrackerId = i;
            }
        }

        protected virtual void CheckTrackerComponent() {
            if (trackerComponent != null)
                return;

            trackerComponent = trackerTransform.GetComponent<OculusTracker>();
            if (trackerComponent == null)
                trackerTransform.gameObject.AddComponent<OculusTracker>();
        }
        #endregion

        #region Update

        public override void UpdateTracker() {
            if (!enabled || trackerTransform == null)
                return;

            if (UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.Oculus) {
                status = Status.Unavailable;
                return;
            }

            status = OculusDevice.status;

            deviceView.position = HumanoidTarget.ToVector(trackerTransform.position);
            deviceView.orientation = HumanoidTarget.ToRotation(trackerTransform.rotation);

            OculusDevice.Update();

            foreach (SubTracker subTracker in subTrackers) {
                if (subTracker != null)
                    subTracker.UpdateTracker(humanoid.showRealObjects);
            }

            if (OculusDevice.ovrp_GetAppShouldRecenter() == OculusDevice.Bool.True) {
                humanoid.Calibrate();
            }
        }
        
        #endregion

        public override void Calibrate() {
            base.Calibrate();

            OculusDevice.ovrp_RecenterTrackingOrigin(unchecked((uint)OculusDevice.RecenterFlags.IgnoreAll));
        }

        public override void AdjustTracking(Vector3 v, Quaternion q) {
            if (trackerTransform != null) {
                trackerTransform.position += v;
                trackerTransform.rotation *= q;
            }
        }
    }
}
#endif