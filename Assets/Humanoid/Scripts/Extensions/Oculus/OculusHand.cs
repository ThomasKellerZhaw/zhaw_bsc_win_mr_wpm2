﻿using System.Collections.Generic;
#if hOCULUS && (UNITY_STANDALONE_WIN || UNITY_ANDROID)
using UnityEngine;

namespace Passer.Humanoid {
    using Tracking;

    [System.Serializable]
    public class OculusHand : ArmController {
        public override string name {
            get { return "Oculus"; }
        }

        protected OculusHumanoidTracker oculusTracker;
        protected OculusController oculusController;
#if hOCHAND
        public OculusHandSkeleton handSkeleton;
#endif

        public override Tracker.Status status {
            get {
#if hOCHAND
                if (oculusTracker.handTracking && handSkeleton != null)
                    return handSkeleton.status;
                else
#endif
                if (!oculusTracker.handTracking && oculusController != null)
                    return oculusController.status;
                else
                    return Tracker.Status.Unavailable;
            }
            set { oculusController.status = value; }
        }

        #region Start

        public override void Init(HandTarget handTarget) {
            base.Init(handTarget);
            oculusTracker = handTarget.humanoid.oculus;
            tracker = oculusTracker;
        }

        public override void Start(HumanoidControl _humanoid, Transform targetTransform) {
            base.Start(_humanoid, targetTransform);
            oculusTracker = handTarget.humanoid.oculus;
            tracker = oculusTracker;

            if (tracker == null || !tracker.enabled || !enabled)
                return;

            SetSensor2Target();
            CheckSensorTransform();
            ShowSensor(handTarget.humanoid.showRealObjects && target.showRealObjects);

            if (sensorTransform != null) {
                oculusController = sensorTransform.GetComponent<OculusController>();
                if (oculusController != null) {
#if UNITY_ANDROID
                    if (handTarget.humanoid.oculus.androidDeviceType == OculusHumanoidTracker.AndroidDeviceType.GearVR ||
                        handTarget.humanoid.oculus.androidDeviceType == OculusHumanoidTracker.AndroidDeviceType.OculusGo) {

                        oculusController.positionalTracking = false;
                    }
                    else
#endif
                    oculusController.positionalTracking = true;
                    oculusController.StartComponent(tracker.trackerTransform);
                }
            }
        }

        #region Controller

        protected override void CreateSensorTransform() {
            if (handTarget.isLeft) {
                string controllerName = "Left Touch Controller";
#if UNITY_ANDROID
                switch (handTarget.humanoid.oculus.androidDeviceType) {
                    case OculusHumanoidTracker.AndroidDeviceType.GearVR:
                        controllerName = "GearVR Controller";
                        break;
                    case OculusHumanoidTracker.AndroidDeviceType.OculusGo:
                        controllerName = "OculusGo Controller";
                        break;
                    case OculusHumanoidTracker.AndroidDeviceType.OculusQuest:
                        controllerName = "Left Quest Controller";
                        break;
                    default:
                        break;
                }
#endif
                CreateSensorTransform(controllerName, new Vector3(-0.1F, -0.05F, 0.04F), Quaternion.Euler(180, 90, 90));
            }
            else {
                string controllerName = "Right Touch Controller";
#if UNITY_ANDROID
                switch (handTarget.humanoid.oculus.androidDeviceType) {
                    case OculusHumanoidTracker.AndroidDeviceType.GearVR:
                        controllerName = "GearVR Controller";
                        break;
                    case OculusHumanoidTracker.AndroidDeviceType.OculusGo:
                        controllerName = "OculusGo Controller";
                        break;
                    case OculusHumanoidTracker.AndroidDeviceType.OculusQuest:
                        controllerName = "Right Quest Controller";
                        break;
                    default:
                        break;
                }
#endif
                CreateSensorTransform(controllerName, new Vector3(0.1F, -0.05F, 0.04F), Quaternion.Euler(180, -90, -90));
            }

            OculusController oculusController = sensorTransform.GetComponent<OculusController>();
            if (oculusController == null)
                oculusController = sensorTransform.gameObject.AddComponent<OculusController>();
            oculusController.isLeft = handTarget.isLeft;
        }

        #endregion

        #region Skeleton

#if hOCHAND
        public OculusHandSkeleton FindHandSkeleton(bool isLeft) {
            OculusHandSkeleton[] handSkeletons = tracker.trackerTransform.GetComponentsInChildren<OculusHandSkeleton>();
            foreach (OculusHandSkeleton handSkeleton in handSkeletons) {
                if (handSkeleton.isLeft == isLeft)
                    return handSkeleton;
            }
            return null;
        }

        public OculusHandSkeleton CreateHandSkeleton(bool isLeft, bool showRealObjects) {
            GameObject skeletonObj = new GameObject(isLeft ? "Left Hand Skeleton" : "Right Hand Skeleton");
            skeletonObj.transform.parent = tracker.trackerTransform;
            skeletonObj.transform.localPosition = Vector3.zero;
            skeletonObj.transform.localRotation = Quaternion.identity;

            OculusHandSkeleton handSkeleton = skeletonObj.AddComponent<OculusHandSkeleton>();
            handSkeleton.isLeft = isLeft;
            handSkeleton._show = showRealObjects;
            return handSkeleton;
        }
#endif

        #endregion

        #endregion

        #region Update

        public override void Update() {
            if (tracker == null || !tracker.enabled || !enabled)
                return;

#if UNITY_ANDROID && hOCHAND
            Debug.Log(oculusTracker.handTracking);
            if (oculusTracker.handTracking) {
                if (handSkeleton == null)
                    handSkeleton = CreateHandSkeleton(handTarget.isLeft, handTarget.showRealObjects);

                if (handSkeleton != null && handSkeleton.status == Tracker.Status.Tracking) {
                    UpdateHandFromSkeleton();
                    handTarget.hand.target.confidence.position = 0.9F;
                    handTarget.hand.target.confidence.rotation = 0.9F;

                    oculusController.show = false;
                    return;
                }
            }
#endif

            if (oculusController == null) {
                UpdateTarget(handTarget.hand.target, sensorTransform);
                return;
            }

            oculusController.UpdateComponent();
            if (oculusController.status != Tracker.Status.Tracking)
                return;

            UpdateTarget(handTarget.hand.target, oculusController);

#if UNITY_ANDROID
            if (handTarget.humanoid.oculus.androidDeviceType == OculusHumanoidTracker.AndroidDeviceType.GearVR ||
                handTarget.humanoid.oculus.androidDeviceType == OculusHumanoidTracker.AndroidDeviceType.OculusGo) {

                CalculateHandPosition();
            }
#endif

            UpdateInput();
        }

        #region Controller

        // arm model for 3DOF tracking: position is calculated from rotation
        protected void CalculateHandPosition() {
            Quaternion hipsYRotation = Quaternion.AngleAxis(handTarget.humanoid.hipsTarget.transform.eulerAngles.y, handTarget.humanoid.up);

            Vector3 pivotPoint = handTarget.humanoid.hipsTarget.transform.position + hipsYRotation * (handTarget.isLeft ? new Vector3(-0.25F, 0.15F, -0.05F) : new Vector3(0.25F, 0.15F, -0.05F));
            Quaternion forearmRotation = handTarget.hand.target.transform.rotation * (handTarget.isLeft ? Quaternion.Euler(0, -90, 0) : Quaternion.Euler(0, 90, 0));

            Vector3 localForearmDirection = handTarget.humanoid.hipsTarget.transform.InverseTransformDirection(forearmRotation * Vector3.forward);

            if (localForearmDirection.x < 0 || localForearmDirection.y > 0) {
                pivotPoint += hipsYRotation * Vector3.forward * Mathf.Lerp(0, 0.15F, -localForearmDirection.x * 3 + localForearmDirection.y);
            }
            if (localForearmDirection.y > 0) {
                pivotPoint += hipsYRotation * Vector3.up * Mathf.Lerp(0, 0.2F, localForearmDirection.y);
            }

            if (localForearmDirection.z < 0.2F) {
                localForearmDirection = new Vector3(localForearmDirection.x, localForearmDirection.y, 0.2F);
                forearmRotation = Quaternion.LookRotation(handTarget.humanoid.hipsTarget.transform.TransformDirection(localForearmDirection), forearmRotation * Vector3.up);
            }

            handTarget.hand.target.transform.position = pivotPoint + forearmRotation * Vector3.forward * handTarget.forearm.bone.length;

            sensorTransform.position = handTarget.hand.target.transform.TransformPoint(-sensor2TargetPosition);
        }

        private void UpdateInput() {
            if (controllerInput == null)
                return;

            if (handTarget.isLeft)
                UpdateInputSide(controllerInput.left);
            else
                UpdateInputSide(controllerInput.right);
        }

        private void UpdateInputSide(ControllerSide controllerInputSide) {
            controllerInputSide.stickHorizontal = oculusController.joystick.x;
            controllerInputSide.stickVertical = oculusController.joystick.y;
            controllerInputSide.stickButton |= (oculusController.joystick.z > 0.5F);
            controllerInputSide.stickTouch |= (oculusController.joystick.z > -0.5F);

            controllerInputSide.buttons[0] |= (oculusController.buttonAX > 0.5F);
            controllerInputSide.buttons[1] |= (oculusController.buttonBY > 0.5F);

            controllerInputSide.trigger1 = oculusController.indexTrigger;
            controllerInputSide.trigger2 = oculusController.handTrigger;
            controllerInputSide.option = oculusController.option > 0;
        }

        public override void Vibrate(float length, float strength) {
            if (controller != null)
                controller.Vibrate(length, strength);
        }

        #endregion

#if hOCHAND
        #region Skeleton
        protected void UpdateHandFromSkeleton() {
            handTarget.hand.target.transform.position = handSkeleton.transform.position;
            if (handTarget.isLeft)
                handTarget.hand.target.transform.rotation = handSkeleton.transform.rotation * Quaternion.Euler(180, 180, 0);
            else
                handTarget.hand.target.transform.rotation = handSkeleton.transform.rotation * Quaternion.Euler(0, 180, 0);
            UpdateThumbFromSkeleton();
            UpdateIndexFingerFromSkeleton();
            UpdateMiddleFingerFromSkeleton();
            UpdateRingFingerFromSkeleton();
            UpdateLittleFingerFromSkeleton();
        }

        protected void UpdateThumbFromSkeleton() {
            FingersTarget.TargetedFinger finger = handTarget.fingers.thumb;
            UpdateFingerBoneFromSkeleton(finger.proximal.target.transform, Finger.Thumb, FingerBone.Proximal);
            UpdateFingerBoneFromSkeleton(finger.intermediate.target.transform, Finger.Thumb, FingerBone.Intermediate);
            UpdateFingerBoneFromSkeleton(finger.distal.target.transform, Finger.Thumb, FingerBone.Distal);
        }

        protected void UpdateIndexFingerFromSkeleton() {
            FingersTarget.TargetedFinger finger = handTarget.fingers.index;
            UpdateFingerBoneFromSkeleton(finger.proximal.target.transform, Finger.Index, FingerBone.Proximal);
            UpdateFingerBoneFromSkeleton(finger.intermediate.target.transform, Finger.Index, FingerBone.Intermediate);
            UpdateFingerBoneFromSkeleton(finger.distal.target.transform, Finger.Index, FingerBone.Distal);
        }

        protected void UpdateMiddleFingerFromSkeleton() {
            FingersTarget.TargetedFinger finger = handTarget.fingers.middle;
            UpdateFingerBoneFromSkeleton(finger.proximal.target.transform, Finger.Middle, FingerBone.Proximal);
            UpdateFingerBoneFromSkeleton(finger.intermediate.target.transform, Finger.Middle, FingerBone.Intermediate);
            UpdateFingerBoneFromSkeleton(finger.distal.target.transform, Finger.Middle, FingerBone.Distal);
        }

        protected void UpdateRingFingerFromSkeleton() {
            FingersTarget.TargetedFinger finger = handTarget.fingers.ring;
            UpdateFingerBoneFromSkeleton(finger.proximal.target.transform, Finger.Ring, FingerBone.Proximal);
            UpdateFingerBoneFromSkeleton(finger.intermediate.target.transform, Finger.Ring, FingerBone.Intermediate);
            UpdateFingerBoneFromSkeleton(finger.distal.target.transform, Finger.Ring, FingerBone.Distal);
        }

        protected void UpdateLittleFingerFromSkeleton() {
            FingersTarget.TargetedFinger finger = handTarget.fingers.little;
            UpdateFingerBoneFromSkeleton(finger.proximal.target.transform, Finger.Little, FingerBone.Proximal);
            UpdateFingerBoneFromSkeleton(finger.intermediate.target.transform, Finger.Little, FingerBone.Intermediate);
            UpdateFingerBoneFromSkeleton(finger.distal.target.transform, Finger.Little, FingerBone.Distal);
        }

        private void UpdateFingerBoneFromSkeleton(Transform targetTransform, Finger finger, FingerBone fingerBone) {
            if (handSkeleton == null)
                return;

            Transform thisBoneTransform = handSkeleton.GetBone(finger, fingerBone);
            Transform nextBoneTransform = handSkeleton.GetBone(finger, fingerBone + 1);
            if (thisBoneTransform == null || nextBoneTransform == null)
                return;

            Vector3 direction = nextBoneTransform.position - thisBoneTransform.position;
            if (handTarget.isLeft)
                targetTransform.rotation = Quaternion.LookRotation(direction, handTarget.hand.target.transform.forward) * Quaternion.Euler(-90, 0, 90);
            else
                targetTransform.rotation = Quaternion.LookRotation(direction, handTarget.hand.target.transform.forward) * Quaternion.Euler(-90, 0, -90);
        }

        #endregion
#endif
        #endregion
    }


}
#endif