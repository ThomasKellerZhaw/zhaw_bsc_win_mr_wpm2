﻿using UnityEngine;

namespace Passer {
    using Humanoid.Tracking;

    public class OculusHmd : SensorComponent {
#if hOCULUS
        private Humanoid.Tracking.Sensor.ID sensorId = Humanoid.Tracking.Sensor.ID.Head;

        bool wasTracking = true;
        Vector3 lastTrackingPosition;

        public override void UpdateComponent() {
            if (OculusDevice.GetRotationalConfidence(sensorId) == 0) {
                status = OculusDevice.IsPresent(0) ? Tracker.Status.Present : Tracker.Status.Unavailable;
                positionConfidence = 0;
                rotationConfidence = 0;
                gameObject.SetActive(false);
                wasTracking = false;
                return;
            }

            if (!OculusDevice.userPresent) {
                status = Tracker.Status.Present;
                positionConfidence = 0;
                rotationConfidence = 0;
                gameObject.SetActive(false);
                wasTracking = false;
                return;
            }

            if (!wasTracking)
                ResetTrackingPosition();

            status = Tracker.Status.Tracking;

            Vector3 localPosition = Humanoid.HumanoidTarget.ToVector3(OculusDevice.GetPosition(sensorId));
            Quaternion localRotation = Humanoid.HumanoidTarget.ToQuaternion(OculusDevice.GetRotation(sensorId));
            transform.position = trackerTransform.TransformPoint(localPosition);
            transform.rotation = trackerTransform.rotation * localRotation;

            positionConfidence = OculusDevice.GetPositionalConfidence(sensorId);
            rotationConfidence = OculusDevice.GetRotationalConfidence(sensorId);
            gameObject.SetActive(true);

            FuseWithUnityCamera();

            wasTracking = true;
            lastTrackingPosition = transform.position;
        }

        protected virtual void FuseWithUnityCamera() {
            Vector3 deltaPos = Camera.main.transform.position - transform.position;
            if (deltaPos.sqrMagnitude > 0.00001) {
                Camera.main.transform.parent.position -= deltaPos;
            }
        }

        // Reset tracking position to the last position when it was tracking
        // This is needed to prevent the camera/avatar move while the headset is off
        protected virtual void ResetTrackingPosition() {
            if (lastTrackingPosition.sqrMagnitude == 0)
                return;

            Vector3 deltaPos = Camera.main.transform.position - lastTrackingPosition;
            trackerTransform.position -= deltaPos;
        }
#endif
    }
}