﻿using UnityEngine;

namespace Passer.Humanoid {

    public class OculusTracker : TrackerComponent {
#if hOCULUS
        public bool persistentTracking;
        public RealWorldConfiguration realWorldConfiguration;

        protected virtual void Start() {
            if (!persistentTracking)
                transform.localPosition = new Vector3(0, Tracking.OculusDevice.eyeHeight, 0);
        }

        protected virtual void OnEnable() {
            if (!persistentTracking)
                return;

            if (realWorldConfiguration == null) {
                Debug.LogError("Could not find Real World Configuration");
                return;
            }

            RealWorldConfiguration.TrackingSpace trackingSpace =
                realWorldConfiguration.trackers.Find(space => space.trackerId == TrackerId.Oculus);

            if (trackingSpace == null)
                return;

            transform.position = trackingSpace.position;
            transform.rotation = trackingSpace.rotation;
        }

        protected virtual void OnDestroy() {
            if (!persistentTracking)
                return;

            if (realWorldConfiguration == null) {
                Debug.LogError("Could not find Real World Configuration");
                return;
            }

            RealWorldConfiguration.TrackingSpace trackingSpace =
                realWorldConfiguration.trackers.Find(space => space.trackerId == TrackerId.Oculus);

            if (trackingSpace == null) {
                trackingSpace = new RealWorldConfiguration.TrackingSpace();
                realWorldConfiguration.trackers.Add(trackingSpace);
            }
            trackingSpace.position = transform.position;
            trackingSpace.rotation = transform.rotation;
        }

#endif
    }

}