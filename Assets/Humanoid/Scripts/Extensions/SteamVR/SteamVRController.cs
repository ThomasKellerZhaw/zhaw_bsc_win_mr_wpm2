﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace Passer.Humanoid.Tracking {

    public class SteamVRController : SensorComponent {
#if hSTEAMVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        protected const string resourceName = "Vive Controller";
        public int trackerId = -1;

        public bool isLeft;

        public enum ControllerType {
            SteamVRController,
            SteamVRKnuckles,
            OculusTouch,
            MixedReality
        }
        public ControllerType controllerType;

        public Vector3 joystick;
        public Vector3 touchpad;
        public float trigger;
        public float grip;
        public float aButton;
        public float bButton;

        [System.NonSerialized]
        ulong actionHandle;
        [System.NonSerialized]
        InputSkeletalActionData_t tempSkeletonActionData = new InputSkeletalActionData_t();
        [System.NonSerialized]
        uint skeletonActionData_size;

        public const int numBones = 31;
        [System.NonSerialized]
        public VRBoneTransform_t[] tempBoneTransforms = new VRBoneTransform_t[numBones];

        public override void StartComponent(Transform trackerTransform) {
            base.StartComponent(trackerTransform);

            string actionName = isLeft ? "/actions/default/in/SkeletonLeftHand" : "/actions/default/in/SkeletonRightHand";
            if (OpenVR.Input != null) {
                EVRInputError err = OpenVR.Input.GetActionHandle(actionName, ref actionHandle);
                if (err != EVRInputError.None) {
                    Debug.LogError("OpenVR.Input.GetActionHandle error: " + err.ToString());
                }

                skeletonActionData_size = (uint)Marshal.SizeOf(tempSkeletonActionData);

                GetInputActionHandles();
            } else {
                Debug.Log("No OpenVR.Input");
            }
        }

        //string renderModelName;
        //string componentName = "RenderModel";
        public RenderModel_ControllerMode_State_t controllerModeState;

        public static SteamVRController NewController(HumanoidControl humanoid, int trackerId = -1) {
            GameObject trackerPrefab = Resources.Load(resourceName) as GameObject;
            GameObject trackerObject = (trackerPrefab == null) ? new GameObject(resourceName) : Instantiate(trackerPrefab);

            trackerObject.name = resourceName;

            SteamVRController trackerComponent = trackerObject.GetComponent<SteamVRController>();
            if (trackerComponent == null)
                trackerComponent = trackerObject.AddComponent<SteamVRController>();

            if (trackerId != -1)
                trackerComponent.trackerId = trackerId;
            trackerObject.transform.parent = humanoid.steam.trackerTransform;

            trackerComponent.StartComponent(humanoid.steam.trackerTransform);

            return trackerComponent;
        }

        public static bool IsLeftController(uint sensorId) {
            ETrackedControllerRole role = OpenVR.System.GetControllerRoleForTrackedDeviceIndex(sensorId);
            return (role == ETrackedControllerRole.LeftHand);
        }

        public static bool IsRightController(uint sensorId) {
            ETrackedControllerRole role = OpenVR.System.GetControllerRoleForTrackedDeviceIndex(sensorId);
            return (role == ETrackedControllerRole.RightHand);
        }

        #region Update

        public override void UpdateComponent() {
            if (SteamDevice.status == Tracker.Status.Unavailable)
                status = Tracker.Status.Unavailable;

            if (trackerId < 0)
                FindOutermostController(isLeft);

            if (SteamDevice.GetConfidence(trackerId) == 0) {
                status = SteamDevice.IsPresent(trackerId) ? Tracker.Status.Present : Tracker.Status.Unavailable;
                positionConfidence = 0;
                rotationConfidence = 0;
                gameObject.SetActive(false);
                return;
            }

            //OpenVR.Input.UpdateActionState()

            //CVRRenderModels rm =  OpenVR.RenderModels;
            //var componentState = new RenderModel_ComponentState_t();
            //if (rm.GetComponentStateForDevicePath(renderModelName, componentName, (ulong)trackerId, ref controllerModeState, ref componentState)) {
            //}


            status = Tracker.Status.Tracking;
            Vector3 localSensorPosition = HumanoidTarget.ToVector3(SteamDevice.GetPosition(trackerId));
            Quaternion localSensorRotation = HumanoidTarget.ToQuaternion(SteamDevice.GetRotation(trackerId));
            transform.position = trackerTransform.TransformPoint(localSensorPosition);
            transform.rotation = trackerTransform.rotation * localSensorRotation;

            positionConfidence = SteamDevice.GetConfidence(trackerId);
            rotationConfidence = SteamDevice.GetConfidence(trackerId);
            gameObject.SetActive(true);

            VRControllerState_t controllerState = new VRControllerState_t();
            var system = OpenVR.System;
            uint controllerStateSize = (uint)Marshal.SizeOf(typeof(VRControllerState_t));
            bool newControllerState = system.GetControllerState((uint)trackerId, ref controllerState, controllerStateSize);
            if (system != null && newControllerState)
                UpdateInput(controllerState);

            Skeletal();
        }

        #endregion

        private void FindOutermostController(bool isLeft) {
            Vector outermostLocalPos = new Vector(isLeft ? -0.1F : 0.1F, 0, 0);

            for (int i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++) {
                if (SteamDevice.GetDeviceClass(i) != ETrackedDeviceClass.Controller)
                    continue;

                ETrackedControllerRole role = OpenVR.System.GetControllerRoleForTrackedDeviceIndex((uint)i);
                if ((isLeft && role == ETrackedControllerRole.LeftHand) ||
                    (!isLeft && role == ETrackedControllerRole.RightHand)) {

                    trackerId = i;
                    return;
                }

                Vector sensorLocalPos = Rotation.Inverse(SteamDevice.GetRotation(0)) * (SteamDevice.GetPosition(i) - SteamDevice.GetPosition(0)); // 0 = HMD

                if ((isLeft && sensorLocalPos.x < outermostLocalPos.x && role != ETrackedControllerRole.RightHand) ||
                    (!isLeft && sensorLocalPos.x > outermostLocalPos.x) && role != ETrackedControllerRole.LeftHand) {

                    trackerId = i;
                    outermostLocalPos = sensorLocalPos;
                }
            }
        }

        /// <summary>The order of the joints that SteamVR Skeleton Input is expecting.</summary>
        public class SteamVR_Skeleton_JointIndexes {
            public const int root = 0;
            public const int wrist = 1;
            public const int thumbMetacarpal = 2;
            public const int thumbProximal = 2;
            public const int thumbMiddle = 3;
            public const int thumbDistal = 4;
            public const int thumbTip = 5;
            public const int indexMetacarpal = 6;
            public const int indexProximal = 7;
            public const int indexMiddle = 8;
            public const int indexDistal = 9;
            public const int indexTip = 10;
            public const int middleMetacarpal = 11;
            public const int middleProximal = 12;
            public const int middleMiddle = 13;
            public const int middleDistal = 14;
            public const int middleTip = 15;
            public const int ringMetacarpal = 16;
            public const int ringProximal = 17;
            public const int ringMiddle = 18;
            public const int ringDistal = 19;
            public const int ringTip = 20;
            public const int pinkyMetacarpal = 21;
            public const int pinkyProximal = 22;
            public const int pinkyMiddle = 23;
            public const int pinkyDistal = 24;
            public const int pinkyTip = 25;
            public const int thumbAux = 26;
            public const int indexAux = 27;
            public const int middleAux = 28;
            public const int ringAux = 29;
            public const int pinkyAux = 30;
        }

        private void Skeletal() {
            ulong restrictToDevice = 0; // Any Device for now
            EVRInputError err = OpenVR.Input.GetSkeletalActionData(actionHandle, ref tempSkeletonActionData, skeletonActionData_size, restrictToDevice);
            if (err != EVRInputError.None) {
                Debug.LogError("GetSkeletalActionData error: " + err.ToString() + ", handle: " + actionHandle.ToString());
                return;
            }
            if (tempSkeletonActionData.bActive) {
                err = OpenVR.Input.GetSkeletalBoneData(actionHandle, EVRSkeletalTransformSpace.Parent, EVRSkeletalMotionRange.WithoutController, tempBoneTransforms, restrictToDevice);
                if (err != EVRInputError.None) {
                    Debug.LogError("GetSkeletalBoneData error: " + err.ToString() + " handle: " + actionHandle.ToString());
                    return;
                }
            }
        }

        [System.NonSerialized]
        protected ulong actionHandleJoystick;
        [System.NonSerialized]
        protected ulong actionHandleJoystickTouch;
        [System.NonSerialized]
        protected ulong actionHandleJoystickPress;

        [System.NonSerialized]
        protected ulong actionHandleTouchpad;
        [System.NonSerialized]
        protected ulong actionHandleTouchpadTouch;
        [System.NonSerialized]
        protected ulong actionHandleTouchpadPress;
        [System.NonSerialized]
        protected ulong actionHandleTouchpadClick;

        [System.NonSerialized]
        protected ulong actionHandleTrigger;
        [System.NonSerialized]
        protected ulong actionHandleTriggerTouch;


        [System.NonSerialized]
        protected ulong actionHandleGrip;
        [System.NonSerialized]
        protected ulong actionHandleGripTouch;
        [System.NonSerialized]
        protected ulong actionHandleGripClick;

        [System.NonSerialized]
        protected ulong actionHandleButtonA;
        [System.NonSerialized]
        protected ulong actionHandleButtonATouch;

        [System.NonSerialized]
        protected ulong actionHandleButtonB;
        [System.NonSerialized]
        protected ulong actionHandleButtonBTouch;

        protected void GetInputActionHandles() {
            GetInputActionHandle(isLeft ? "LeftJoystick" : "RightJoystick", ref actionHandleJoystick);
            GetInputActionHandle(isLeft ? "LeftJoystickTouch" : "RightJoystickTouch", ref actionHandleJoystickTouch);
            GetInputActionHandle(isLeft ? "LeftJoystickPress" : "RightJoystickPress", ref actionHandleJoystickPress);

            GetInputActionHandle(isLeft ? "LeftTouchpad" : "RightTouchpad", ref actionHandleTouchpad);
            GetInputActionHandle(isLeft ? "LeftTouchpadTouch" : "RightTouchpadTouch", ref actionHandleTouchpadTouch);
            GetInputActionHandle(isLeft ? "LeftTouchpadPress" : "RightTouchpadPress", ref actionHandleTouchpadPress);
            GetInputActionHandle(isLeft ? "LeftTouchpadClick" : "RightTouchpadClick", ref actionHandleTouchpadClick);

            GetInputActionHandle(isLeft ? "LeftTrigger" : "RightTrigger", ref actionHandleTrigger);
            GetInputActionHandle(isLeft ? "LeftTriggerTouch" : "RightTriggerTouch", ref actionHandleTriggerTouch);

            GetInputActionHandle(isLeft ? "LeftGrip" : "RightGrip", ref actionHandleGrip);
            GetInputActionHandle(isLeft ? "LeftGripTouch" : "RightGripTouch", ref actionHandleGripTouch);
            GetInputActionHandle(isLeft ? "LeftGripClick" : "RightGripClick", ref actionHandleGripClick);

            GetInputActionHandle(isLeft ? "LeftButtonA" : "RightButtonA", ref actionHandleButtonA);
            GetInputActionHandle(isLeft ? "LeftButtonATouch" : "RightButtonATouch", ref actionHandleButtonATouch);

            GetInputActionHandle(isLeft ? "LeftButtonB" : "RightButtonB", ref actionHandleButtonB);
            GetInputActionHandle(isLeft ? "LeftButtonBTouch" : "RightButtonBTouch", ref actionHandleButtonBTouch);
        }

        protected static void GetInputActionHandle(string name, ref ulong actionHandle) {
            EVRInputError err;
            string path = "/actions/default/in/" + name;
            err = OpenVR.Input.GetActionHandle(path, ref actionHandle);
            if (err != EVRInputError.None) {
                Debug.LogError("OpenVR.Input.GetActionHandle error: " + err.ToString());
            }
        }

        public void UpdateInput(VRControllerState_t controllerState) {
            Vector2 joystickPosition = GetVector2(actionHandleJoystick);
            float joystickButton =
                GetBoolean(actionHandleJoystickPress) ? 1 :
                GetBoolean(actionHandleJoystickTouch) ? 0 : -1;
            joystick = new Vector3(joystickPosition.x, joystickPosition.y, joystickButton);

            Vector2 touchPadPosition = GetVector2(actionHandleTouchpad);
            float touchPadButton =
                GetBoolean(actionHandleTouchpadTouch) ?
                (GetBoolean(actionHandleTouchpadClick) ? 1 :
                GetFloat(actionHandleTouchpadPress)) : -1;
            touchpad = new Vector3(touchPadPosition.x, touchPadPosition.y, touchPadButton);

            float triggerPress = GetFloat(actionHandleTrigger);
            trigger =
                triggerPress == 0 && !GetBoolean(actionHandleTriggerTouch) ?
                -1 : triggerPress;

            float gripPress = GetBoolean(actionHandleGripClick) ? 1 : GetFloat(actionHandleGrip);
            grip =
                gripPress == 0 && !GetBoolean(actionHandleGripTouch) ?
                -1 : gripPress;
                //(GetBoolean(actionHandleGripClick) ? 1 : GetFloat(actionHandleGrip))
                // : -1;

            aButton =
                GetBoolean(actionHandleButtonA) ? 1 :
                GetBoolean(actionHandleButtonATouch) ? 0 : -1;
            bButton =
                GetBoolean(actionHandleButtonB) ? 1 :
                GetBoolean(actionHandleButtonBTouch) ? 0 : -1;
        }

        [System.NonSerialized]
        protected InputDigitalActionData_t digitalActionData = new InputDigitalActionData_t();
        [System.NonSerialized]
        protected readonly uint digitalActionDataSize = (uint)Marshal.SizeOf(typeof(InputDigitalActionData_t));

        protected bool GetBoolean(ulong actionHandle) {
            EVRInputError err;

            err = OpenVR.Input.GetDigitalActionData(actionHandle, ref digitalActionData, digitalActionDataSize, 0);
            if (err != EVRInputError.None)
                return false;

            return digitalActionData.bState;
        }

        [System.NonSerialized]
        protected InputAnalogActionData_t analogActionData = new InputAnalogActionData_t();
        [System.NonSerialized]
        protected readonly uint analogActionDataSize = (uint)Marshal.SizeOf(typeof(InputAnalogActionData_t));

        protected float GetFloat(ulong actionHandle) {
            EVRInputError err;

            err = OpenVR.Input.GetAnalogActionData(actionHandle, ref analogActionData, analogActionDataSize, 0);
            if (err != EVRInputError.None)
                return 0;

            return analogActionData.x;
        }

        protected Vector2 GetVector2(ulong actionHandle) {
            EVRInputError err;

            err = OpenVR.Input.GetAnalogActionData(actionHandle, ref analogActionData, analogActionDataSize, 0);
            if (err != EVRInputError.None)
                return Vector2.zero;

            Vector2 v = new Vector2(analogActionData.x, analogActionData.y);
            return v;
        }

        protected Vector3 GetVector3(ulong actionHandle) {
            EVRInputError err;

            err = OpenVR.Input.GetAnalogActionData(actionHandle, ref analogActionData, analogActionDataSize, 0);
            if (err != EVRInputError.None)
                return Vector3.zero;

            Vector3 v = new Vector3(analogActionData.x, analogActionData.y, analogActionData.z);
            return v;
        }
#endif
    }
}