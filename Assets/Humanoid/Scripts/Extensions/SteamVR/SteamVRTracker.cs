﻿using UnityEngine;

namespace Passer.Humanoid {

    public class SteamVRTracker : MonoBehaviour {
#if hSTEAMVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        public SteamVRHumanoidTracker tracker = new SteamVRHumanoidTracker();

        private void Start() {
            tracker.trackerTransform = this.transform;
            tracker.StartTracker();
        }

        private void Update() {
            tracker.UpdateTracker();
        }
#endif
    }
}