﻿#if hSTEAMVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Passer.Humanoid {
    using Tracking;

    /// <summary>The SteamVR tracker</summary>
    [System.Serializable]
    public class SteamVRHumanoidTracker : HumanoidTracker {
        public SteamVRHumanoidTracker() {
            deviceView = new DeviceView();
        }

        /// <summary>The name of this tracker</summary>
        public override string name {
            get { return SteamDevice.name; }
        }

        public override HeadSensor headSensor {
            get { return humanoid.headTarget.steamVR; }
        }
        public override ArmSensor leftHandSensor {
            get { return humanoid.leftHandTarget.steamVR; }
        }
        public override ArmSensor rightHandSensor {
            get { return humanoid.rightHandTarget.steamVR; }
        }

        [System.NonSerialized]
        private UnitySensor[] _sensors;
        public override UnitySensor[] sensors {
            get {
                if (_sensors == null)
                    _sensors = new UnitySensor[] {
                        headSensor,
                        leftHandSensor,
                        rightHandSensor
                    };

                return _sensors;
            }
        }

        public SteamVRHmd hmd = null;
        public List<SteamVRController> controllers = new List<SteamVRController>();

#if hVIVETRACKER
        public HeadSensor headSensorVive {
            get { return humanoid.headTarget.viveTracker; }
        }
        public ArmSensor leftHandSensorVive {
            get { return humanoid.leftHandTarget.viveTracker; }
        }
        public ArmSensor rightHandSensorVive {
            get { return humanoid.rightHandTarget.viveTracker; }
        }
        public TorsoSensor hipsSensorVive {
            get { return humanoid.hipsTarget.viveTracker; }
        }
        public LegSensor leftFootSensorVive {
            get { return humanoid.leftFootTarget.viveTracker; }
        }
        public LegSensor rightFootSensorVive {
            get { return humanoid.rightFootTarget.viveTracker; }
        }

        public List<ViveTrackerComponent> viveTrackers = new List<ViveTrackerComponent>();
#endif

        ulong pHandle;

        [System.NonSerialized]
        protected VRActiveActionSet_t actionSet = new VRActiveActionSet_t();
        [System.NonSerialized]
        protected static uint activeActionSetSize;
        [System.NonSerialized]
        protected VRActiveActionSet_t[] activeActionSets;

        public override bool AddTracker(HumanoidControl humanoid, string resourceName) {
            bool trackerAdded = base.AddTracker(humanoid, resourceName);
            if (trackerAdded) {
                /*SteamVRTrackerComponent trackerComponent = */
                trackerTransform.gameObject.AddComponent<SteamVRTracker>();
            }
            return trackerAdded;
        }

        #region Start
        public override void StartTracker(HumanoidControl _humanoid) {
            humanoid = _humanoid;

            if (humanoid.headTarget.unity.enabled && UnityVRDevice.xrDevice == UnityVRDevice.XRDeviceType.OpenVR)
                enabled = true;

            if (!enabled || UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.OpenVR)
                return;

            TraditionalDevice.gameControllerEnabled = false;
            // Game controllers interfere with SteamVR Controller Input ... :-(

            SteamDevice.Start();

            AddTracker(humanoid, "SteamVR");

#if UNITY_EDITOR
            string path = Application.dataPath + humanoid.path + "Extensions/SteamVR/actions.json";
#else
            string path = Application.dataPath + "/../actions.json";
#endif
            Debug.Log(path);
            EVRInputError err = OpenVR.Input.SetActionManifestPath(path);
            if (err != EVRInputError.None)
                Debug.LogError("OpenVR.Input.SetActionManifestPath error (" + path + "): " + err.ToString());
            else {
                err = OpenVR.Input.GetActionSetHandle("/actions/default", ref pHandle);
                if (err != EVRInputError.None)
                    Debug.LogError("OpenVR.Input.GetActionSetHandle error (): " + err.ToString());
                else
                    actionSet.ulActionSet = pHandle;
            }
            activeActionSets = new VRActiveActionSet_t[] { actionSet };
            activeActionSetSize = (uint)(Marshal.SizeOf(typeof(VRActiveActionSet_t)));

            SteamDevice.onNewSensor = OnNewSensor;
#if hVIVETRACKER
            Debug.Log("Detecting Vive Tracker positions.\nMake sure the Vive HMD is looking in the same direction as the user!");
#endif
        }

        public void StartTracker() {
            if (!enabled || UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.OpenVR)
                return;

            TraditionalDevice.gameControllerEnabled = false;
            // Game controllers interfere with SteamVR Controller Input ... :-(

            SteamDevice.Start();
        }

        protected virtual void OnNewSensor(uint sensorId) {
            ETrackedDeviceClass deviceClass = OpenVR.System.GetTrackedDeviceClass(sensorId);
            switch (deviceClass) {
                case ETrackedDeviceClass.HMD:
                    hmd = FindHmd(sensorId);
                    if (hmd == null) 
                        hmd = SteamVRHmd.NewHmd(humanoid, (int)sensorId);                    
                    break;
                case ETrackedDeviceClass.TrackingReference:
                    SubTracker subTracker = FindLighthouse(sensorId);
                    if (subTracker == null)
                        subTrackers.Add(NewLighthouse(humanoid, sensorId));                    
                    break;
                case ETrackedDeviceClass.Controller:
                    SteamVRController controller = FindController(sensorId);
                    if (controller == null) 
                        controllers.Add(SteamVRController.NewController(humanoid, (int)sensorId));                    
                    break;
#if hVIVETRACKER
                case ETrackedDeviceClass.GenericTracker:
                    ViveTrackerComponent viveTracker = FindViveTracker(sensorId);
                    if (viveTracker == null) 
                        viveTrackers.Add(ViveTracker.NewViveTracker(humanoid, sensorId));                    
                    break;
#endif
                default:
                    break;
            }
        }

        protected SubTracker NewLighthouse(HumanoidControl humanoid, uint sensorId) {
            SubTracker subTracker = SteamVRSubTracker.Create(this);
            subTracker.subTrackerId = (int)sensorId;
            return subTracker;
        }

        protected SteamVRHmd FindHmd(uint sensorId) {
            if (hmd != null && hmd.trackerId == sensorId)
                return hmd;

            SteamVRHead steamVRHead = humanoid.headTarget.steamVR;
            if (steamVRHead.hmd != null) {
                steamVRHead.hmd.trackerId = (int)sensorId;
                hmd = steamVRHead.hmd;
                return hmd;
            }

            // See if a HMD already exists in the Real World
            SteamVRHmd steamVRHmd = trackerTransform.GetComponentInChildren<SteamVRHmd>();
            if (steamVRHmd != null) {
                steamVRHmd.trackerId = (int)sensorId;
                hmd = steamVRHmd;
                steamVRHead.hmd = steamVRHmd;
                return hmd;
            }

            return null;
        }

        protected SteamVRController FindController(uint sensorId) {
            foreach (SteamVRController controller in controllers) {
                if (controller != null && controller.trackerId == sensorId)
                    return controller;
            }
            if (SteamVRController.IsLeftController(sensorId)) {
                SteamVRHand leftHand = humanoid.leftHandTarget.steamVR;
                if (leftHand != null && leftHand.steamVrController != null) {
                    leftHand.steamVrController.trackerId = (int)sensorId;
                    // leftHand Controller was not in controller list yet
                    controllers.Add(leftHand.steamVrController);
                    return leftHand.steamVrController;
                }
            }
            else
            if (SteamVRController.IsRightController(sensorId)) {
                SteamVRHand rightHand = humanoid.rightHandTarget.steamVR;
                if (rightHand != null && rightHand.steamVrController != null) {
                    rightHand.steamVrController.trackerId = (int)sensorId;
                    // rightHand Controller was not in controller list yet
                    controllers.Add(rightHand.steamVrController);
                    return rightHand.steamVrController;
                }
            }

            // See if the controller already exists in the Real World
            SteamVRController[] steamVRControllers = trackerTransform.GetComponentsInChildren<SteamVRController>();
            foreach (SteamVRController controller in steamVRControllers) {
                if (controller.trackerId == sensorId) {
                    // controller was not in controller list yet
                    controllers.Add(controller);
                    return controller;
                }
            }

            return null;
        }

#if hVIVETRACKER
        protected ViveTrackerComponent FindViveTracker(uint sensorId) {
            foreach (ViveTrackerComponent viveTracker in viveTrackers) {
                if (viveTracker != null && viveTracker.trackerId == sensorId)
                    return viveTracker;
            }
            return null;
        }
#endif

#endregion

        public override void ShowTracker(bool shown) {
            if (!enabled)
                return;
#if hVIVETRACKER
            if (humanoid == null)
                return;

            ViveTracker.ShowTracker(humanoid, shown);
#endif
        }


#region Update
        public override void UpdateTracker() {
            if (!enabled || UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.OpenVR) {
                status = Status.Unavailable;
                return;
            }

            status = SteamDevice.status;

            SteamDevice.Update();
            OpenVR.Input.UpdateActionState(activeActionSets, activeActionSetSize);

            deviceView.position = HumanoidTarget.ToVector(trackerTransform.position);
            deviceView.orientation = HumanoidTarget.ToRotation(trackerTransform.rotation);

            bool showRealObjects = humanoid == null ? true : humanoid.showRealObjects;

            if (hmd != null)
                hmd.UpdateComponent();
            foreach (SubTracker subTracker in subTrackers)
                subTracker.UpdateTracker(showRealObjects);
            foreach (SteamVRController controller in controllers)
                controller.UpdateComponent();
#if hVIVETRACKER
            foreach (ViveTrackerComponent viveTracker in viveTrackers)
                viveTracker.UpdateComponent();
#endif
        }

        protected SubTracker FindLighthouse(uint sensorId) {
            foreach (SubTracker subTracker in subTrackers) {
                if (subTracker != null && subTracker.subTrackerId == sensorId)
                    return subTracker;
            }
            return null;
        }

        private bool IsTracking() {
            if (!humanoid.leftHandTarget.steamVR.enabled || humanoid.leftHandTarget.steamVR.status == Status.Tracking ||
#if hVIVETRACKER
                humanoid.headTarget.viveTracker.status == Status.Tracking ||
                humanoid.leftHandTarget.viveTracker.status == Status.Tracking ||
                humanoid.rightHandTarget.viveTracker.status == Status.Tracking ||
                humanoid.leftHandTarget.viveTracker.status == Status.Tracking ||
                humanoid.hipsTarget.viveTracker.status == Status.Tracking ||
                humanoid.leftFootTarget.viveTracker.status == Status.Tracking ||
                humanoid.rightFootTarget.viveTracker.status == Status.Tracking ||
#endif
                !humanoid.rightHandTarget.steamVR.enabled || humanoid.rightHandTarget.steamVR.status == Status.Tracking)
                return true;
            else
                return false;
        }
#endregion

        public static GameObject CreateControllerObject() {
            GameObject trackerPrefab = Resources.Load("Vive Controller") as GameObject;
            if (trackerPrefab == null)
                return null;

            GameObject trackerObject = Object.Instantiate(trackerPrefab);
            trackerObject.name = "Vive Controller";

            return trackerObject;
        }

        public override void Calibrate() {
            SteamDevice.ResetSensors();
        }

        public override void AdjustTracking(Vector3 v, Quaternion q) {
            if (trackerTransform != null) {
                trackerTransform.position += v;
                trackerTransform.rotation *= q;
            }
        }

        public Transform GetTrackingTransform() {
            if (!enabled ||
                subTrackers == null ||
                subTrackers.Count <= 0 ||
                subTrackers[0] == null ||
                subTrackers[0].subTrackerId == -1) {

                return null;
            }

            return subTrackers[0].transform;
        }

        public void SyncTracking(Vector3 position, Quaternion rotation) {
            if (!enabled)
                return;


            // rotation

            // Not stable
            //Quaternion deltaRotation = Quaternion.Inverse(lighthouses[0].transform.rotation) * rotation;
            //unityVRroot.rotation *= deltaRotation;

            // stable
            float angle = (-subTrackers[0].transform.eulerAngles.y) + rotation.eulerAngles.y;
            trackerTransform.Rotate(Vector3.up, angle, Space.World);

            // position
            Vector3 deltaPosition = position - subTrackers[0].transform.position;

            trackerTransform.Translate(deltaPosition, Space.World);
        }
    }
}
#endif