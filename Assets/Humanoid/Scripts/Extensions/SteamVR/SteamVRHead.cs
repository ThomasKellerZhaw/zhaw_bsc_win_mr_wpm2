﻿#if hSTEAMVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
using UnityEngine;

namespace Passer.Humanoid {

    [System.Serializable]
    public class SteamVRHead : HeadSensor {
        public override string name {
            get { return "SteamVR HMD"; }
        }

        public SteamVRHmd hmd;

        public override Tracker.Status status {
            get {
                if (hmd == null)
                    return Tracker.Status.Unavailable;
                return hmd.status;
            }
            set { hmd.status = value; }
        }

#region Start
        public override void Init(HeadTarget headTarget) {
            base.Init(headTarget);
            if (headTarget.humanoid != null)
                tracker = headTarget.humanoid.steam;
        }

        public override void Start(HumanoidControl _humanoid, Transform targetTransform) {
            base.Start(_humanoid, targetTransform);
            tracker = headTarget.humanoid.steam;

            if (tracker == null || !tracker.enabled || !enabled)
                return;

            SetSensor2Target();
            CheckSensorTransform();
            sensor2TargetPosition = -headTarget.head2eyes;

            if (sensorTransform != null) {
                hmd = sensorTransform.GetComponent<SteamVRHmd>();
                if (hmd != null)
                    hmd.StartComponent(tracker.trackerTransform);
            }
        }

        protected override void CreateSensorTransform() {
            CreateSensorTransform("SteamVR HMD", headTarget.head2eyes, Quaternion.identity);
            SteamVRHmd steamVrHmd = sensorTransform.GetComponent<SteamVRHmd>();
            if (steamVrHmd == null)
                sensorTransform.gameObject.AddComponent<SteamVRHmd>();
        }
#endregion

#region Update
        protected bool calibrated = false;

        public override void Update() {
            if (tracker == null || !tracker.enabled || !enabled)
                return;

            if (hmd == null) {
                hmd = sensorTransform.GetComponent<SteamVRHmd>();
                UpdateTarget(headTarget.head.target, sensorTransform);
            }

            hmd.UpdateComponent();
            if (hmd.status != Tracker.Status.Tracking)
                return;

            UpdateTarget(headTarget.head.target, hmd);
            UpdateNeckTargetFromHead();

            if (!calibrated && tracker.humanoid.calibrateAtStart) {
                tracker.humanoid.Calibrate();
                calibrated = true;
            }

            UpdateTarget(headTarget.head.target, hmd);
        }
#endregion
    }
}
#endif