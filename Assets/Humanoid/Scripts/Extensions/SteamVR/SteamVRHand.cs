﻿#if hSTEAMVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
using UnityEngine;

namespace Passer.Humanoid {
    using Tracking;

    [System.Serializable]
    public class SteamVRHand : ArmController {
        public override string name {
            get { return "Steam VR Controller"; }
        }

        public SteamVRController steamVrController;

        public override Tracker.Status status {
            get {
                if (steamVrController == null)
                    return Tracker.Status.Unavailable;
                return steamVrController.status;
            }
            set { steamVrController.status = value; }
        }

        public SteamVRController.ControllerType controllerType;

        #region Start
        public override void Init(HandTarget handTarget) {
            base.Init(handTarget);
            tracker = handTarget.humanoid.steam;
        }

        public override void Start(HumanoidControl _humanoid, Transform targetTransform) {
            base.Start(_humanoid, targetTransform);
            tracker = handTarget.humanoid.steam;

            if (tracker == null || !tracker.enabled || !enabled)
                return;

            SetSensor2Target();
            CheckSensorTransform();
            ShowSensor(handTarget.humanoid.showRealObjects && target.showRealObjects);

            if (sensorTransform != null) {
                steamVrController = sensorTransform.GetComponent<SteamVRController>();
                if (steamVrController != null)
                    steamVrController.StartComponent(tracker.trackerTransform);
            }
        }

        public override void RefreshSensor() {
            if (steamVrController != null) {
#if UNITY_EDITOR
                Object.DestroyImmediate(steamVrController, true);
#else
                Object.Destroy(steamVrController);
#endif
            }

            CreateSensorTransform();
        }

        protected override void CreateSensorTransform() {
            string prefabLeftName = "Vive Controller";
            string prefabRightName = "Vive Controller";
            switch (controllerType) {
                case SteamVRController.ControllerType.SteamVRKnuckles:
                    break;
                case SteamVRController.ControllerType.MixedReality:
                    prefabLeftName = "Windows MR Controller Left";
                    prefabRightName = "Windows MR Controller Right";
                    break;
                case SteamVRController.ControllerType.OculusTouch:
                    prefabLeftName = "Left Touch Controller";
                    prefabRightName = "Right Touch Controller";
                    break;
                case SteamVRController.ControllerType.SteamVRController:
                default:
                    break;
            }

            if (handTarget.isLeft)
                CreateSensorTransform(prefabLeftName, new Vector3(-0.14F, -0.04F, 0.08F), Quaternion.Euler(0, -30, -90));
            else
                CreateSensorTransform(prefabRightName, new Vector3(0.14F, -0.04F, 0.08F), Quaternion.Euler(0, 30, 90));

            steamVrController = sensorTransform.GetComponent<SteamVRController>();
            if (steamVrController == null)
                steamVrController = sensorTransform.gameObject.AddComponent<SteamVRController>();
            steamVrController.isLeft = handTarget.isLeft;
        }
        #endregion

        #region Update
        public override void Update() {
            if (tracker == null || !tracker.enabled || !enabled)
                return;

            if (steamVrController == null) {
                UpdateTarget(handTarget.hand.target, sensorTransform);
                return;
            }

            steamVrController.UpdateComponent();
            if (steamVrController.status != Tracker.Status.Tracking)
                return;

            UpdateTarget(handTarget.hand.target, steamVrController);
            UpdateInput();
            UpdateHand();
        }

        protected void UpdateInput() {
            if (handTarget.isLeft)
                SetControllerInput(controllerInput.left);
            else
                SetControllerInput(controllerInput.right);
        }

        protected void SetControllerInput(ControllerSide controllerSide) {
            controllerSide.stickHorizontal += Mathf.Clamp(steamVrController.joystick.x + steamVrController.touchpad.x, -1, 1);
            controllerSide.stickVertical += Mathf.Clamp(steamVrController.joystick.y + steamVrController.touchpad.y, -1, 1);
            controllerSide.stickButton |= (steamVrController.joystick.z > 0.5F) || (steamVrController.touchpad.z > 0.5F);

            controllerSide.buttons[0] |= steamVrController.aButton > 0.5F;
            controllerSide.buttons[1] |= steamVrController.bButton > 0.5F;

            controllerSide.trigger1 += steamVrController.trigger;
            controllerSide.trigger2 += steamVrController.grip;

            controllerSide.option |= steamVrController.aButton > 0.5F;
        }

        protected void UpdateHand() {
            for (int i = 0; i < (int)Finger.Count; i++)
                UpdateFinger(handTarget.fingers.allFingers[i], i);
        }

        private void UpdateFinger(FingersTarget.TargetedFinger finger, int fingerIx) {
            //finger.proximal.target.transform.localRotation = GetFingerRotation(steamVrController, fingerIx, 0);
            //finger.intermediate.target.transform.localRotation = GetFingerRotation(steamVrController, fingerIx, 1);
            //finger.distal.target.transform.localRotation = GetFingerRotation(steamVrController, fingerIx, 2);
            UpdateFingerRotation(finger.proximal.target.transform, steamVrController, fingerIx, 0);
            UpdateFingerRotation(finger.intermediate.target.transform, steamVrController, fingerIx, 1);
            UpdateFingerRotation(finger.distal.target.transform, steamVrController, fingerIx, 2);
        }

        protected Quaternion GetFingerRotation(SteamVRController steamVRController, int fingerIx, int boneIx) {
            int ix = fingerIx * 5 + boneIx + 2;
            VRBoneTransform_t boneTransform = steamVRController.tempBoneTransforms[ix];

            Quaternion q = new Quaternion(
                boneTransform.orientation.x,
                boneTransform.orientation.y,
                boneTransform.orientation.z,
                boneTransform.orientation.w
                );
            
            if (!handTarget.isLeft)
                q = new Quaternion(q.x, -q.y, -q.z, q.w);
            if (fingerIx == 0) {
                if (boneIx == 0) {
                    q = Rotations.Rotate(q, Quaternion.Euler(90, 0, 0));
                    if (handTarget.isLeft)
                        q = Quaternion.Euler(0, -180, -90) * q;
                    else
                        q = Quaternion.Euler(180, -180, -90) * q;
                }
                else
                    q = Rotations.Rotate(q, Quaternion.Euler(90, 0, 0));
            }
            return q;
        }

        protected void UpdateFingerRotation(Transform phalanxTransform, SteamVRController steamVRController, int fingerIx, int boneIx) {
            int ix = fingerIx * 5 + boneIx + 2;
            VRBoneTransform_t boneTransform = steamVRController.tempBoneTransforms[ix];

            Quaternion q = new Quaternion(
                boneTransform.orientation.x,
                boneTransform.orientation.y,
                boneTransform.orientation.z,
                boneTransform.orientation.w
                );
            if (q.x + q.y + q.z + q.w == 0) {
                // No valid rotation
                return;
            }

            if (!handTarget.isLeft)
                q = new Quaternion(q.x, -q.y, -q.z, q.w);
            if (fingerIx == 0) {
                if (boneIx == 0) {
                    q = Rotations.Rotate(q, Quaternion.Euler(90, 0, 0));
                    if (handTarget.isLeft)
                        q = Quaternion.Euler(0, -180, -90) * q;
                    else
                        q = Quaternion.Euler(180, -180, -90) * q;
                }
                else
                    q = Rotations.Rotate(q, Quaternion.Euler(90, 0, 0));
            }
            phalanxTransform.localRotation = q;
        }

        #endregion

        //length is how long the vibration should go for in seconds
        //strength is vibration strength from 0-1
        public override void Vibrate(float length, float strength) {
            if (tracker == null || !tracker.enabled || !enabled)
                return;

            for (float i = 0; i < length; i += Time.deltaTime)
                controller.Vibrate(length, strength);
        }
    }
}

#endif