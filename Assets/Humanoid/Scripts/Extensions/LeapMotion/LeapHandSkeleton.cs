﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if hLEAP
using Leap.Unity;
#endif

namespace Passer.Humanoid {
	using Tracking;

	public class LeapHandSkeleton : SensorComponent {
#if hLEAP
		public bool isLeft;
		public bool showSkeleton;

		protected List<Transform> bones;

		private LeapProvider leapProvider;

		protected static Material boneWhite;
		protected void InitalizeSkeleton() {
			// Should become XRserviceProvider for VR...
			AddServiceProvider();

			if (leapProvider == null) {
				leapProvider = Leap.Unity.Hands.Provider;
			}

			status = Tracker.Status.Unavailable;
			leapProvider.OnUpdateFrame -= OnUpdateFrame;
			leapProvider.OnUpdateFrame += OnUpdateFrame;


			boneWhite = new Material(Shader.Find("Standard")) {
				color = new Color(1, 1, 1)
			};

			bones = new List<Transform>(new Transform[(int)LeapDevice.BoneId.Count]);

			//bones[(int)LeapDevice.BoneId.Hand] = AddBone("Hand", this.transform);

			for (int i = 0; i < (int)Finger.Count; i++) {
				Transform parent = this.transform; // bones[(int)LeapDevice.BoneId.Hand];
				for (int j = (int)FingerBone.Proximal; j <= (int)FingerBone.Tip; j++) {
					LeapDevice.BoneId boneId = LeapDevice.BoneId.ThumbProximal + i * 4 + (j - 1);
					bones[(int)boneId] = AddBone(boneId.ToString(), parent);
					parent = bones[(int)boneId];
				}
			}
		}

		private void AddServiceProvider() {
			Transform cameraTransform = this.transform.parent;
			if (cameraTransform != null) {
				Leap.Unity.LeapServiceProvider serviceProvider = cameraTransform.GetComponent<Leap.Unity.LeapServiceProvider>();
				if (serviceProvider == null)
					cameraTransform.gameObject.AddComponent<LeapServiceProvider>();
			}
		}

		protected Transform AddBone(string name, Transform parent) {
			GameObject boneGO = new GameObject(name);

			if (showSkeleton) {
				LineRenderer boneRenderer = boneGO.AddComponent<LineRenderer>();
				boneRenderer.startWidth = 0.01F;
				boneRenderer.endWidth = 0.01F;
				boneRenderer.useWorldSpace = false;
				boneRenderer.SetPosition(0, Vector3.zero);
				boneRenderer.SetPosition(1, Vector3.zero);
#if UNITY_2017_1_OR_NEWER
                        boneRenderer.generateLightingData = true;
#endif
				boneRenderer.material = boneWhite;
			}

			boneGO.transform.SetParent(parent, false);

			return boneGO.transform;
		}

		public override void UpdateComponent() {
			//base.UpdateComponent();

			if (bones == null)
				InitalizeSkeleton();
			if (bones == null)
				status = Tracker.Status.Unavailable;

			if (showSkeleton)
				UpdateSkeletonRender();
		}

		protected void UpdateSkeletonBones(Leap.Hand leapHand) {
			if (leapHand == null)
				return;

			this.transform.position = leapHand.WristPosition.ToVector3();
			this.transform.rotation = leapHand.Rotation.ToQuaternion() * Quaternion.AngleAxis(isLeft ? 90 : -90, Vector3.up);

			UpdateLeapFingers(leapHand);

			status = Tracker.Status.Tracking;
		}

		private void UpdateLeapFingers(Leap.Hand leapHand) {
			for (int i = 0; i < (int)Finger.Count; i++)
				UpdateLeapFinger(leapHand, i);
		}

		private void UpdateLeapFinger(Leap.Hand leapHand, int fingerIx) {
			LeapDevice.BoneId boneId = LeapDevice.BoneId.ThumbProximal + fingerIx * 4;

			Leap.Finger leapFinger = leapHand.Fingers[fingerIx];

			bones[(int)boneId].position = leapFinger.Bone(Leap.Bone.BoneType.TYPE_METACARPAL).NextJoint.ToVector3();
			Quaternion proximalRotation = leapFinger.Bone(Leap.Bone.BoneType.TYPE_PROXIMAL).Rotation.ToQuaternion();
			bones[(int)boneId].rotation = proximalRotation * Quaternion.AngleAxis(isLeft ? 90 : -90, Vector3.up);
	
			bones[(int)boneId + 1].position = leapFinger.Bone(Leap.Bone.BoneType.TYPE_PROXIMAL).NextJoint.ToVector3();
			Quaternion intermediateRotation = leapFinger.Bone(Leap.Bone.BoneType.TYPE_INTERMEDIATE).Rotation.ToQuaternion();
			bones[(int)boneId + 1].rotation = intermediateRotation * Quaternion.AngleAxis(isLeft ? 90 : -90, Vector3.up);

			bones[(int)boneId + 2].position = leapFinger.Bone(Leap.Bone.BoneType.TYPE_INTERMEDIATE).NextJoint.ToVector3();
			Quaternion distalRotation = leapFinger.Bone(Leap.Bone.BoneType.TYPE_DISTAL).Rotation.ToQuaternion();
			bones[(int)boneId + 2].rotation = distalRotation * Quaternion.AngleAxis(isLeft ? 90 : -90, Vector3.up);

			bones[(int)boneId + 3].position = leapFinger.Bone(Leap.Bone.BoneType.TYPE_DISTAL).NextJoint.ToVector3();
		}

		protected void UpdateSkeletonRender() {
			if (bones == null)
				return;

			// Render Skeleton
			foreach (Transform bone in bones) {
				if (bone == null)
					continue;
				LineRenderer boneRenderer = bone.GetComponent<LineRenderer>();
				Vector3 localParentPosition = bone.InverseTransformPoint(bone.parent.position);
				boneRenderer.SetPosition(1, localParentPosition);
			}
		}

		private void OnUpdateFrame(Leap.Frame frame) {
			if (frame == null)
				return;

			for (int i = 0; i < frame.Hands.Count; i++) {
				Leap.Hand curHand = frame.Hands[i];
				if (isLeft == curHand.IsLeft) {
					UpdateSkeletonBones(curHand);
					return;
				}
			}
		}

		public Transform GetBone(Finger finger, FingerBone fingerBone) {
			if (bones == null)
				return null;

			LeapDevice.BoneId boneId = LeapDevice.GetBoneId(finger, fingerBone);
			if (boneId == LeapDevice.BoneId.Invalid)
				return null;

			return bones[(int)boneId];
		}
#endif
	}

}