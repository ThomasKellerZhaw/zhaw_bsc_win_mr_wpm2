﻿using UnityEngine;

namespace Passer.Humanoid {

    public class HandSocket : Socket {

        public HandTarget handTarget;

        protected override void MoveHandleToSocket(Transform socketTransform, Handle handle) {
            //Debug.Log("MoveHandleToHand");
            Transform handleTransform = handle.GetComponent<Transform>();
            Rigidbody handleRigidbody = handle.GetComponentInParent<Rigidbody>();
            if (handleRigidbody != null)
                handleTransform = handleRigidbody.transform;

            handleTransform.rotation = handle.RotationTo(socketTransform.rotation) * handleTransform.rotation;
            handleTransform.position += handle.TranslationTo(socketTransform.position);
        }

        protected override void MoveSocketToHandle(Transform socketTransform, Handle handle) {
            //Debug.Log("MoveHandToHandle");
            if (handle.grabType == Handle.GrabType.RailGrab) {

                // Project Socket rotation on rail

                Vector3 handleYaxis = handle.transform.up;
                Vector3 socketYaxis = handTarget.grabSocket.transform.up;
                float angle = Vector3.Angle(handleYaxis, socketYaxis);                
                if (angle > 90) 
                    socketYaxis = -handTarget.grabSocket.transform.up;

                Quaternion socketToHandleRotation = Quaternion.FromToRotation(socketYaxis, handleYaxis);
                Quaternion targetRotation = socketToHandleRotation * handTarget.grabSocket.transform.rotation;

                //Debug.DrawRay(handle.transform.position, handle.transform.rotation * Vector3.up, Color.blue);
                //Debug.DrawRay(handTarget.grabSocket.transform.position, handTarget.grabSocket.transform.rotation * Vector3.up, Color.green);
                //Debug.DrawRay(handTarget.grabSocket.transform.position, targetRotation * Vector3.up);
                //Debug.Break();
                
                Quaternion socket2handRotation = Quaternion.Inverse(handTarget.grabSocket.transform.localRotation);
                handTarget.hand.bone.transform.rotation = targetRotation * socket2handRotation;

                // Project Socket on Rail

                // Socket along rail
                Vector3 localSocketPosition = handTarget.grabSocket.transform.position - handle.transform.position;
                Vector3 targetPosition = Vector3.Project(localSocketPosition, handle.transform.up);
                //Debug.DrawRay(handle.transform.position, handle.transform.up, Color.green);
                //Debug.DrawRay(handle.transform.position, targetPosition, Color.magenta);

                // Socket within rail length
                float maxDistance = handle.transform.lossyScale.y / 2;
                float distance = Mathf.Clamp(targetPosition.magnitude, -maxDistance, maxDistance);
                float scale = distance / targetPosition.magnitude;
                targetPosition = Vector3.Scale(targetPosition, Vector3.one * scale);
                //Debug.DrawRay(handle.transform.position, targetPosition, Color.cyan);

                targetPosition = handle.transform.position + targetPosition;
                //Debug.DrawLine(handTarget.grabSocket.transform.position, targetPosition);

                Vector3 socket2HandPosition = handTarget.hand.bone.transform.position - handTarget.grabSocket.transform.position;
                handTarget.hand.bone.transform.position = targetPosition + socket2HandPosition;                
            }
            else {
                Quaternion socket2handRotation = Quaternion.Inverse(handTarget.grabSocket.transform.localRotation);
                handTarget.hand.bone.transform.rotation = handle.worldRotation * socket2handRotation;

                Vector3 socket2HandPosition = handTarget.hand.bone.transform.position - handTarget.grabSocket.transform.position;
                handTarget.hand.bone.transform.position = handle.worldPosition + socket2HandPosition;
            }
        }

        protected override void MassRedistribution(Rigidbody socketRigidbody, Rigidbody objRigidbody) {
            originalMass = socketRigidbody.mass;
            socketRigidbody.mass = objRigidbody.mass;
        }

        #region Attach

        #region Rigidbody

        protected override bool AttachRigidbody(Rigidbody objRigidbody, Handle handle, bool rangeCheck = true) {
            //Debug.Log("AttachRigidbody");

            float grabDistance = Vector3.Distance(this.transform.position, handle.worldPosition);
            if (rangeCheck && handle.range > 0 && grabDistance > handle.range) {
                //Debug.Log("Socket is outside range of handle");
                return false;
            }

            Transform objTransform = objRigidbody.transform;

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            Joint joint = objRigidbody.GetComponent<Joint>();
            // See if these joints are being destroyed
            DestroyedJoints destroyedJoints = objRigidbody.GetComponent<DestroyedJoints>();

            if (objRigidbody.isKinematic
                || (objRigidbody == handTarget.otherHand.handRigidbody && handTarget.otherHand.handPhysics != null)
                ) {
                if (thisRigidbody == null)
                    AttachRigidbodyParenting(objRigidbody, handle);
                else if (thisRigidbody.isKinematic)
                    AttachTransformParenting(objRigidbody.transform, handle);
                else
                    AttachSocketParenting(objRigidbody, handle, thisRigidbody);
            }
            else if (thisRigidbody == null) {
                AttachRigidbodyReverseJoint(objRigidbody, handle);
            }
            else if (
                (joint != null && destroyedJoints == null) ||
                objRigidbody.constraints != RigidbodyConstraints.None
                //|| handTarget.otherHand.handPhysics != null
                ) {

                AttachRigidbodyJoint(objRigidbody, handle);
            }
            else {
                AttachRigidbodyParenting(objRigidbody, handle);
            }

            releasingTransform = null;
            attachedTransform = objTransform;
            handle.socket = this;
            return true;
        }

        protected override void AttachRigidbodyJoint(Rigidbody objRigidbody, Handle handle) {
            Debug.Log("AttachRigidbodyJoint " + objRigidbody);

            //MassRedistribution(thisRididbody, objRigidbody);

            MoveHandleToSocket(this.transform, handle);

            ConfigurableJoint joint = handTarget.handRigidbody.gameObject.AddComponent<ConfigurableJoint>();
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;

            joint.angularXMotion = ConfigurableJointMotion.Locked;
            joint.angularYMotion = ConfigurableJointMotion.Locked;
            joint.angularZMotion = ConfigurableJointMotion.Locked;

            joint.projectionMode = JointProjectionMode.PositionAndRotation;
            joint.projectionDistance = 0.01F;
            joint.projectionAngle = 1;

            Collider c = objRigidbody.transform.GetComponentInChildren<Collider>();
            joint.connectedBody = c.attachedRigidbody;

            attachedTransform = objRigidbody.transform;
            attachedHandle = handle;
            handle.socket = this;
        }

        #endregion

        #region Static

        protected override void AttachStaticJoint(Transform objTransform) {
            Debug.Log("AttachStaticJoint");
            // Joint is no longer necessary, because the constraint makes sure the hand cannot move

            //FixedJoint joint = handTarget.handRigidbody.gameObject.AddComponent<FixedJoint>();

            //Collider c = objTransform.GetComponentInChildren<Collider>();
            //if (c == null)
            //    c = objTransform.GetComponentInParent<Collider>();
            //joint.connectedBody = c.attachedRigidbody;

            handTarget.handRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }

        #endregion

        #endregion

        #region Release

        #region Rigidbody

        protected override void ReleaseRigidbodyJoint() {
            Debug.Log("Release from Joint");

            Joint[] joints = handTarget.handRigidbody.GetComponents<Joint>();
            foreach (Joint joint in joints) {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    DestroyImmediate(joint, true);
                else
#endif
                    Destroy(joint);
            }
            //MassRestoration(..., ...);

            // Trick: when released and immediately attached to anther socket (e.g. grabbing)
            // the joints are not yet destroyed, because Destroy is executed with a delay.
            // Adding the DestroyedJoints component indicates that the joints which may
            // still be there are to be destroyed.
            attachedTransform.gameObject.AddComponent<DestroyedJoints>();
        }

        protected override void ReleaseSocketParenting(Rigidbody objRigidbody, Transform socketTransform) {
            //Debug.Log("ReleaseHandParenting");
            Rigidbody handRigidbody = RigidbodyDisabled.UnparentRigidbody(objRigidbody, socketTransform);
            handTarget.handRigidbody = handRigidbody;
        }

        #endregion

        #region Static

        protected override void ReleaseStaticObject() {
            Debug.Log("ReleaseStaticObject");

            Rigidbody thisRigidbody = handTarget.handRigidbody;
            RigidbodyDisabled thisDisabledRigidbody = this.GetComponent<RigidbodyDisabled>();

            if (thisRigidbody != null)
                ReleaseStaticJoint();
            else if (thisDisabledRigidbody != null)
                ReleaseSocketParenting(attachedTransform);
            else
                ReleaseTransformParenting();
        }

        protected override void ReleaseStaticJoint() {
            Debug.Log("ReleaseStaticJoint");

            // Joint is no longer necessary, because the constraint makes sure the hand cannot move

//            Joint[] joints = handTarget.handRigidbody.GetComponents<Joint>();
//            foreach (Joint joint in joints) {
//#if UNITY_EDITOR
//                DestroyImmediate(joint, true);
//#else
//                Destroy(joint);
//#endif
//            }
            handTarget.handRigidbody.constraints = RigidbodyConstraints.None;
        }

        #endregion

        #endregion

        protected override void MassRestoration(Rigidbody socketRigidbody, Rigidbody objRigidbody) {
            if (socketRigidbody != null)
                socketRigidbody.mass = originalMass;
        }

        public override Vector3 worldPosition {
            get {
                Vector3 handPosition = handTarget.hand.target.transform.position;
                Vector3 hand2Socket = handTarget.grabSocket.transform.position - handTarget.hand.bone.transform.position;
                Vector3 socketPosition = handPosition + hand2Socket;
                return socketPosition;
            }
        }

        public virtual Quaternion worldRotation {
            get {
                Quaternion handRotation = handTarget.hand.target.transform.rotation;
                Quaternion hand2Socket = Quaternion.Inverse(handTarget.hand.bone.targetRotation) * handTarget.grabSocket.transform.rotation;
                Quaternion socketRotation = handRotation * hand2Socket;
                return socketRotation;
            }
        }
    }
}