﻿using UnityEngine;

namespace Passer.Humanoid {

    [CreateAssetMenu(menuName = "Humanoid/Configuration", fileName = "HumanoidConfiguration", order = 100)]
    [System.Serializable]
    public class Configuration : ScriptableObject {
        public bool openVRSupport = false;
        public bool viveTrackerSupport = false;
        public bool oculusSupport = true;
        public bool windowsMRSupport = true;
        public bool vrtkSupport = true;
        public bool neuronSupport;
        public bool realsenseSupport;
        public bool leapSupport;
        public bool kinect1Support;
        public bool kinectSupport;
        public bool astraSupport;
        public bool hydraSupport;
        public bool arkitSupport;
        public bool tobiiSupport;
        public bool optitrackSupport;
        public bool pupilSupport;

        public Passer.Humanoid.NetworkingSystems networkingSupport;
    }
}