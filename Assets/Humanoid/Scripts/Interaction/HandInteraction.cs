﻿using UnityEngine;
#if hNW_UNET
using UnityEngine.Networking;
#endif

namespace Passer.Humanoid {

    [System.Serializable]
    public class HandInteraction {
        public const float kinematicMass = 1; // masses < 1 will move kinematic when not colliding
        public const float maxGrabbingMass = 10; // maxMass you can grab is 10

        #region Start

        public static void StartInteraction(HandTarget handTarget) {
            // Remote humanoids should not interact
            if (handTarget.humanoid.isRemote)
                return;

            // Gun Interaction pointer creates an Event System
            // First solve that before enabling this warning
            // because the warning will appear in standard Grocery Store demo scene

            handTarget.inputModule = handTarget.humanoid.GetComponent<InteractionModule>();
            if (handTarget.inputModule == null) {
                handTarget.inputModule = Object.FindObjectOfType<InteractionModule>();
                if (handTarget.inputModule == null) {
                    handTarget.inputModule = handTarget.humanoid.gameObject.AddComponent<InteractionModule>();
                }
            }

            handTarget.inputModule.EnableTouchInput(handTarget.humanoid, handTarget.isLeft, 0);
        }

        public static HandSocket CreateGrabSocket(HandTarget handTarget) {
            GameObject socketObj = new GameObject("Grab Socket");
            Transform socketTransform = socketObj.transform;
            socketTransform.parent = handTarget.hand.bone.transform;
            MoveToPalm(handTarget, socketTransform);

            HandSocket grabSocket = socketObj.AddComponent<HandSocket>();
            grabSocket.handTarget = handTarget;
            return grabSocket;
        }

        private static void MoveToPalm(HandTarget handTarget, Transform t) {
            if (handTarget.hand.bone.transform == null)
                return;

            Transform indexFingerBone = handTarget.fingers.index.proximal.bone.transform;
            Transform middleFingerBone = handTarget.fingers.middle.proximal.bone.transform;

            // Determine position
            Vector3 palmOffset;
            if (indexFingerBone)
                palmOffset = (indexFingerBone.position - handTarget.hand.bone.transform.position) * 0.9F;
            else if (middleFingerBone)
                palmOffset = (middleFingerBone.position - handTarget.hand.bone.transform.position) * 0.9F;
            else
                palmOffset = new Vector3(0.1F, 0, 0);

            t.position = handTarget.hand.bone.transform.position + palmOffset;

            Vector3 handUp = handTarget.hand.bone.targetRotation * Vector3.up;

            // Determine rotation
            if (indexFingerBone)
                t.LookAt(indexFingerBone, handUp);
            else if (middleFingerBone)
                t.LookAt(middleFingerBone, handUp);
            else if (handTarget.isLeft)
                t.LookAt(t.position - handTarget.humanoid.avatarRig.transform.right, handUp);
            else
                t.LookAt(t.position + handTarget.humanoid.avatarRig.transform.right, handUp);

            // Now get it in the palm
            if (handTarget.isLeft) {
                t.rotation *= Quaternion.Euler(0, -45, -90);
                t.position += t.rotation * new Vector3(0.02F, -0.02F, 0);
            }
            else {
                t.rotation *= Quaternion.Euler(0, 45, 90);
                t.position += t.rotation * new Vector3(-0.02F, -0.02F, 0);
            }
        }

        // Should become private...
        public static void DeterminePalmPosition(HandTarget handTarget) {
            if (handTarget.hand.bone.transform == null)
                return;

            if (handTarget.handPalm == null) {
                handTarget.handPalm = handTarget.hand.bone.transform.Find("Hand Palm");
                if (handTarget.handPalm == null) {
                    GameObject handPalmObj = new GameObject("Hand Palm");
                    handTarget.handPalm = handPalmObj.transform;
                    handTarget.handPalm.parent = handTarget.hand.bone.transform;
                }
            }


            Transform indexFingerBone = handTarget.fingers.index.proximal.bone.transform; // handTarget.fingers.indexFinger.bones[(int)FingerBones.Proximal];
            Transform middleFingerBone = handTarget.fingers.middle.proximal.bone.transform; //.middleFinger.bones[(int)FingerBones.Proximal];

            // Determine position
            Vector3 palmOffset;
            if (indexFingerBone)
                palmOffset = (indexFingerBone.position - handTarget.hand.bone.transform.position) * 0.9F;
            else if (middleFingerBone)
                palmOffset = (middleFingerBone.position - handTarget.hand.bone.transform.position) * 0.9F;
            else
                palmOffset = new Vector3(0.1F, 0, 0);

            handTarget.handPalm.position = handTarget.hand.bone.transform.position + palmOffset;

            Vector3 handUp = handTarget.hand.bone.targetRotation * Vector3.up;

            // Determine rotation
            if (indexFingerBone)
                handTarget.handPalm.LookAt(indexFingerBone, handUp);
            else if (middleFingerBone)
                handTarget.handPalm.LookAt(middleFingerBone, handUp);
            else if (handTarget.isLeft)
                handTarget.handPalm.LookAt(handTarget.handPalm.position - handTarget.humanoid.avatarRig.transform.right, handUp);
            else
                handTarget.handPalm.LookAt(handTarget.handPalm.position + handTarget.humanoid.avatarRig.transform.right, handUp);

            // Now get it in the palm
            if (handTarget.isLeft) {
                handTarget.handPalm.rotation *= Quaternion.Euler(0, -45, -90);
                handTarget.handPalm.position += handTarget.handPalm.rotation * new Vector3(0.02F, -0.02F, 0);
            }
            else {
                handTarget.handPalm.rotation *= Quaternion.Euler(0, 45, 90);
                handTarget.handPalm.position += handTarget.handPalm.rotation * new Vector3(-0.02F, -0.02F, 0);
            }
        }

        public static Socket CreatePinchSocket(HandTarget handTarget) {
            if (handTarget.hand.bone.transform == null)
                return null;

            GameObject socketObj = new GameObject("Pinch Socket");
            Transform socketTransform = socketObj.transform;
            socketTransform.parent = handTarget.hand.bone.transform;
            socketTransform.rotation = handTarget.hand.bone.targetRotation * Quaternion.Euler(355, 190, 155);
            socketTransform.position = handTarget.hand.target.transform.TransformPoint(handTarget.isLeft ? -0.1F : 0.1F, -0.035F, 0.03F);

            Socket pinchSocket = socketObj.AddComponent<Socket>();
            return pinchSocket;
        }

        #endregion

        #region Update
        public static void UpdateInteraction() {
            // This interferes with the HandPhysics which also sets the touchingObject...

            //InputDeviceIDs inputDeviceID = isLeft ? InputDeviceIDs.LeftHand : InputDeviceIDs.RightHand;
            //touchingObject = inputModule.GetTouchObject(inputDeviceID);
        }
        #endregion

        #region Touching

        public static void OnTouchStart(HandTarget handTarget, GameObject obj) {
            GrabCheck(handTarget, obj);
            if (handTarget.inputModule != null)
                handTarget.inputModule.OnFingerTouchStart(handTarget.isLeft, obj);
        }

        public static void OnTouchEnd(HandTarget handTarget, GameObject obj) {
            if (handTarget.inputModule != null && obj == handTarget.touchedObject)
                handTarget.inputModule.OnFingerTouchEnd(handTarget.isLeft);
        }

        #endregion

        #region Grabbing/Pinching

        public enum GrabType {
            HandGrab,
            Pinch
        }

        private static bool grabChecking;

        public static void GrabCheck(HandTarget handTarget, GameObject obj) {
            if (grabChecking || handTarget.grabbedObject != null || handTarget.humanoid.isRemote)
                return;

            grabChecking = true;
            if (CanBeGrabbed(handTarget, obj)) {
                float handCurl = handTarget.HandCurl();
                if (handCurl > 2) {
                    NetworkedGrab(handTarget, obj);
                }
                else {
                    bool pinching = PinchInteraction.IsPinching(handTarget);
                    if (pinching) {
                        //Debug.Log(handCurl);
                        NetworkedPinch(handTarget, obj);
                    }
                }
            }
            grabChecking = false;
        }

        #region Grab

        /// <summary>Grab and object with the hand (networked)</summary>
        /// <param name="handTarget">The hand to grab with</param>
        /// <param name="obj">The gameObject to grab</param>
        /// <param name="rangeCheck">check wither the hand is in range of the handle</param>
        public static void NetworkedGrab(HandTarget handTarget, GameObject obj, bool rangeCheck = true) {
            if (handTarget.humanoid.humanoidNetworking != null)
                handTarget.humanoid.humanoidNetworking.Grab(handTarget, obj, rangeCheck, GrabType.HandGrab);

            Grab(handTarget, obj, rangeCheck);
        }

        public static bool CanBeGrabbed(HandTarget handTarget, GameObject obj) {
            if (obj == null || obj == handTarget.humanoid.gameObject ||
                (handTarget.humanoid.characterRigidbody != null && obj == handTarget.humanoid.characterRigidbody.gameObject) ||
                obj == handTarget.humanoid.headTarget.gameObject
                )
                return false;

            // We cannot grab 2D objects like UI
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            if (rectTransform != null)
                return false;

            return true;
        }

        /// <summary>Grab and object with the hand (non-networked)</summary>
        /// <param name="handTarget">The hand to grab with</param>
        /// <param name="obj">The gameObject to grab</param>
        /// <param name="rangeCheck">check wither the hand is in range of the handle</param>
        public static void Grab(HandTarget handTarget, GameObject obj, bool rangeCheck = true) {
            //Debug.Log(handTarget + " grabs " + obj);
            bool grabbed = false;

            NoGrab noGrab = obj.GetComponent<NoGrab>();
            if (noGrab != null)
                return;

            if (AlreadyGrabbedWithOtherHand(handTarget, obj))
                grabbed = Grab2(handTarget, obj);

            else {
                Rigidbody objRigidbody = obj.GetComponent<Rigidbody>();
#if pHUMANOID
                Handle handle = Handle.GetClosestHandle(obj.transform, handTarget.transform.position, handTarget.isLeft ? Handle.Hand.Left : Handle.Hand.Right);
#else
                Handle handle = Handle.GetClosestHandle(obj.transform, handTarget.transform.position);
#endif
                if (handle != null) {
                    grabbed = GrabHandle(handTarget, objRigidbody, handle, rangeCheck);
                }
                else if (objRigidbody != null) {
                    grabbed = GrabRigidbodyWithoutHandle(handTarget, objRigidbody);
                }
            }

            if (grabbed) {
                TrackedRigidbody trackedRigidbody = obj.GetComponent<TrackedRigidbody>();
                if (trackedRigidbody != null && trackedRigidbody.target != null) {
                    //Debug.Log("grabbed trackedRigidbody");
                    handTarget.AddSensorComponent(trackedRigidbody.target.GetComponent<SensorComponent>());
                    handTarget.AddTrackedRigidbody(trackedRigidbody);
                }

                if (handTarget.humanoid.physics && handTarget.grabbedRigidbody) {
                    AdvancedHandPhysics.SetNonKinematic(handTarget.handRigidbody, handTarget.colliders);
                    if (handTarget.handPhysics != null && handTarget.handPhysics.mode != AdvancedHandPhysics.PhysicsMode.ForceLess)
                        handTarget.handPhysics.DeterminePhysicsMode(kinematicMass);
                }

                if (Application.isPlaying) {
                    handTarget.SendMessage("OnGrabbing", handTarget.grabbedObject, SendMessageOptions.DontRequireReceiver);
                    handTarget.grabbedObject.SendMessage("OnGrabbed", handTarget, SendMessageOptions.DontRequireReceiver);
                    if (handTarget.grabbedHandle != null)
                        handTarget.grabbedHandle.SendMessage("OnGrabbed", handTarget, SendMessageOptions.DontRequireReceiver);
                }
            }
        }

        private static bool AlreadyGrabbedWithOtherHand(HandTarget handTarget, GameObject obj) {
            if (handTarget.otherHand == null || handTarget.otherHand.hand.bone.transform == null)
                return false;

            if (obj.transform == handTarget.otherHand.hand.bone.transform)
                return true;

            Rigidbody objRigidbody = obj.GetComponent<Rigidbody>();
            if (objRigidbody != null && objRigidbody.isKinematic) {
                Transform parent = objRigidbody.transform.parent;
                if (parent == null)
                    return false;

                Rigidbody parentRigidbody = parent.GetComponentInParent<Rigidbody>();
                if (parentRigidbody == null)
                    return false;

                return AlreadyGrabbedWithOtherHand(handTarget, parentRigidbody.gameObject);
            }

            return false;
        }

        protected static bool Grab2(HandTarget handTarget, GameObject obj) {
            //Debug.Log("Grab2 " + obj);

#if pHUMANOID
            Handle handle = Handle.GetClosestHandle(obj.transform, handTarget.transform.position, handTarget.isLeft ? Handle.Hand.Left : Handle.Hand.Right);// obj.GetComponentInChildren<Handle>();
#else
            Handle handle = Handle.GetClosestHandle(obj.transform, handTarget.transform.position);
#endif
            if (handle == null)
                return false;

            Rigidbody objRigidbody = handle.GetComponentInParent<Rigidbody>();
            if (objRigidbody == null)
                return false;

            if (handle != null)
                GrabHandle(handTarget, objRigidbody, handle, false);
            else
                GrabRigidbody(handTarget, objRigidbody);

            handTarget.otherHand.twoHandedGrab = true;
            handTarget.otherHand.targetToSecondaryHandle = handTarget.otherHand.hand.target.transform.InverseTransformPoint(handle.transform.position);
            return true;
        }

        protected static bool GrabHandle(HandTarget handTarget, Rigidbody objRigidbody, Handle handle, bool rangeCheck) {
            //Debug.Log("GrabHandle " + handle);

            if (handle.grabType == Handle.GrabType.NoGrab)
                return false;

            GameObject obj = (objRigidbody != null) ? objRigidbody.gameObject : handle.gameObject;

            if (objRigidbody != null && objRigidbody.isKinematic) {
                //Debug.Log("Grab Kinematic Rigidbody Handle");
                // When grabbing a kinematic rigidbody, the hand should change to a non-kinematic rigidbody first
                AdvancedHandPhysics.SetNonKinematic(handTarget.handRigidbody, handTarget.colliders);
            }

            if (handle.socket != null) {
                //Debug.Log("Grab from socket");
                handle.socket.Release();
            }

            handTarget.targetToHandle = handTarget.hand.target.transform.InverseTransformPoint(handTarget.grabSocket.transform.position);

            handTarget.grabSocket.Attach(handle, rangeCheck);
            handTarget.grabbedHandle = handle;

            handTarget.grabbedObject = obj;
            handTarget.grabbedRigidbody = (objRigidbody != null);
            if (handTarget.grabbedRigidbody)
                handTarget.grabbedKinematicRigidbody = objRigidbody.isKinematic;

            return true;
        }

        public static bool GrabRigidbody(HandTarget handTarget, Rigidbody objRigidbody, bool rangeCheck = true) {
            //Debug.Log("GrabRigidbody " + objRigidbody);

            if (objRigidbody.mass > maxGrabbingMass)
                return false;

            if (objRigidbody.isKinematic) {
                // When grabbing a kinematic rigidbody, the hand should change to a non-kinematic rigidbody first
                AdvancedHandPhysics.SetNonKinematic(handTarget.handRigidbody, handTarget.colliders);
            }

            Handle[] handles = objRigidbody.GetComponentsInChildren<Handle>();
            for (int i = 0; i < handles.Length; i++) {

#if pHUMANOID
                if ((handTarget.isLeft && handles[i].hand == Handle.Hand.Right) ||
                    (!handTarget.isLeft && handles[i].hand == Handle.Hand.Left))
                    continue;
#endif

                return GrabRigidbodyHandle(handTarget, objRigidbody, handles[i], rangeCheck);
            }

            GrabRigidbodyWithoutHandle(handTarget, objRigidbody);

            handTarget.grabbedObject = objRigidbody.gameObject;
            handTarget.grabbedRigidbody = true;
            handTarget.grabbedKinematicRigidbody = objRigidbody.isKinematic;
            return true;
        }

        private static bool GrabRigidbodyHandle(HandTarget handTarget, Rigidbody objRigidbody, Handle handle, bool rangeCheck) {
            //Debug.Log("GrabRigidbodyHandle " + objRigidbody);
            Transform objTransform = objRigidbody.transform;

            if (handle.socket != null) {
                //Debug.Log("Grab from socket");
                handle.socket.Release();
            }
            handTarget.grabSocket.Attach(handle.transform, rangeCheck);

            handTarget.grabbedObject = handle.gameObject;
            handTarget.grabbedRigidbody = true;
            handTarget.grabbedKinematicRigidbody = objRigidbody.isKinematic;
#if pHUMANOID
            handle.handTarget = handTarget;
#endif

            ////if (AlreadyGrabbedWithOtherHand(handTarget, objRigidbody)) {
            ////    GrabRigidbodyBarHandle2(handTarget, objRigidbody, handle);
            ////    return;
            ////}

            //Joint joint = null;

            //// If the handle is in a socket, the socket should release the handle
            //// because you can grab handles from sockets with your hands
            //if (handle.socket != null) {
            //    Debug.Log("Grab from socket");
            //    handle.socket.Release();
            //}
            //else {
            //    joint = objRigidbody.GetComponent<Joint>();
            //}

            //if (joint != null || objRigidbody.constraints != RigidbodyConstraints.None) {
            //    MoveHandBoneToHandle(handTarget, handle);

            //    // To add: if handle.rotation = true
            //    Vector3 handleWorldPosition = handle.transform.TransformPoint(handle.position);
            //    Vector3 handleLocalPosition = handTarget.hand.bone.transform.InverseTransformPoint(handleWorldPosition);

            //    Quaternion handleWorldRotation = handle.transform.rotation * Quaternion.Euler(handle.rotation);
            //    Vector3 handleRotationAxis = handleWorldRotation * Vector3.up;

            //    Vector3 handleLocalRotationAxis = handTarget.hand.bone.transform.InverseTransformDirection(handleRotationAxis);

            //    GrabRigidbodyJoint(handTarget, objRigidbody, handleLocalPosition, handleLocalRotationAxis);
            //}
            //else {
            //    MoveObjectToHand(handTarget, objTransform, handle);
            //    GrabRigidbodyParenting(handTarget, objRigidbody);
            //}
            //handTarget.grabbedHandle = handle;

#if pHUMANOID
            if (handle.pose != null) {
                handTarget.poseMixer.SetPoseValue(handle.pose, 1);
            }
#endif

            handTarget.grabbedRigidbody = true;
            handTarget.grabbedKinematicRigidbody = objRigidbody.isKinematic;
            return true;
        }

        // Grab with second hand moving to object
        //private static void GrabRigidbodyBarHandle2(HandTarget handTarget, Rigidbody objRigidbody, Handle handle) {
        //    //Debug.Log("Grab Second Hand");
        //    Transform objTransform = handle.transform;

        //    MoveHandBoneToHandle(handTarget, handle);
        //    GrabRigidbodyJoint(handTarget, objRigidbody);
        //    handTarget.grabbedHandle = handle;

        //    handTarget.handPhysics.mode = AdvancedHandPhysics.PhysicsMode.ForceLess;

        //    handTarget.handMovements.toOtherHandle = handTarget.grabbedHandle.worldPosition - handTarget.otherHand.grabbedHandle.worldPosition;
        //    Quaternion hand2handle = Quaternion.LookRotation(handTarget.handMovements.toOtherHandle);
        //    handTarget.otherHand.handMovements.hand2handle = Quaternion.Inverse(hand2handle) * handTarget.otherHand.hand.target.transform.rotation;

        //    handTarget.grabbedRigidbody = true;
        //}

        public static void MoveHandBoneToHandle(HandTarget handTarget, Handle handle) {
            //Vector3 handPosition;
            //Quaternion handRotation;
            //GetGrabPosition(handTarget, handle, out handPosition, out handRotation);

            if (handle.grabType == Handle.GrabType.DefaultGrab ||
                handle.grabType == Handle.GrabType.BarGrab) {

                // Should use GetGrabPosition
                //handTarget.hand.bone.transform.rotation = handRotation;

                Quaternion handleWorldRotation = handle.transform.rotation; // * Quaternion.Euler(handle.rotation);
                Quaternion palm2handRot = Quaternion.Inverse(handTarget.handPalm.localRotation);
                handTarget.hand.bone.transform.rotation = handleWorldRotation * palm2handRot;
            }

            if (handle.grabType == Handle.GrabType.DefaultGrab ||
                handle.grabType == Handle.GrabType.BarGrab ||
                handle.grabType == Handle.GrabType.BallGrab) {

                //handTarget.hand.bone.transform.position = handPosition;
                Vector3 handleWPos = handle.transform.position;  //TransformPoint(handle.position);
                Vector3 palm2handPos = handTarget.hand.bone.transform.position - handTarget.handPalm.position;
                handTarget.hand.bone.transform.position = handleWPos + palm2handPos;
            }
        }

        public static void MoveAndGrabHandle(HandTarget handTarget, Handle handle) {
            if (handTarget == null || handle == null)
                return;

            MoveHandTargetToHandle(handTarget, handle);
            GrabHandle(handTarget, handle);
        }

        public static void MoveHandTargetToHandle(HandTarget handTarget, Handle handle) {
            // Should use GetGrabPosition
            Quaternion handleWorldRotation = handle.transform.rotation; // * Quaternion.Euler(handle.rotation);
            Quaternion palm2handRot = Quaternion.Inverse(Quaternion.Inverse(handTarget.hand.bone.targetRotation) * handTarget.palmRotation);
            handTarget.hand.target.transform.rotation = handleWorldRotation * palm2handRot;

            Vector3 handleWorldPosition = handle.transform.position; // TransformPoint(handle.position);
            handTarget.hand.target.transform.position = handleWorldPosition - handTarget.hand.target.transform.rotation * handTarget.localPalmPosition;
        }

        public static void GetGrabPosition(HandTarget handTarget, Handle handle, out Vector3 handPosition, out Quaternion handRotation) {
            Vector3 handleWPos = handle.transform.position; // TransformPoint(handle.position);
            Quaternion handleWRot = handle.transform.rotation; // * Quaternion.Euler(handle.rotation);

            GetGrabPosition(handTarget, handleWPos, handleWRot, out handPosition, out handRotation);
        }

        private static void GetGrabPosition(HandTarget handTarget, Vector3 targetPosition, Quaternion targetRotation, out Vector3 handPosition, out Quaternion handRotation) {
            Quaternion palm2handRot = Quaternion.Inverse(handTarget.handPalm.localRotation) * handTarget.hand.bone.toTargetRotation;
            handRotation = targetRotation * palm2handRot;

            Vector3 hand2palmPos = handTarget.handPalm.localPosition;
            Vector3 hand2palmWorld = handTarget.hand.bone.transform.TransformVector(hand2palmPos);
            Vector3 hand2palmTarget = handTarget.hand.target.transform.InverseTransformVector(hand2palmWorld); // + new Vector3(0, -0.03F, 0); // small offset to prevent fingers colliding with collider
            handPosition = targetPosition + handRotation * -hand2palmTarget;
            Debug.DrawLine(targetPosition, handPosition);
        }

        // This is not fully completed, no parenting of joints are created yet
        public static void GrabHandle(HandTarget handTarget, Handle handle) {
            handTarget.grabbedHandle = handle;
            handTarget.targetToHandle = handTarget.hand.target.transform.InverseTransformPoint(handle.transform.position);
            handTarget.grabbedObject = handle.gameObject;
#if pHUMANOID
            handle.handTarget = handTarget;

            if (handle.pose != null)
                handTarget.SetPose1(handle.pose);
#endif
        }

        private static bool GrabRigidbodyWithoutHandle(HandTarget handTarget, Rigidbody objRigidbody) {
            if (objRigidbody.mass > maxGrabbingMass) {
                Debug.Log("Object is too heavy, mass > " + maxGrabbingMass);
                return false;
            }

            GrabRigidbodyParenting(handTarget, objRigidbody);
            handTarget.grabbedRigidbody = true;
            handTarget.grabbedKinematicRigidbody = objRigidbody.isKinematic;
            return true;
        }

        private static bool GrabRigidbodyParenting(HandTarget handTarget, Rigidbody objRigidbody) {
            GameObject obj = objRigidbody.gameObject;

            RigidbodyDisabled.ParentRigidbody(handTarget.handRigidbody, objRigidbody);

            if (handTarget.humanoid.humanoidNetworking != null)
                HumanoidNetworking.DisableNetworkSync(objRigidbody.gameObject);

            if (Application.isPlaying)
                Object.Destroy(objRigidbody);
            else
                Object.DestroyImmediate(objRigidbody, true);
            handTarget.grabbedObject = obj;

            return true;
        }

        #endregion

        #region Pinch
        private static void NetworkedPinch(HandTarget handTarget, GameObject obj, bool rangeCheck = true) {
            if (handTarget.grabbedObject != null)                   // We are already holding an object
                return;

            if (obj.GetComponent<NoGrab>() != null)                 // Don't pinch NoGrab Rigidbodies
                return;

            Rigidbody objRigidbody = obj.GetComponent<Rigidbody>();
            RigidbodyDisabled objDisabledRigidbody = obj.GetComponent<RigidbodyDisabled>();
            if (objRigidbody == null && objDisabledRigidbody == null)   // We can only pinch Rigidbodies
                return;

            if (objRigidbody != null && objRigidbody.mass > maxGrabbingMass) // Don't pinch too heavy Rigidbodies            
                return;

            if (objDisabledRigidbody != null && objDisabledRigidbody.mass > maxGrabbingMass)    // Don't pinch too heavy Rigidbodies
                return;

            if (handTarget.humanoid.humanoidNetworking != null)
                handTarget.humanoid.humanoidNetworking.Grab(handTarget, obj, rangeCheck, GrabType.Pinch);

            LocalPinch(handTarget, obj);

            //Collider[] handColliders = handTarget.hand.bone.transform.GetComponentsInChildren<Collider>();
            //foreach (Collider handCollider in handColliders)
            //    Physics.IgnoreCollision(c, handCollider);            
        }

        public static void LocalPinch(HandTarget handTarget, GameObject obj, bool rangeCheck = true) {
            PinchWithSocket(handTarget, obj);
        }

        private static bool PinchWithSocket(HandTarget handTarget, GameObject obj) {
            Handle handle = obj.GetComponentInChildren<Handle>();
            if (handle != null) {
                if (handle.socket != null) {
                    //Debug.Log("Grab from socket");
                    handle.socket.Release();
                }
            }
            bool grabbed = handTarget.pinchSocket.Attach(obj.transform);
            handTarget.grabbedObject = obj;

            return grabbed;
        }
        #endregion

        #endregion

        #region Letting go

        private static bool letGoChecking = false;

        public static void CheckLetGo(HandTarget handTarget) {
            if (letGoChecking || handTarget.grabbedObject == null)
                return;

            letGoChecking = true;
            bool pulledLoose = PulledLoose(handTarget);

            if (handTarget.pinchSocket.attachedTransform != null) {
                bool notPinching = PinchInteraction.IsNotPinching(handTarget);
                if (notPinching || pulledLoose)
                    NetworkedLetGoPinch(handTarget);
            }
            else if (handTarget.grabSocket.attachedTransform != null || handTarget.grabbedObject != null) {
                float handCurl = handTarget.HandCurl();
                bool fingersGrabbing = (handCurl >= 1.5F);
                if (!fingersGrabbing || pulledLoose) {
                    NetworkedLetGo(handTarget);
                }
            }
            letGoChecking = false;
        }

        private static bool PulledLoose(HandTarget handTarget) {
            float forearmStretch = Vector3.Distance(handTarget.hand.bone.transform.position, handTarget.forearm.bone.transform.position) - handTarget.forearm.bone.length;
            if (forearmStretch > 0.15F) {
                //Debug.Log("Pulled loose 1");
                return true;
            }

            if (handTarget.grabbedHandle != null) {
                Vector3 handlePosition = handTarget.grabbedHandle.worldPosition;
                float handle2palm = Vector3.Distance(handlePosition, handTarget.palmPosition);
                if (handle2palm > 0.15F) {
                    //Debug.Log("Pulled loose 2");
                    return true;
                }
            }
            return false;
        }

        public static void NetworkedLetGo(HandTarget target) {
            if (target.humanoid.humanoidNetworking != null)
                target.humanoid.humanoidNetworking.LetGo(target);

            LetGo(target);
        }

        public static void LetGo(HandTarget handTarget) {
            //Debug.Log("LetGo " + handTarget.grabbedObject);

            if (handTarget.hand.bone.transform == null || handTarget.grabbedObject == null)
                return;

            if (handTarget.grabSocket.attachedHandle != null)
                handTarget.grabSocket.Release();
            else
                LetGoWithoutHandle(handTarget);

            if (handTarget.grabbedRigidbody) {
                Rigidbody grabbedRigidbody = handTarget.grabbedObject.GetComponent<Rigidbody>();
                LetGoRigidbody(handTarget, grabbedRigidbody);
            }

            //if (handTarget.humanoid.physics)
            //    handTarget.colliders = AdvancedHandPhysics.SetKinematic(handTarget.handRigidbody);

            if (handTarget.humanoid.dontDestroyOnLoad) {
                // Prevent this object inherites the dontDestroyOnLoad from the humanoid
                Object.DontDestroyOnLoad(handTarget.grabbedObject);
            }


            if (handTarget.humanoid.physics) {
                Target.UnsetColliderToTrigger(handTarget.colliders);

                handTarget.colliders = AdvancedHandPhysics.SetKinematic(handTarget.handRigidbody);
            }

            HandTarget.TmpDisableCollisions(handTarget, 0.2F);

            if (handTarget.handPhysics != null)
                handTarget.handPhysics.DeterminePhysicsMode(kinematicMass);

#if hNW_UNET
#pragma warning disable 0618
            NetworkTransform nwTransform = handTarget.grabbedObject.GetComponent<NetworkTransform>();
            if (nwTransform != null)
                nwTransform.sendInterval = 1;
#pragma warning restore 0618
#endif

            if (Application.isPlaying) {
                handTarget.SendMessage("OnLettingGo", null, SendMessageOptions.DontRequireReceiver);
                handTarget.grabbedObject.SendMessage("OnLetGo", handTarget, SendMessageOptions.DontRequireReceiver);
            }

            handTarget.grabbedObject = null;
            handTarget.twoHandedGrab = false;
            handTarget.otherHand.twoHandedGrab = false;
        }

        //private static void LetGoFromGrabSocket(HandTarget handTarget) {
        //    handTarget.grabSocket.Release();
        //}

        //private static void LetGoJoint(HandTarget handTarget, Joint joint) {
        //    Object.DestroyImmediate(joint);
        //}

        //private static void LetGoParenting(HandTarget handTarget) {
        //    if (handTarget.grabbedObject.transform.parent == handTarget.hand.bone.transform || handTarget.grabbedObject.transform.parent == handTarget.handPalm)
        //        handTarget.grabbedObject.transform.parent = null; // originalParent, see InstantVR

        //    if (handTarget.humanoid.humanoidNetworking != null)
        //        HumanoidNetworking.ReenableNetworkSync(handTarget.grabbedObject);
        //}

        //        private static void LetGoGrabbedObject(HandTarget handTarget) {
        //            //Debug.Log("letGoGrabbedObject");
        //            HandMovements.SetAllColliders(handTarget.grabbedObject, true);
        //            if (handTarget.humanoid.physics)
        //                //HumanoidTarget.SetColliderToTrigger(handTarget.grabbedObject, false);
        //                Target.UnsetColliderToTrigger(handTarget.colliders);

        //            //if (handTarget.grabbedRBdata == null)
        //            //    LetGoStaticObject(handTarget, handTarget.grabbedObject);
        //            //else
        //            //    LetGoRigidbody(handTarget);

        //            HandTarget.TmpDisableCollisions(handTarget, 0.2F);

        //            if (handTarget.handPhysics != null)
        //                handTarget.handPhysics.DeterminePhysicsMode(kinematicMass);

        //#if hNW_UNET
        //#pragma warning disable 0618
        //            NetworkTransform nwTransform = handTarget.grabbedObject.GetComponent<NetworkTransform>();
        //            if (nwTransform != null)
        //                nwTransform.sendInterval = 1;
        //#pragma warning restore 0618
        //#endif

        //            handTarget.SendMessage("OnLettingGo", null, SendMessageOptions.DontRequireReceiver);
        //            handTarget.grabbedObject.SendMessage("OnLetGo", handTarget, SendMessageOptions.DontRequireReceiver);

        //            handTarget.grabbedObject = null;
        //        }

        private static void LetGoWithoutHandle(HandTarget handTarget) {
            Rigidbody objRigidbody = RigidbodyDisabled.UnparentRigidbody(handTarget.handPalm, handTarget.grabbedObject.transform);

            if (handTarget.handRigidbody != null) {
                objRigidbody.velocity = handTarget.handRigidbody.velocity;
                objRigidbody.angularVelocity = handTarget.handRigidbody.angularVelocity;
            }
        }

        private static void LetGoRigidbody(HandTarget handTarget, Rigidbody grabbedRigidbody) {
            //Debug.Log("LetGoRigidbody");

            if (grabbedRigidbody != null) {
                //if (handTarget.handRigidbody != null)
                //    GrabMassRestoration(handTarget.handRigidbody, grabbedRigidbody);

                Joint[] joints = handTarget.grabbedObject.GetComponents<Joint>();
                for (int i = 0; i < joints.Length; i++) {
                    if (joints[i].connectedBody == handTarget.handRigidbody)
                        Object.Destroy(joints[i]);
                }
                //grabbedRigidbody.centerOfMass = handTarget.storedCOM;

                if (handTarget.handRigidbody != null) {
                    if (handTarget.handRigidbody.isKinematic) {
                        grabbedRigidbody.velocity = handTarget.hand.target.velocity;
                        Vector3 targetAngularSpeed = handTarget.hand.target.rotationVelocity.eulerAngles;
                        grabbedRigidbody.angularVelocity = targetAngularSpeed * Mathf.Deg2Rad;
                    }
                    else {
                        grabbedRigidbody.velocity = handTarget.handRigidbody.velocity;
                        grabbedRigidbody.angularVelocity = handTarget.handRigidbody.angularVelocity;
                    }
                }

                if (handTarget.grabbedHandle != null)
                    LetGoHandle(handTarget, handTarget.grabbedHandle);
            }
            handTarget.grabbedRigidbody = false;
        }

        private static void LetGoStaticObject(HandTarget handTarget, GameObject obj) {
            //Debug.Log("LetGoStaticObject");
            if (handTarget.grabbedHandle != null)
                LetGoHandle(handTarget, handTarget.grabbedHandle);

            Collider c = obj.GetComponentInChildren<Collider>();
            if (c != null) {
                Collider[] handColliders = handTarget.hand.bone.transform.GetComponentsInChildren<Collider>();
                foreach (Collider handCollider in handColliders) {
                    Physics.IgnoreCollision(handCollider, c, false);
                    //Debug.Log("Ignore Collision: " + handCollider.name + " - " + c.name + " FALSE");
                }
            }
        }

        private static void LetGoHandle(HandTarget handTarget, Handle handle) {
#if pHUMANOID
            if (handTarget.grabbedHandle != null) {
                handTarget.grabbedHandle.SendMessage("OnLetGo", handTarget, SendMessageOptions.DontRequireReceiver);
            }

            handle.handTarget = null;
            handTarget.grabbedHandle = null;
            if (handTarget.transform.parent == handle.transform)
                handTarget.transform.parent = handTarget.humanoid.transform;

            if (handle.pose != null) {
                //handTarget.poseMixer.SetPoseValue(handle.pose, 0);
                handTarget.poseMixer.Remove(handle.pose);
            }
#endif
        }

        #region Pinch

        protected static void NetworkedLetGoPinch(HandTarget handTarget) {
            // No Networking yet!
            LetGoPinch(handTarget);
        }

        protected static void LetGoPinch(HandTarget handTarget) {
            handTarget.pinchSocket.Release();

            if (handTarget.humanoid.physics)
                //HumanoidTarget.SetColliderToTrigger(handTarget.grabbedObject, false);
                HumanoidTarget.UnsetColliderToTrigger(handTarget.colliders);

            handTarget.SendMessage("OnLettingGo", null, SendMessageOptions.DontRequireReceiver);
            handTarget.grabbedObject.SendMessage("OnLetGo", handTarget, SendMessageOptions.DontRequireReceiver);

            handTarget.grabbedObject = null;

        }
        #endregion

        #endregion

        public static void GrabOrLetGo(HandTarget handTarget, GameObject obj, bool rangeCheck = true) {
            if (handTarget.grabbedObject != null)
                NetworkedLetGo(handTarget);
            else
                NetworkedGrab(handTarget, obj, rangeCheck);
        }
    }
}