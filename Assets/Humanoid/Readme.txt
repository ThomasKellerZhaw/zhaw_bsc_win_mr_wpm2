Humanoid Control
Version: 3.1 beta 2
Baseline: 2.2.4

Features
- Oculus Quest Hand Tracking
- Skeleton for Leap Motion

Improvements
- Pose errors with Bone.None entries
- LeapMotion Hands not tracking because of missing skeleton
- Oculus hand tracking Editor fix for OSX
- Physics stability fixes
- Oculus support not enabling fix