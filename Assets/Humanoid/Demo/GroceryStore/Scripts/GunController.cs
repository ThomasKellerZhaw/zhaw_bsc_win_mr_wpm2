﻿using Passer;
using UnityEngine;

[ExecuteInEditMode]
public class GunController : MonoBehaviour {

    public bool bulletLoaded = true;
    public bool autoLoad = true;

    public AudioSource audioSource;
    public FlashLight flashLight;
    public InteractionPointer interactionPointer;
    public Counter magazine;

    public virtual void Shoot() {
        if (bulletLoaded == false)
            return;

        if (!autoLoad)
            bulletLoaded = false;        

        if (flashLight)
            flashLight.Flash();

        if (audioSource)
            audioSource.Play();

        if (interactionPointer != null) {
            GameObject target = interactionPointer.objectInFocus;
            if (target == null)
                return;

            Rigidbody targetRigidbody = target.GetComponent<Rigidbody>();
            if (targetRigidbody == null)
                return;

            Vector3 hitPoint = interactionPointer.focusPointObj.transform.position;
            Vector3 forceDirection = interactionPointer.transform.forward;
            targetRigidbody.AddForceAtPosition(forceDirection * 10, hitPoint, ForceMode.Impulse);
        }
    }

    public virtual void LoadBullet() {
        if (magazine != null) {
            bulletLoaded = (magazine.value > 0);
            magazine.Decrement();
        }
        else
            bulletLoaded = true;
    }

    public virtual void ReplaceMagazine() {
        if (magazine != null)
            magazine.value = magazine.max;
    }

    public void OnGrabbed() {
        if (interactionPointer != null)
            interactionPointer.active = true;
    }

    public void OnLetGo() {
        if (interactionPointer != null)
            interactionPointer.active = false;
    }
}
