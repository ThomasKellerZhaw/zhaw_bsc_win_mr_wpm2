﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Passer {

    /// <summary>
    /// Controls players using controller input
    /// </summary>
    public class PawnControl : MonoBehaviour {

        #region Settings

        /// <summary>If true, real world objects like controllers and cameras are shown in the scene</summary>
        public bool showRealObjects = true;

        /// <summary>Enables controller physics and collisions during walking.</summary>
        public bool physics = true;
        /// <summary>If there is not static object below the feet of the avatar the avatar will fall down until it reaches solid ground</summary>
        public bool useGravity = true;
        /// <summary>The maximum height of objects of the ground which do not stop the humanoid</summary>
        public float stepOffset = 0.3F;
        /// <summary>Will move the pawn position when grabbing handles on static objects</summary>
        public bool bodyPull = false;
        public void set_bodyPull(bool value) {
            bodyPull = value;
        }

        /// <summary>Reduces the walking speed of the humanoid when in the neighbourhood of objects to reduce motion sickness.</summary>
        public bool proximitySpeed = false;
        /// <summary>The amount of influence of the proximity speed. 1=No influence, 0 = Maximum</summary>
        public float proximitySpeedRate = 0.8f;

        /// <summary>Types of startposition for the Pawn</summary>
        public enum StartPosition {
            AvatarPosition,
            PlayerPosition
        }
        /// <summary>The start position of the humanoid</summary>
        public StartPosition startPosition = StartPosition.AvatarPosition;

        public enum MatchingType {
            None,
            SetHeightToCharacter,
        }
        /// <summary>Scale Tracking to Avatar scales the tracking input to match the size of the avatar</summary>
        [SerializeField]
        protected MatchingType avatarMatching = MatchingType.SetHeightToCharacter;
        /// <summary>Perform a calibration when the scene starts</summary>
        public bool calibrateAtStart = false;
        /// <summary>Sets the Don't Destoy On Load such that the pawn survives scene changes</summary>
        public bool dontDestroyOnLoad = false;

        #endregion

        #region Init

        protected virtual void Awake() {
            if (dontDestroyOnLoad)
                DontDestroyOnLoad(this.transform.root);

            //AddPlayer();
            characterController = GetCharacter();

            //DetermineTargets();
            InitTargets();
            NewTargetComponents();

            characterNeckHeight = GetCharacterNeckHeight();
            characterSoleThickness = GetCharacterSoleThickness();

            StartTargets();
            InitTrackers();
            StartTrackers();

            StartSensors();
        }

        #endregion

        #region Character

        public CharacterController characterController;
        public float characterNeckHeight;
        public float characterSoleThickness;

        public virtual CharacterController GetCharacter() {
            if (characterController != null && characterController.enabled && characterController.gameObject.activeInHierarchy) {
                // We already have a good characterController
                return characterController;
            }

            CharacterController[] characterControllers = GetComponentsInChildren<CharacterController>();
            if (characterControllers != null && characterControllers.Length > 0)
                return characterControllers[0];

            return null;
        }

        protected virtual float GetCharacterNeckHeight() {
            if (characterController != null && cameraTarget != null) {
                float characterHeight = cameraTarget.pawn.characterController.height;
                return characterHeight * 0.75F;
            }
            else if (cameraTarget != null)
                return cameraTarget.transform.localPosition.y;
            else
                return 1.6F;
        }

        protected virtual float GetCharacterSoleThickness() {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 1, Physics.DefaultRaycastLayers))
                return hit.distance;

            return 0;
        }

        protected virtual void UpdateCharacter() {
            if (characterController == null)
                return;

            characterController.detectCollisions = false;
            //float cameraYAngle = cameraTarget.unity.sensorTransform.eulerAngles.y;
            //characterController.transform.rotation = Quaternion.AngleAxis(cameraYAngle, Vector3.up);
        }

        #endregion

        #region Targets

        public enum TargetId {
            Camera,
            LeftController,
            RightController,
        }

        public CameraTarget cameraTarget;
        public ControllerTarget leftControllerTarget;
        public ControllerTarget rightControllerTarget;

        protected virtual void NewTargetComponents() {
            if (cameraTarget != null)
                cameraTarget.InitComponent();
            if (leftControllerTarget != null)
                leftControllerTarget.InitComponent();
            if (rightControllerTarget != null)
                rightControllerTarget.InitComponent();
        }

        public virtual void InitTargets() {
        }

        protected virtual void StartTargets() {
            if (cameraTarget != null)
                cameraTarget.StartTarget();
            if (leftControllerTarget != null)
                leftControllerTarget.StartTarget();
            if (rightControllerTarget != null)
                rightControllerTarget.StartTarget();
        }

        protected virtual void UpdateTargets() {
            if (cameraTarget != null)
                cameraTarget.UpdateTarget();
            if (leftControllerTarget != null)
                leftControllerTarget.UpdateTarget();
            if (rightControllerTarget != null)
                rightControllerTarget.UpdateTarget();
        }

        #endregion

        #region Trackers

        /// <summary>The transform containing all real-world objects</summary>
        public Transform realWorld;

        /// <summary>Gets the Real World GameObject for this Pawn</summary>
        /// <param name="transform">The root transform of the Pawn</param>
        public static Transform GetRealWorld(Transform transform) {
            Transform realWorldTransform = transform.Find("Real World");
            if (realWorldTransform != null)
                return realWorldTransform;

            GameObject realWorld = new GameObject("Real World");
            realWorldTransform = realWorld.transform;

            realWorldTransform.parent = transform;
            realWorldTransform.position = transform.position;
            realWorldTransform.rotation = transform.rotation;
            return realWorldTransform;
        }

        /// <summary>Use game controller input</summary>
        public bool gameControllerEnabled = true;
        /// <summary>The game controller for this pawn</summary>
        public Controller controller;
        /// <summary>The index of the game controller</summary>
        public int gameControllerIndex;
        /// <summary>The game controller type for this pawn</summary>
        public GameControllers gameController;
        ///// <summary>Set the controller Id for this pawn</summary>
        //public static void SetControllerID(PawnControl player, int controllerID) {
        //    if (player.traditionalInput != null) {
        //        player.controller = player.traditionalInput.SetControllerID(controllerID);
        //        player.gameControllerIndex = controllerID;
        //    }
        //}

        public List<TrackerComponent> trackerComponents = new List<TrackerComponent>();
        public UnityTrackerComponent unityComponent;

        protected Tracker[] _trackers;
        /// <summary>All available trackers for this pawn</summary>
        public Tracker[] trackers {
            get {
                if (_trackers == null)
                    InitTrackers();
                return _trackers;
            }
        }

        public UnityTracker unityTracker = new UnityTracker();

        //#if hOPENVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        //        /// <summary>The SteamVR tracker</summary>
        //        public SteamVRTracker steam = new SteamVRTracker();
        //#endif
        //#if hOCULUS
        //        /// <summary>The Oculus tracker</summary>
        //        public OculusTracker oculus = new OculusTracker();
        //#endif
        //        /// <summary>The Windows Mixed Reality tracker</summary>
        //#if hWINDOWSMR && UNITY_2017_2_OR_NEWER && UNITY_WSA_10_0
        //        public WindowsMRTracker mixedReality = new WindowsMRTracker();
        //#endif
        //        /// <summary>The Wave VR tracker</summary>
        //#if hWAVEVR
        //        public WaveVRTracker waveVR = new WaveVRTracker();
        //#endif

        private TraditionalDevice traditionalInput = new TraditionalDevice();

        protected virtual void InitTrackers() {
            _trackers = new Tracker[] {
                unityTracker,
//#if hOPENVR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
//                steam,
//#endif
//#if hOCULUS
//                oculus,
//#endif
//#if hWINDOWSMR && UNITY_2017_2_OR_NEWER && UNITY_WSA_10_0
//                mixedReality,
//#endif
//#if hWAVEVR
//                waveVR,
//#endif
            };
        }

        protected virtual void StartTrackers() {
            traditionalInput.SetControllerID(0);

            for (int i = 0; i < _trackers.Length; i++)
                _trackers[i].StartTracker(this.transform);
        }

        protected virtual void UpdateTrackers() {
            if (gameControllerEnabled && traditionalInput != null)
                traditionalInput.UpdateGameController(gameController);

            //if (cameraTarget != null && cameraTarget.unity != null)
            //    //cameraTarget.unity.Update();
            //if (leftControllerTarget != null && leftControllerTarget.unityController != null)
            //    leftControllerTarget.unityController.Update();
            //if (rightControllerTarget != null && rightControllerTarget.unityController != null)
            //    rightControllerTarget.unityController.Update();

            for (int i = 0; i < _trackers.Length; i++)
                _trackers[i].UpdateTracker();
        }

        protected virtual void StartSensors() {
            if (cameraTarget != null)
                cameraTarget.StartSensors();
            if (leftControllerTarget != null)
                leftControllerTarget.StartSensors();
            if (rightControllerTarget != null)
                rightControllerTarget.StartSensors();
        }

        protected virtual void StopSensors() {
        }

        #endregion

        #region Update
        protected virtual void Update() {
            Controllers.Clear();
            UpdateTrackers();
            UpdateTargets();
            UpdateCharacter();
            CalculateVelocity();
            //UpdateAnimation();
        }

        protected virtual void FixedUpdate() {
            CheckGround();
            CalculateMovement();
        }

        protected virtual void LateUpdate() {
            Controllers.EndFrame();
        }
        #endregion

        #region Calibration

        /// <summary>Calibrates the tracking with the player</summary>
        public virtual void Calibrate() {
            Debug.Log("Calibrate");
            foreach (Tracker tracker in _trackers)
                tracker.Calibrate();

            if (startPosition == StartPosition.AvatarPosition)
                SetStartPosition();

            switch (avatarMatching) {
                case MatchingType.SetHeightToCharacter:
                    SetTrackingHeightToAvatar();
                    break;
                default:
                    break;
            }
        }

        public virtual void SetStartPosition() {
            Vector3 delta = transform.position - cameraTarget.unity.camera.transform.position;
            delta.y = 0;
            AdjustTracking(delta);
        }

        protected virtual void SetTrackingHeightToAvatar() {
            Vector3 localNeckPosition;
            if (UnityTracker.xrDevice == UnityTracker.XRDeviceType.None || cameraTarget.unity.camera == null)
                localNeckPosition = cameraTarget.transform.position - transform.position;
            else
                localNeckPosition = cameraTarget.GetNeckPosition() - transform.position;

            float deltaHeight = (characterNeckHeight - localNeckPosition.y);
            AdjustTracking(deltaHeight * Vector3.up);
        }

        /// <summary>Adjust the tracking origin of all trackers</summary>
        /// <param name="delta">The translation to apply to the tracking origin</param>
        public virtual void AdjustTracking(Vector3 delta) {
            foreach (Tracker tracker in trackers) {
                tracker.AdjustTracking(delta, Quaternion.identity);
            }
        }

        #endregion

        #region Movement

        /// <summary>
        /// maximum forward speed in units(meters)/second
        /// </summary>
        public float forwardSpeed = 1;
        /// <summary>
        /// maximum backward speed in units(meters)/second
        /// </summary>
        public float backwardSpeed = 0.6F;
        /// <summary>
        /// maximum sideways speed in units(meters)/second
        /// </summary>
        public float sidewardSpeed = 1;
        /// <summary>
        /// maximum acceleration in units(meters)/second/second
        /// value 0 = no maximum acceleration
        /// </summary>
        public float maxAcceleration = 1;
        /// <summary>
        /// maximum rotational speed in degrees/second
        /// </summary>
        public float rotationSpeed = 60;

        public Vector3 velocity;
        [HideInInspector]
        protected Vector3 gravitationalVelocity;

        protected Vector3 inputMovement = Vector3.zero;

        public virtual void MoveForward(float z) {
            if (z > 0)
                z *= forwardSpeed;
            else
                z *= backwardSpeed;

            inputMovement = new Vector3(inputMovement.x, inputMovement.y, z);
        }

        public virtual void MoveSideward(float x) {
            x = x * sidewardSpeed;

            inputMovement = new Vector3(x, inputMovement.y, inputMovement.z);
        }

        public virtual void Stop() {
            inputMovement = Vector3.zero;
        }

        /// <summary>Rotate the pawn</summary>
        /// Rotates the pawn along the Y axis
        /// <param name="angularSpeed">The speed in degrees per second</param>
        public virtual void Rotate(float angularSpeed) {
            angularSpeed *= Time.deltaTime * rotationSpeed;
            transform.Rotate(transform.up, angularSpeed);
        }

        /// <summary>Set the rotation angle along Y axis</summary>
        public virtual void Rotation(float yAngle) {
            Vector3 angles = transform.eulerAngles;
            transform.rotation = Quaternion.Euler(angles.x, yAngle, angles.z);
        }

        /** @name Pawn Actions
         */
        ///@{

        /// <summary>Rotation animation to reach the given rotation</summary>
        /// The speed of the rotation is determined by the #rotationSpeed
        /// <param name="targetRotation">The target rotation</param>
        public IEnumerator RotateTo(Quaternion targetRotation) {
            float angle;
            float maxAngle;
            do {
                maxAngle = rotationSpeed * Time.deltaTime;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, maxAngle);
                angle = Quaternion.Angle(transform.rotation, targetRotation);
                yield return new WaitForEndOfFrame();
            } while (angle > maxAngle);
        }

        /// <summary>
        /// Rotation animation to look at the given position
        /// </summary>
        /// The speed of the rotation is determined by the #rotationSpeed
        /// <param name="targetPosition">The position to look at</param>
        /// <returns></returns>
        public IEnumerator LookAt(Vector3 targetPosition) {
            Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position);
            yield return RotateTo(targetRotation);
        }

        /// <summary>
        /// Movement animation to reach the given position
        /// </summary>
        /// The pawn will walk to the given position. The speed of the movement is determined by #forwardSpeed.
        /// Any required rotation will be limited by #rotationSpeed
        /// <param name="targetPosition">The position to where the pawn should walk</param>
        /// <returns></returns>
        public IEnumerator WalkTo(Vector3 targetPosition) {
            yield return LookAt(targetPosition);

            float totalDistance = Vector3.Distance(transform.position, targetPosition);
            float distance = totalDistance;
            do {
                if (distance > 0.03F)
                    transform.LookAt(targetPosition);
                transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);
                if (distance > 0.5F) // make dependent on maxAcceleration?
                    MoveForward(1);
                else {
                    float f = 1 - (distance / 0.5F);
                    MoveForward(1 - (f * f * f));
                }
                Vector3 direction = targetPosition - transform.position;
                direction.y = 0;
                distance = direction.magnitude;
                //distance = Vector3.Distance(transform.position, targetPosition);
                yield return new WaitForEndOfFrame();
            } while (distance > 0.01F);
            Stop();

            transform.position = targetPosition;
        }

        /// <summary>
        /// Movement animation to reach the given position and rotation
        /// </summary>
        /// The pawn will walk to the given position and rotation. The speed of the movement is determined by #forwardSpeed.
        /// Any required rotation will be limited by #rotationSpeed
        /// <param name="targetPosition">The position to where the pawn should walk</param>
        /// <param name="targetRotation">The rotation the pawn should have at the end</param>
        /// <returns></returns>
        public IEnumerator WalkTo(Vector3 targetPosition, Quaternion targetRotation) {
            yield return WalkTo(targetPosition);
            yield return RotateTo(targetRotation);
        }

        ///@}

        public virtual void Jump(float velocity) {
            if (ground == null)
                return;

            gravitationalVelocity -= new Vector3(0, -velocity, 0);
            float y = inputMovement.y - velocity;
            inputMovement = new Vector3(inputMovement.x, y, inputMovement.z);
        }

        protected virtual void CalculateMovement() {
            if (characterController != null && cameraTarget != null) {
                if (UnityTracker.xrDevice == UnityTracker.XRDeviceType.None) {
                    // Determine tracking translation
                    Vector3 localHmdPosition = new Vector3(0, -1.5F, -characterController.radius);
                    Vector3 cameraPositionOnCharacter = cameraTarget.unity.transform.TransformPoint(localHmdPosition);
                    Vector3 cameraTranslation = cameraPositionOnCharacter - characterController.transform.position;

                    Vector3 moveDirection = characterController.transform.TransformDirection(inputMovement);

                    characterController.Move(cameraTranslation + moveDirection * Time.fixedDeltaTime);
                }
                else {
                    Vector3 oldCameraPosition = cameraTarget.unity.transform.position;

                    Vector3 moveDirection = characterController.transform.TransformDirection(inputMovement);

                    Vector3 localHmdPosition = new Vector3(0, 0, -characterController.radius);
                    Vector3 neckPosition = cameraTarget.unity.transform.TransformPoint(localHmdPosition) + moveDirection * Time.fixedDeltaTime;

                    Vector3 footPosition = new Vector3(neckPosition.x, transform.position.y, neckPosition.z);

                    Vector3 cameraTranslation = footPosition - transform.position;

                    float characterHeight = (neckPosition.y - transform.position.y) / 0.75F;

                    characterController.height = characterHeight;
                    characterController.center = new Vector3(0, characterHeight / 2, 0);

                    Vector3 ccPos = characterController.transform.position;
                    characterController.Move(cameraTranslation + moveDirection * Time.fixedDeltaTime);
                    unityTracker.trackerTransform.Translate(-cameraTranslation);
                }
            }
            else {
                Vector3 moveDirection = transform.TransformDirection(inputMovement);
                transform.Translate(moveDirection * Time.fixedDeltaTime);
            }
            inputMovement = Vector3.zero;
        }

        protected virtual void CalculateMovement2() {
            if (characterController != null && cameraTarget != null) {
                Vector3 localHmdPosition = new Vector3(0, 0, -characterController.radius);

                Vector3 cameraPositionOnCharacter = cameraTarget.unity.transform.TransformPoint(localHmdPosition);
                Vector3 cameraTranslation = cameraPositionOnCharacter - characterController.transform.position;

                float characterBottom = characterController.transform.position.y; // - characterController.height / 2 - characterController.skinWidth;
                float cameraHeight = cameraPositionOnCharacter.y;

                float characterHeight = cameraHeight * (2 / 1.5F); // + characterController.height / 4;
                if (characterHeight > 0.5F)
                    characterController.height = characterHeight;
                float characterY = characterBottom; // + characterHeight / 2;
                float translationY = characterY - characterController.transform.position.y;
                translationY = 0;

                cameraTranslation = new Vector3(cameraTranslation.x, translationY, cameraTranslation.z);
                //Vector3 moveDirection = characterController.transform.TransformDirection(inputMovement);

                Vector3 ccPos = characterController.transform.position;
                characterController.Move(cameraTranslation);
                //Vector3 ccTranslation = characterController.transform.position - ccPos;


                // compensate charactercontroller translation in VR HMD
                //cameraTarget.unity.tracker.trackerTransform.Translate(-ccTranslation + moveDirection * Time.fixedDeltaTime);

                /*
                unity.trackerTransform.Translate(-ccTranslation + moveDirection * Time.fixedDeltaTime);
                */

                // Better restore Camera.main.transform.position
                // by moving cameraTarget.unity.tracker.trackertransform based on camPos

                //float cameraDistance = cameraTranslation.magnitude - ccTranslation.magnitude;
                //if (cameraDistance > 0.01F)
                //    cameraTarget.unity.Fader(cameraDistance - 0.01F);
                //else
                //    cameraTarget.unity.Fader(0);
            }
            else {
                Vector3 moveDirection = transform.TransformDirection(inputMovement);
                transform.Translate(moveDirection * Time.fixedDeltaTime);
            }
            inputMovement = Vector3.zero;
        }

        private void Fall() {
            gravitationalVelocity += Physics.gravity * Time.fixedDeltaTime;
            transform.Translate(gravitationalVelocity * Time.fixedDeltaTime);
        }

        #region Ground

        public Transform ground;
        [HideInInspector]
        protected Transform lastGround;
        [HideInInspector]
        public Vector3 groundVelocity;
        [HideInInspector]
        public float groundAngularVelocity;
        [HideInInspector]
        protected Vector3 lastGroundPosition = Vector3.zero;
        [HideInInspector]
        protected float lastGroundAngle = 0;

        protected virtual void CheckGround() {
            CheckGrounded();
            //CheckGroundMovement();
        }

        protected virtual void CheckGrounded() {
            //if (leftControllerTarget.GrabbedStaticObject() || rightControllerTarget.GrabbedStaticObject())
            //    return;

            Vector3 pawnBase = transform.position;
            Vector3 groundNormal;
            float distance = GetDistanceToGroundAt(pawnBase, stepOffset, out ground, out groundNormal);
            if (ground != null && distance < 0.01F && gravitationalVelocity.y <= 0) {
                gravitationalVelocity = Vector3.zero;
                transform.Translate(0, distance, 0);
            }
            else {
                ground = null;
                if (useGravity)
                    Fall();
            }
        }

        protected virtual void CheckGroundMovement() {
            if (ground == null) {
                lastGround = null;
                lastGroundPosition = Vector3.zero;
                lastGroundAngle = 0;
                return;
            }

            if (ground == lastGround) {
                Vector3 groundTranslation = ground.position - lastGroundPosition;
                // Vertical movement is handled by CheckGrounded
                groundTranslation = new Vector3(groundTranslation.x, 0, groundTranslation.z);
                groundVelocity = groundTranslation / Time.deltaTime;

                float groundRotation = ground.eulerAngles.y - lastGroundAngle;
                groundAngularVelocity = groundRotation / Time.deltaTime;

                if (this.transform.root != ground.root) {
                    transform.Translate(groundTranslation, Space.World);
                    transform.RotateAround(ground.position, Vector3.up, groundRotation);
                }
            }

            lastGround = ground;
            lastGroundPosition = ground.position;
            lastGroundAngle = ground.eulerAngles.y;
        }

        public virtual float GetDistanceToGroundAt(Vector3 position, float maxDistance, out Transform ground, out Vector3 normal) {
            normal = Vector3.up;

            Vector3 rayStart = position + normal * maxDistance;
            Vector3 rayDirection = -normal;
            //Debug.DrawRay(rayStart, rayDirection * maxDistance * 2, Color.magenta);
            RaycastHit[] hits = Physics.RaycastAll(rayStart, rayDirection, maxDistance * 2, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore);

            if (hits.Length == 0) {
                ground = null;
                return -maxDistance;
            }

            int closestHit = 0;
            bool foundClosest = false;
            for (int i = 0; i < hits.Length; i++) {
                if ((characterController == null || hits[i].transform != characterController.transform) &&
                    hits[i].distance <= hits[closestHit].distance) {
                    closestHit = i;
                    foundClosest = true;
                }
            }
            if (!foundClosest) {
                ground = null;
                return -maxDistance;
            }

            ground = hits[closestHit].transform;
            normal = hits[closestHit].normal;
            float distance = maxDistance - hits[closestHit].distance;
            return distance;
        }

        #endregion

        [HideInInspector]
        protected float lastVelocityTime;
        [HideInInspector]
        protected Vector3 lastPosition;

        protected virtual void CalculateVelocity() {
            if (lastVelocityTime > 0) {
                float deltaTime = Time.time - lastVelocityTime;

                Vector3 velocity = Vector3.zero; // -groundVelocity;
                //if (avatarRig != null) {
                //    Vector3 headTranslation = headTarget.neck.target.transform.position - lastHeadPosition;//preTrackingHeadPosition;
                //    if (headTranslation.magnitude == 0)
                //        // We assume we did not get an update - needs to be improved though
                //        // Especially with networking, position updates occur less frequent than frame updates
                //        return;
                //    Vector3 localHeadTranslation = headTarget.neck.target.transform.InverseTransformDirection(headTranslation);
                //    localVelocity += localHeadTranslation / deltaTime;
                //    //Debug.Log(gameObject.name + " " + localHeadTranslation.z);

                //    float headDirection = headTarget.neck.target.transform.eulerAngles.y - lastHeadDirection; //preTrackingHeadDirection;
                //    float localHeadDirection = Angle.Normalize(headDirection);
                //    turningVelocity = localHeadDirection / deltaTime;
                //}
                //else {
                Vector3 translation = transform.position - lastPosition;
                velocity += translation / deltaTime;
                //}
                //acceleration = (localVelocity - velocity) / deltaTime;
                // Acceleration is not correct like this. We get accels like -24.3, 22, 6.7, -34.4, 32.6, -5.0 for linear speed increase...
                // This code is not correct. 
                //if (acceleration.magnitude > 15) { // more than 15 is considered unhuman and will be ignored
                //    localVelocity = Vector3.zero;
                //    acceleration = Vector3.zero;
                //}
            }
            lastVelocityTime = Time.time;

            lastPosition = transform.position;
            //lastRotation = headTarget.neck.target.transform.rotation;
            //lastDirection = headTarget.neck.target.transform.eulerAngles.y;
        }

        #endregion

        #region Animation
        /*
        public Animator animatorController;

        public string animatorParameterForward;
        public string animatorParameterSideward;
        public string animatorParameterRotation;
        public string animatorParameterHeight;

        // needed for the Editor
        [HideInInspector]
        public int animatorParameterForwardIndex;
        [HideInInspector]
        public int animatorParameterSidewardIndex;
        [HideInInspector]
        public int animatorParameterRotationIndex;
        [HideInInspector]
        public int animatorParameterHeightIndex;

        protected void UpdateAnimation() {
            if (animatorController == null)
                return;

            if (animatorParameterForward != null && animatorParameterForward != "")
                animatorController.SetFloat(animatorParameterForward, velocity.z);

            if (animatorParameterSideward != null && animatorParameterSideward != "")
                animatorController.SetFloat(animatorParameterSideward, velocity.x);

            //if (animatorParameterRotation != null && animatorParameterRotation != "")
            //    animatorController.SetFloat(animatorParameterRotation, turningVelocity);

            //if (animatorParameterHeight != null && animatorParameterHeight != "") {
            //    float relativeHeadHeight = headTarget.neck.target.transform.position.y - avatarNeckHeight;
            //    animatorController.SetFloat(animatorParameterHeight, relativeHeadHeight);
            //}            
        }

        public void SetAnimatorForward(float f) {
            if (animatorController == null)
                return;

            animatorController.SetFloat(animatorParameterForward, f);
        }
        */
        #endregion

        #region Networking

        /// <summary>The networking interface</summary>
        public IPawnNetworking pawnNetworking;
        /// <summary>Is true when this is a remote pawn</summary>
        /// Remote pawns are not controlled locally, but are controlled from another computer.
        /// These are copies of the pawn on that other computer and are updated via messages
        /// exchanges on a network.
        public bool isRemote = false;
        /// <summary>The Id of this pawn across the network</summary>
        public ulong nwId;

        /// <summary>The local Id of this humanoid</summary>
        public int id = -1;

        #endregion
    }
}