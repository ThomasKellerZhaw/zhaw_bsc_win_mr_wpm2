﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Passer {

#if hNW_UNET
#pragma warning disable 0618
    [RequireComponent(typeof(NetworkIdentity))]
    public class PawnUnet : NetworkBehaviour, IPawnNetworking {
#pragma warning restore 0618
#else
    public class PawnUnet : MonoBehaviour {
#endif
#if hNW_UNET
#pragma warning disable 0618
        [SerializeField]
        protected float _sendRate = 25;
        public float sendRate {
            get { return _sendRate; }
        }

        [SerializeField]
        protected PawnNetworking.DebugLevel _debug = PawnNetworking.DebugLevel.Error;
        public PawnNetworking.DebugLevel debug {
            get { return _debug; }
        }

        [SerializeField]
        protected bool _createLocalRemotes = false;
        public bool createLocalRemotes {
            get { return _createLocalRemotes; }
            set { _createLocalRemotes = value; }
        }

        [SerializeField]
        protected PawnNetworking.Smoothing _smoothing = PawnNetworking.Smoothing.None;
        public PawnNetworking.Smoothing smoothing {
            get { return _smoothing; }
        }

        protected NetworkWriter writer;
        protected NetworkReader reader;

        public bool isLocal { get; set; }

        public List<PawnControl> pawns = new List<PawnControl>();

        #region Init

        virtual public void Awake() {
            DontDestroyOnLoad(this);
            PawnNetworking.Start(debug);
        }

        public override void OnStartClient() {
            name = name + " " + netId;

            NetworkManager nwManager = FindObjectOfType<NetworkManager>();
            short msgType = MsgType.Highest + 2;
            nwManager.client.RegisterHandler(msgType, ClientProcessPawn);
        }

        public override void OnStartServer() {
            short msgType = MsgType.Highest + 1;
            NetworkServer.RegisterHandler(msgType, ForwardPawn);
        }

        #endregion

        #region Start
        public override void OnStartLocalPlayer() {
            isLocal = true;
            name = "PawnUnet(Local)";

            pawns = PawnNetworking.FindLocalPawns();
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log((int)netId.Value + ": Found " + pawns.Count + " Pawns");

            if (pawns.Count <= 0)
                return;

            foreach (PawnControl pawn in pawns) {
                pawn.nwId = (ulong)netId.Value;
                pawn.pawnNetworking = this;

                if (debug <= PawnNetworking.DebugLevel.Info)
                    Debug.Log(pawn.nwId + ": Send Start Pawn " + pawn.id);

                CmdServerStartPawn(pawn.nwId, pawn.id, pawn.gameObject.name, pawn.transform.position, pawn.transform.rotation);
            }

            //NetworkingSpawner spawner = FindObjectOfType<NetworkingSpawner>();
            //if (spawner != null)
            //    spawner.OnNetworkingStarted();
        }

        #endregion

        #region Update
        protected float lastSend;
        virtual public void LateUpdate() {
            if (!isLocalPlayer)
                return;

            NetworkIdentity identity = GetComponent<NetworkIdentity>();
            if (Time.time > lastSend + 1 / sendRate) {
                foreach (PawnControl pawn in pawns) {
                    if (!pawn.isRemote) {
                        SendPawn2Server(identity, pawn);
                        //if (syncTracking)
                        //    SendTracking2Server(identity, pawn);
                    }
                }
                lastSend = Time.time;
            }
        }
        #endregion

        #region Stop

        virtual public void OnDestroy() {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log((int)netId.Value + ": Destroy Remote Pawn");

            foreach (PawnControl pawn in pawns) {
                if (pawn == null)
                    continue;

                if (pawn.isRemote) {
                    if (pawn.gameObject != null)
                        Destroy(pawn.gameObject);
                }
                else
                    pawn.nwId = 0;
            }
        }
        #endregion

        #region Instantiate Pawn
        void IPawnNetworking.InstantiatePawn(PawnControl pawn) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": Instantiate Pawn " + pawn.id);

            pawns.Add(pawn);
            pawn.nwId = (ulong)netId.Value;

            CmdServerStartPawn(pawn.nwId, pawn.id, pawn.gameObject.name, pawn.transform.position, pawn.transform.rotation);
        }

        protected ulong serverNwId;
        private int serverPawnId;
        protected string serverName;
        protected Vector3 serverPosition;
        protected Quaternion serverRotation;

        [Command] // @ server
        private void CmdServerStartPawn(ulong nwId, int pawnId, string name, Vector3 position, Quaternion rotation) {
            serverNwId = nwId;
            serverPawnId = pawnId;
            serverName = name;
            serverPosition = position;
            serverRotation = rotation;

            PawnUnet[] nwPawns = FindObjectsOfType<PawnUnet>();
            foreach (PawnUnet nwPawn in nwPawns)
                nwPawn.NewClientStarted();
        }

        virtual public void NewClientStarted() {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(serverNwId + ": Forward Start Pawn " + serverPawnId);

            RpcClientStartPawn(serverNwId, serverPawnId, serverName, serverPosition, serverRotation);
        }

        [ClientRpc] // @ remote client
        private void RpcClientStartPawn(ulong nwId, int pawnId, string name, Vector3 position, Quaternion rotation) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl remotePawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (remotePawn != null) {
                // This remote pawn already exists
                return;
            }

            pawns.Add(PawnNetworking.StartPawn(nwId, pawnId, name, position, rotation));
        }
        #endregion

        #region Destroy Pawn
        void IPawnNetworking.DestroyPawn(PawnControl pawn) {
            if (pawn == null)
                return;

            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": Destroy Pawn " + pawn.id);

            pawns.Remove(pawn);
            pawn.nwId = 0;

            if (this != null)
                CmdServerDestroyPawn(pawn.nwId, pawn.id);
        }

        [Command] // @ server
        private void CmdServerDestroyPawn(ulong nwId, int pawnId) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": Forward Destroy Pawn " + pawnId);

            RpcClientDestroyPawn(nwId, pawnId);
        }

        [ClientRpc]
        private void RpcClientDestroyPawn(ulong nwId, int pawnId) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl remotePawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (remotePawn == null) {
                // Unknown remote pawn
                return;
            }

            if (remotePawn.gameObject != null)
                Destroy(remotePawn.gameObject);
        }
        #endregion

        #region Pawn

        #region Send @ local client
        public void SendPawn2Server(NetworkIdentity identity, PawnControl pawn) {
            short msgType = MsgType.Highest + 1;
            writer = new NetworkWriter();

            writer.StartMessage(msgType);
            this.SendPawn(pawn);
            writer.FinishMessage();

            identity.connectionToServer.SendWriter(writer, Channels.DefaultUnreliable);
        }
        #endregion

        #region Forward @ server
        [ServerCallback] // @ server
        public void ForwardPawn(NetworkMessage msg) {
            short msgType = MsgType.Highest + 2;
            NetworkWriter sWriter = new NetworkWriter();

            sWriter.StartMessage(msgType);
            ForwardPawn(msg.reader, sWriter);
            sWriter.FinishMessage();

            NetworkServer.SendWriterToReady(null, sWriter, Channels.DefaultUnreliable);
        }

        public void ForwardPawn(NetworkReader sReader, NetworkWriter sWriter) {
            int nwId = sReader.ReadInt32();
            sWriter.Write(nwId); // NwId
            int pawnId = sReader.ReadInt32();
            sWriter.Write(pawnId); // Pawn Id

            sWriter.Write(sReader.ReadSingle()); // Pose Time

            byte targetMask = sReader.ReadByte();
            sWriter.Write(targetMask);

            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": Forward Pawn = " + pawnId + ", targetMask = " + targetMask);

            ForwardTransform(sReader, sWriter); // pawn.transform is always sent

            ForwardTargets(sReader, sWriter, targetMask);
        }

        private void ForwardTargets(NetworkReader sReader, NetworkWriter sWriter, byte targetMask) {
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.Camera);
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.LeftController);
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.RightController);
        }

        private void ForwardTarget(NetworkReader sReader, NetworkWriter sWriter, byte targetMask, PawnControl.TargetId targetId) {
            if (PawnNetworking.IsTargetActive(targetMask, targetId))
                ForwardTransform(sReader, sWriter);
        }

        private void ForwardTransform(NetworkReader sReader, NetworkWriter sWriter) {
            sWriter.Write(sReader.ReadVector3()); // position;
            sWriter.Write(sReader.ReadQuaternion()); // rotation;
        }

        #endregion

        #region Receive @ remote client
        protected float lastPoseTime;
        [ClientCallback] // @ remote client
        public void ClientProcessPawn(NetworkMessage msg) {
            reader = msg.reader;

            int nwId = ReceiveInt();
            int pawnId = ReceiveInt();

            NetworkInstanceId netId = new NetworkInstanceId((uint)nwId);

            // NetworkMessages are not send to a specific player, but are received by the last
            // instantiated player. We need to find the player with the correct NetworkInstanceId
            // first and then let that player receive the message.
            GameObject nwObject = ClientScene.FindLocalObject(netId);
            if (nwObject != null) {
                PawnUnet pawnUnet = nwObject.GetComponent<PawnUnet>();
                pawnUnet.ReceivePawn(msg, pawnId);
            }
            else {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(nwId + ": Could not find PawnNetworking object");
            }
        }

        public void ReceivePawn(NetworkMessage msg, int pawnId) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId.Value + ": Could not find pawn: " + pawnId);
                return;
            }

            reader = msg.reader;
            lastPoseTime = this.ReceivePawn(pawn, lastPoseTime);
        }
        #endregion

        #endregion


        #region Grab

        void IPawnNetworking.Grab(ControllerTarget controllerTarget, GameObject obj, bool rangeCheck) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(controllerTarget.pawn.nwId + ": Grab " + obj);

            NetworkIdentity nwIdentity = obj.GetComponent<NetworkIdentity>();
            if (nwIdentity == null) {
                if (debug <= PawnNetworking.DebugLevel.Error)
                    Debug.LogError("Grabbed object " + obj + " does not have a network identity");
            }
            else
                CmdGrab(controllerTarget.pawn.nwId, controllerTarget.pawn.id, obj, controllerTarget.isLeft, rangeCheck);
        }

        [Command] // @ server
        virtual public void CmdGrab(ulong nwId, int pawnId, GameObject obj, bool leftHanded, bool rangeCheck) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log("Server: CmdGrab " + obj);

            RpcClientGrab(nwId, pawnId, obj, leftHanded, rangeCheck);
        }

        [ClientRpc] // @ remote client
        virtual public void RpcClientGrab(ulong nwId, int pawnId, GameObject obj, bool leftHanded, bool rangeCheck) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": RpcGrab " + obj);

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId.Value + ": Could not find pawn: " + pawnId);
                return;
            }

            ControllerTarget controllerTarget = leftHanded ? pawn.leftControllerTarget : pawn.rightControllerTarget;
            ControllerInteraction.Grab(controllerTarget, obj, true);
        }
        #endregion

        #region Let Go

        void IPawnNetworking.LetGo(ControllerTarget controllerTarget) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(controllerTarget.pawn.nwId + ": LetGo ");

            CmdLetGo(controllerTarget.pawn.nwId, controllerTarget.pawn.id, controllerTarget.isLeft);
        }

        [Command] // @ server
        virtual public void CmdLetGo(ulong nwId, int pawnId, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Warning)
                Debug.Log("Server: CmdLetGo ");

            RpcClientLetGo(nwId, pawnId, leftHanded);
        }

        [ClientRpc] // @ remote client
        virtual public void RpcClientLetGo(ulong nwId, int pawnId, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": RpcLetGo " + pawnId);

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId.Value + ": Could not find pawn: " + pawnId);
                return;
            }

            ControllerTarget controllerTarget = leftHanded ? pawn.leftControllerTarget : pawn.rightControllerTarget;
            ControllerInteraction.LetGo(controllerTarget);
        }

        #endregion

        #region Send
        public void Send(bool b) { writer.Write(b); }
        public void Send(byte b) { writer.Write(b); }
        public void Send(int x) { writer.Write(x); }
        public void Send(float f) { writer.Write(f); }
        public void Send(Vector3 v) { writer.Write(v); }
        public void Send(Quaternion q) { writer.Write(q); }
        #endregion

        #region Receive
        public bool ReceiveBool() { return reader.ReadBoolean(); }
        public byte ReceiveByte() { return reader.ReadByte(); }
        public int ReceiveInt() { return reader.ReadInt32(); }
        public float ReceiveFloat() { return reader.ReadSingle(); }
        public Vector3 ReceiveVector3() { return reader.ReadVector3(); }
        public Quaternion ReceiveQuaternion() { return reader.ReadQuaternion(); }
        #endregion
#pragma warning restore 0618
#endif
    }

    public partial class NetworkedTransform {
#if hNW_UNET
#pragma warning disable 0618
        public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation) {
            return (GameObject)Network.Instantiate(prefab, position, rotation, 0);
        }

#pragma warning restore 0618
#endif
    }
}