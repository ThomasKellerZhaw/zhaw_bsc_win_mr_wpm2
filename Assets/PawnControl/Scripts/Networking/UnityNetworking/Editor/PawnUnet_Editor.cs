﻿using UnityEditor;

namespace Passer {

    [CustomEditor(typeof(PawnUnet))]
    public class PawnUnet_Editor : PawnNetworking_Editor {
#if hNW_UNET
        public override void OnInspectorGUI() {
            serializedObject.Update();

            SendRateInspector();
            DebugLevelInspector();
            SmoothingInspector();
            CreateLocalRemotesInspector();

            serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}