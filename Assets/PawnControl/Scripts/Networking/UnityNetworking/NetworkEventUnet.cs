﻿using UnityEngine;
using UnityEngine.Networking;


namespace Passer {

#if hNW_UNET
#pragma warning disable 0618
    [RequireComponent(typeof(NetworkIdentity))]
    public partial class NetworkObject : NetworkBehaviour {
        #region Void Event

        public void RPC(FunctionCall functionCall) {
            CmdRPCVoid(functionCall.targetGameObject, functionCall.methodName);
        }

        [Command] // @ server
        public void CmdRPCVoid(GameObject target, string methodName) {
            RpcRPCVoid(target, methodName);
        }

        [ClientRpc] // @ remote client
        public void RpcRPCVoid(GameObject targetGameObject, string methodName) {
            Debug.Log("RPC: " + methodName);
            FunctionCall.Execute(targetGameObject, methodName);
        }

        #endregion

        #region Bool Event

        public void RPC(FunctionCall functionCall, bool value) {
            CmdRPCBool(functionCall.targetGameObject, functionCall.methodName, value);
        }

        [Command] // @ server
        public void CmdRPCBool(GameObject target, string methodName, bool value) {
            RpcRPCBool(target, methodName, value);
        }

        [ClientRpc] // @ remote client
        public void RpcRPCBool(GameObject target, string methodName, bool value) {
            FunctionCall.Execute(target, methodName, value);
        }

        #endregion
    }

    /*
    [RequireComponent(typeof(NetworkIdentity))]
    public partial class NetworkEvent : NetworkBehaviour, INetworkEvent {

        #region Void Event

        public static void Execute(NetworkEvent networkEvent, FunctionCall functionCall) {
            if (networkEvent == null && functionCall.targetGameObject != null) {
                networkEvent = functionCall.targetGameObject.GetComponent<NetworkEvent>();
                if (networkEvent == null) {
                    Debug.LogError(
                        functionCall.targetGameObject + " has not NetworkEvent, " +
                        "Event " + functionCall.methodName + " will not work over the network.");
                }
            }
            if (networkEvent != null)
                networkEvent.VoidEvent(functionCall.targetGameObject, functionCall.methodName);
        }

        public void VoidEvent(Object target, string methodName) {
            CmdVoidEvent((GameObject)target, methodName);
        }

        [Command] // @ server
        public void CmdVoidEvent(GameObject target, string methodName) {
            RpcVoidEvent(target, methodName);
        }

        [ClientRpc] // @ remote client
        public void RpcVoidEvent(GameObject targetGameObject, string methodName) {
            FunctionCall.Execute(targetGameObject, methodName);
        }

        #endregion

        #region Bool Event

        public void BoolEvent(Object target, string methodName, bool value) {
            //NetworkIdentity identity = GetComponent<NetworkIdentity>();
            if (target is GameObject)
                CmdBoolEvent((GameObject)target, methodName, value);
            else
                CmdBoolEvent(((Component)target).gameObject, methodName, value);
        }

        [Command] // @ server
        public void CmdBoolEvent(GameObject target, string methodName, bool value) {
            RpcBoolEvent(target, methodName, value);
        }

        [ClientRpc] // @ remote client
        public void RpcBoolEvent(GameObject targetGameObject, string methodName, bool value) {
            //EventHandler.CallTargetMethod(targetGameObject, methodName, value);
            // Need to create a functioncall object and use that now
        }

        #endregion

#pragma warning restore 0618
//#else
//    public partial class NetworkEvent : MonoBehaviour {
//#endif

//    }
    */
#endif
}