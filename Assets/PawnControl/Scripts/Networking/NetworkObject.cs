﻿using UnityEngine;

namespace Passer {

    // This replaces both NetworkedTransform and NetworkEvent
    public partial class NetworkObject {
#if hNW_UNET
        public static NetworkObject GetNetworkObject(FunctionCall functionCall) {
            if (functionCall.targetGameObject == null)
                return null;

            NetworkObject networkObj = functionCall.targetGameObject.GetComponent<NetworkObject>();
            if (networkObj == null) {
                Debug.LogError(
                    functionCall.targetGameObject + " has no NetworkObject component, " +
                    "Event " + functionCall.methodName + " will not work over the network.");
            }
            return networkObj;
        }
#else
        public static void Execute(FunctionCall functionCall) { }
#endif
#if !hNW_UNET && !hNW_MIRROR && !hNW_PHOTON && !hNW_BOLT
        public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation) {
            return Instantiate(prefab, position, rotation);
        }
#endif
    }

    public interface INetworkObject {
        void Execute(FunctionCall functionCall);
    }
}