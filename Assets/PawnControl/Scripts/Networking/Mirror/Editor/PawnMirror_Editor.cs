﻿using UnityEditor;

namespace Passer {

    [CustomEditor(typeof(PawnMirror))]
    public class PawnMirror_Editor : PawnNetworking_Editor {
#if hNW_MIRROR
        public override void OnInspectorGUI() {
            serializedObject.Update();

            SendRateInspector();
            DebugLevelInspector();
            SmoothingInspector();
            CreateLocalRemotesInspector();

            serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}