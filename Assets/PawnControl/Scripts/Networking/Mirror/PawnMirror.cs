﻿using System.Collections.Generic;
using UnityEngine;
#if hMIRROR
using Mirror;
#endif

namespace Passer {

#if hNW_MIRROR
    [RequireComponent(typeof(NetworkIdentity))]
    public class PawnMirror : NetworkBehaviour, IPawnNetworking {
#else
    public class PawnMirror : MonoBehaviour {
#endif
#if hNW_MIRROR
        [SerializeField]
        protected float _sendRate = 25;
        public float sendRate {
            get { return _sendRate; }
        }

        [SerializeField]
        protected PawnNetworking.DebugLevel _debug = PawnNetworking.DebugLevel.Error;
        public PawnNetworking.DebugLevel debug {
            get { return _debug; }
        }

        [SerializeField]
        protected bool _createLocalRemotes = false;
        public bool createLocalRemotes {
            get { return _createLocalRemotes; }
            set { _createLocalRemotes = value; }
        }

        [SerializeField]
        private PawnNetworking.Smoothing _smoothing = PawnNetworking.Smoothing.None;
        public PawnNetworking.Smoothing smoothing {
            get { return _smoothing; }
        }

        protected NetworkWriter writer;
        protected NetworkReader reader;

        public bool isLocal { get; set; }

        public List<PawnControl> pawns = new List<PawnControl>();

        #region Init

        virtual public void Awake() {
            DontDestroyOnLoad(this);
            PawnNetworking.Start(debug);
        }

        public override void OnStartClient() {
            name = name + " " + netId;

            NetworkManager nwManager = FindObjectOfType<NetworkManager>();
            //short msgType = (short)MsgType.Highest + 2;
            //nwManager.client.RegisterHandler(msgType, ClientProcessPawn);
            //NetworkClient.RegisterHandler(msgType, ClientProcessPawn);
            NetworkClient.RegisterHandler<PawnMessage>(ClientProcessPawn);
        }

        public override void OnStartServer() {
            //short msgType = (short)MsgType.Highest + 1;
            //NetworkServer.RegisterHandler(msgType, ForwardPawn);
            NetworkServer.RegisterHandler<PawnMessage>(ForwardPawn);
        }

        #endregion

        #region Start
        public override void OnStartLocalPlayer() {
            isLocal = true;
            name = "PawnUnet(Local)";

            pawns = PawnNetworking.FindLocalPawns();
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log((int)netId + ": Found " + pawns.Count + " Pawns");

            if (pawns.Count <= 0)
                return;

            foreach (PawnControl pawn in pawns) {
                pawn.nwId = (ulong)netId;
                pawn.pawnNetworking = this;

                if (debug <= PawnNetworking.DebugLevel.Info)
                    Debug.Log(pawn.nwId + ": Send Start Pawn " + pawn.id);

                CmdServerStartPawn(pawn.nwId, pawn.id, pawn.gameObject.name, pawn.transform.position, pawn.transform.rotation);
            }

            //NetworkingSpawner spawner = FindObjectOfType<NetworkingSpawner>();
            //if (spawner != null)
            //    spawner.OnNetworkingStarted();
        }

        #endregion

        #region Update
        protected float lastSend;
        virtual public void LateUpdate() {
            if (!isLocalPlayer)
                return;

            NetworkIdentity identity = GetComponent<NetworkIdentity>();
            if (Time.time > lastSend + 1 / sendRate) {
                foreach (PawnControl pawn in pawns) {
                    if (!pawn.isRemote) {
                        SendPawn2Server(identity, pawn);
                        //if (syncTracking)
                        //    SendTracking2Server(identity, pawn);
                    }
                }
                lastSend = Time.time;
            }
        }
        #endregion

        #region Stop

        virtual public void OnDestroy() {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log((int)netId + ": Destroy Remote Pawn");

            foreach (PawnControl pawn in pawns) {
                if (pawn == null)
                    continue;

                if (pawn.isRemote) {
                    if (pawn.gameObject != null)
                        Destroy(pawn.gameObject);
                }
                else
                    pawn.nwId = 0;
            }
        }
        #endregion

        #region Instantiate Pawn
        void IPawnNetworking.InstantiatePawn(PawnControl pawn) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": Instantiate Pawn " + pawn.id);

            pawns.Add(pawn);
            pawn.nwId = (ulong)netId;

            CmdServerStartPawn(pawn.nwId, pawn.id, pawn.gameObject.name, pawn.transform.position, pawn.transform.rotation);
        }

        protected ulong serverNwId;
        private int serverPawnId;
        protected string serverName;
        protected Vector3 serverPosition;
        protected Quaternion serverRotation;

        [Command] // @ server
        private void CmdServerStartPawn(ulong nwId, int pawnId, string name, Vector3 position, Quaternion rotation) {
            serverNwId = nwId;
            serverPawnId = pawnId;
            serverName = name;
            serverPosition = position;
            serverRotation = rotation;

            PawnMirror[] nwPawns = FindObjectsOfType<PawnMirror>();
            foreach (PawnMirror nwPawn in nwPawns)
                nwPawn.NewClientStarted();
        }

        virtual public void NewClientStarted() {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(serverNwId + ": Forward Start Pawn " + serverPawnId);

            RpcClientStartPawn(serverNwId, serverPawnId, serverName, serverPosition, serverRotation);
        }

        [ClientRpc] // @ remote client
        private void RpcClientStartPawn(ulong nwId, int pawnId, string name, Vector3 position, Quaternion rotation) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl remotePawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (remotePawn != null) {
                // This remote pawn already exists
                return;
            }

            pawns.Add(PawnNetworking.StartPawn(nwId, pawnId, name, position, rotation));
        }
        #endregion

        #region Destroy Pawn
        void IPawnNetworking.DestroyPawn(PawnControl pawn) {
            if (pawn == null)
                return;

            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": Destroy Pawn " + pawn.id);

            pawns.Remove(pawn);
            pawn.nwId = 0;

            if (this != null)
                CmdServerDestroyPawn(pawn.nwId, pawn.id);
        }

        [Command] // @ server
        private void CmdServerDestroyPawn(ulong nwId, int pawnId) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": Forward Destroy Pawn " + pawnId);

            RpcClientDestroyPawn(nwId, pawnId);
        }

        [ClientRpc]
        private void RpcClientDestroyPawn(ulong nwId, int pawnId) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl remotePawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (remotePawn == null) {
                // Unknown remote pawn
                return;
            }

            if (remotePawn.gameObject != null)
                Destroy(remotePawn.gameObject);
        }
        #endregion

        #region Pawn

        #region Send @ local client

        public class PawnMessage : MessageBase {
            public ulong nwId;
            public int id;
            public float time; // Pose time
            public byte targetMask;
        }

        public void SendPawn2Server(NetworkIdentity identity, PawnControl pawn) {
            //short msgType = (short)MsgType.Highest + 1;
            //writer = new NetworkWriter();

            //writer.StartMessage(msgType);
            //this.SendPawn(pawn);
            //writer.FinishMessage();

            //identity.connectionToServer.SendWriter(writer, Channels.DefaultUnreliable);

            PawnMessage pawnMessage = new PawnMessage() {
                nwId = pawn.nwId,
                id = pawn.id,
                time = Time.time,
                targetMask = PawnNetworking.DetermineActiveTargets(pawn)
            };
            identity.connectionToServer.Send(pawnMessage);
        }
        #endregion

        #region Forward @ server
        [ServerCallback] // @ server
        public void ForwardPawn(NetworkConnection nwConnection, PawnMessage pawnMessage) {
            //short msgType = (short)MsgType.Highest + 2;
            //NetworkWriter sWriter = new NetworkWriter();

            //sWriter.StartMessage(msgType);
            //ForwardPawn(msg.reader, sWriter);
            //sWriter.FinishMessage();

            //NetworkServer.SendWriterToReady(null, sWriter, Channels.DefaultUnreliable);
            NetworkIdentity identity = GetComponent<NetworkIdentity>();
            NetworkServer.SendToReady(identity, pawnMessage, Channels.DefaultReliable);
        }

        public void ForwardPawn(NetworkReader sReader, NetworkWriter sWriter) {
            int nwId = sReader.ReadInt32();
            sWriter.WriteInt32(nwId); // NwId
            int pawnId = sReader.ReadInt32();
            sWriter.WriteInt32(pawnId); // Pawn Id

            sWriter.WriteSingle(sReader.ReadSingle()); // Pose Time

            byte targetMask = sReader.ReadByte();
            sWriter.WriteByte(targetMask);

            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": Forward Pawn = " + pawnId + ", targetMask = " + targetMask);

            ForwardTransform(sReader, sWriter); // pawn.transform is always sent

            ForwardTargets(sReader, sWriter, targetMask);
        }

        private void ForwardTargets(NetworkReader sReader, NetworkWriter sWriter, byte targetMask) {
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.Camera);
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.LeftController);
            ForwardTarget(sReader, sWriter, targetMask, PawnControl.TargetId.RightController);
        }

        private void ForwardTarget(NetworkReader sReader, NetworkWriter sWriter, byte targetMask, PawnControl.TargetId targetId) {
            if (PawnNetworking.IsTargetActive(targetMask, targetId))
                ForwardTransform(sReader, sWriter);
        }

        private void ForwardTransform(NetworkReader sReader, NetworkWriter sWriter) {
            sWriter.WriteVector3(sReader.ReadVector3()); // position;
            sWriter.WriteQuaternion(sReader.ReadQuaternion()); // rotation;
        }

        #endregion

        #region Receive @ remote client
        protected float lastPoseTime;

        [ClientCallback] // @ remote client
        public void ClientProcessPawn(NetworkConnection nwConnection, PawnMessage pawnMessage) {

        }

        [ClientCallback] // @ remote client
        public void ClientProcessPawn(NetworkMessage msg) {
            reader = msg.reader;

            int nwId = ReceiveInt();
            int pawnId = ReceiveInt();

            //NetworkInstanceId netId = new NetworkInstanceId((uint)nwId);

            // NetworkMessages are not send to a specific player, but are received by the last
            // instantiated player. We need to find the player with the correct NetworkInstanceId
            // first and then let that player receive the message.
            NetworkIdentity networkIdentity = NetworkIdentity.spawned[(uint)nwId];
            if (networkIdentity == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(nwId + ": Could not find Network Identity");
            }

            GameObject nwObject = networkIdentity.gameObject; //ClientScene.FindLocalObject(netId);
            if (nwObject != null) {
                PawnMirror pawnMirror = nwObject.GetComponent<PawnMirror>();
                pawnMirror.ReceivePawn(msg, pawnId);
            }
            else {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(nwId + ": Could not find PawnNetworking object");
            }
        }

        public void ReceivePawn(NetworkMessage msg, int pawnId) {
            if (isLocalPlayer && !createLocalRemotes)
                return;

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId + ": Could not find pawn: " + pawnId);
                return;
            }

            reader = msg.reader;
            lastPoseTime = this.ReceivePawn(pawn, lastPoseTime);
        }
        #endregion

        #endregion


        #region Grab

        void IPawnNetworking.Grab(ControllerTarget controllerTarget, GameObject obj, bool rangeCheck) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(controllerTarget.pawn.nwId + ": Grab " + obj);

            NetworkIdentity nwIdentity = obj.GetComponent<NetworkIdentity>();
            if (nwIdentity == null) {
                if (debug <= PawnNetworking.DebugLevel.Error)
                    Debug.LogError("Grabbed object " + obj + " does not have a network identity");
            }
            else
                CmdGrab(controllerTarget.pawn.nwId, controllerTarget.pawn.id, obj, controllerTarget.isLeft);
        }

        [Command] // @ server
        virtual public void CmdGrab(ulong nwId, int pawnId, GameObject obj, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log("Server: CmdGrab " + obj);

            RpcClientGrab(nwId, pawnId, obj, leftHanded);
        }

        [ClientRpc] // @ remote client
        virtual public void RpcClientGrab(ulong nwId, int pawnId, GameObject obj, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(nwId + ": RpcGrab " + obj);

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId + ": Could not find pawn: " + pawnId);
                return;
            }

            ControllerTarget controllerTarget = leftHanded ? pawn.leftControllerTarget : pawn.rightControllerTarget;
            ControllerInteraction.Grab(controllerTarget, obj, true);
        }
        #endregion

        #region Let Go

        void IPawnNetworking.LetGo(ControllerTarget controllerTarget) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(controllerTarget.pawn.nwId + ": LetGo ");

            CmdLetGo(controllerTarget.pawn.nwId, controllerTarget.pawn.id, controllerTarget.isLeft);
        }

        [Command] // @ server
        virtual public void CmdLetGo(ulong nwId, int pawnId, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Warning)
                Debug.Log("Server: CmdLetGo ");

            RpcClientLetGo(nwId, pawnId, leftHanded);
        }

        [ClientRpc] // @ remote client
        virtual public void RpcClientLetGo(ulong nwId, int pawnId, bool leftHanded) {
            if (debug <= PawnNetworking.DebugLevel.Info)
                Debug.Log(netId + ": RpcLetGo " + pawnId);

            PawnControl pawn = PawnNetworking.FindRemotePawn(pawns, pawnId);
            if (pawn == null) {
                if (debug <= PawnNetworking.DebugLevel.Warning)
                    Debug.LogWarning(netId + ": Could not find pawn: " + pawnId);
                return;
            }

            ControllerTarget controllerTarget = leftHanded ? pawn.leftControllerTarget : pawn.rightControllerTarget;
            ControllerInteraction.LetGo(controllerTarget);
        }

        #endregion

        #region Send
        public void Send(bool b) { writer.WriteBoolean(b); }
        public void Send(byte b) { writer.WriteByte(b); }
        public void Send(int x) { writer.WriteInt32(x); }
        public void Send(float f) { writer.WriteSingle(f); }
        public void Send(Vector3 v) { writer.WriteVector3(v); }
        public void Send(Quaternion q) { writer.WriteQuaternion(q); }
        #endregion

        #region Receive
        public bool ReceiveBool() { return reader.ReadBoolean(); }
        public byte ReceiveByte() { return reader.ReadByte(); }
        public int ReceiveInt() { return reader.ReadInt32(); }
        public float ReceiveFloat() { return reader.ReadSingle(); }
        public Vector3 ReceiveVector3() { return reader.ReadVector3(); }
        public Quaternion ReceiveQuaternion() { return reader.ReadQuaternion(); }
        #endregion
#endif
    }
}