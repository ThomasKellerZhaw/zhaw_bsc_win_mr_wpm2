﻿using System.Collections.Generic;
using UnityEngine;

namespace Passer {

    public enum Side {
        AnySide,
        Left,
        Right,
    }

    public abstract class Target : MonoBehaviour {
        /// <summary>
        /// Main targeted bone, matches the target
        /// </summary>

        public bool showRealObjects = true;

        public virtual void InitComponent() { }

        public static bool IsNotInitialized(Quaternion q) {
            return (q.x == 0 && q.y == 0 && q.z == 0 && q.w == 0);
        }

        public abstract void StartTarget();
        public abstract void InitSensors();
        public virtual void StartSensors() { }
        protected virtual void UpdateSensors() { }
        public virtual void StopSensors() { }
        public abstract void UpdateTarget();

        public static List<Collider> SetColliderToTrigger(GameObject obj) {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb != null)
                return SetColliderToTrigger(rb);
            else {
                List<Collider> changedColliders = new List<Collider>();

                Collider[] colliders = obj.GetComponentsInChildren<Collider>();
                for (int j = 0; j < colliders.Length; j++) {
                    if (!colliders[j].isTrigger) {
                        colliders[j].isTrigger = true;
                        changedColliders.Add(colliders[j]);
                    }
                }

                return changedColliders;
            }
        }

        public static List<Collider> SetColliderToTrigger(Rigidbody rb) {
            List<Collider> changedColliders = new List<Collider>();

            Collider[] colliders = rb.GetComponentsInChildren<Collider>();
            for (int j = 0; j < colliders.Length; j++) {
                Rigidbody colliderRigidbody = colliders[j].attachedRigidbody;
                if (colliderRigidbody == null || colliderRigidbody == rb) {
                    if (!colliders[j].isTrigger) {
                        colliders[j].isTrigger = true;
                        changedColliders.Add(colliders[j]);
                    }
                }
            }
            return changedColliders;
        }

        public static void UnsetColliderToTrigger(List<Collider> colliders) {
            if (colliders == null)
                return;

            foreach (Collider c in colliders) {
                if (c != null)
                    c.isTrigger = false;
            }
        }

        public static void UnsetColliderToTrigger(List<Collider> colliders, Collider collider) {
            if (colliders == null || collider == null)
                return;

            if (colliders.Contains(collider))
                collider.isTrigger = false;
        }
    }
}