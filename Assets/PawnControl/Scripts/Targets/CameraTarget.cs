﻿using UnityEngine;

namespace Passer {

    public class CameraTarget : Target {

        public PawnControl pawn;

        public static CameraTarget Create(PawnControl pawn) {
            GameObject targetObject = new GameObject();
            targetObject.name = "Camera";
            Transform targetTransform = targetObject.transform;

            targetTransform.parent = pawn.transform;

            if (pawn.characterController != null)
                targetTransform.localPosition = pawn.transform.position + new Vector3(0, pawn.characterController.height * 0.75F, pawn.characterController.radius);
            else
                targetTransform.localPosition = new Vector3(0, 2, 0);
            targetTransform.localRotation = Quaternion.identity;

            CameraTarget cameraTarget = targetTransform.gameObject.AddComponent<CameraTarget>();
            cameraTarget.pawn = pawn;

            pawn.cameraTarget = cameraTarget;

            return cameraTarget;
        }

        #region Sensors
        public UnityCameraComponent unity;

        public override void InitSensors() {
            if (pawn == null)
                return;

            if (!pawn.isRemote) {
                if (unity == null) {
                    unity = UnityCameraComponent.Get(pawn.unityComponent);
                    unity.transform.position = transform.position;
                    unity.transform.rotation = transform.rotation;
                }
                pawn.unityComponent.unityCamera = unity;
            }

        }

        public void ShowSensors(bool shown) {
            if (unity != null)
                unity.Show(shown && showRealObjects);
        }
        #endregion

        #region Configuration
        public Vector3 neck2eyes;

        public virtual Vector3 GetNeckEyeDelta() {
            if (pawn.characterController != null) {
                Vector3 neckPosition = pawn.characterController.transform.position + pawn.characterController.center + Vector3.up * pawn.characterController.height / 4;
                Vector3 neck2eyes = this.transform.position - neckPosition;
                return neck2eyes;
            }
            return new Vector3(0, 0.15F, 0.15F);
        }

        virtual public Vector3 GetNeckPosition() {
            Vector3 neckPosition = transform.TransformPoint(-neck2eyes);
            return neckPosition;
        }

        #endregion Configuration

        #region Init
        public override void StartTarget() {
            InitSensors();
#if hOPENVR || hOCULUS
            //unityVRHead.tracker.enabled = UnityVRDevice.xrDevice != UnityVRDevice.XRDeviceType.None;
#endif
            neck2eyes = GetNeckEyeDelta();
        }

        #endregion

        #region Update

        protected bool calibrated = false;

        /// <summary>Update all head sensors</summary>
        public override void UpdateTarget() {
            UpdateSensors();

            if (!pawn.isRemote) {
                transform.rotation = unity.transform.rotation;
                transform.position = unity.transform.position;
            }

            if (!calibrated && pawn.calibrateAtStart) {
                pawn.Calibrate();
                calibrated = true;
            }
        }

        #endregion

        #region HeadPose

        /// <summary>Sets the rotation of the HMD around the X axis</summary>
        public void RotationX(float xAngle) {
            Vector3 neckPosition = transform.TransformPoint(-neck2eyes);

            Quaternion localHmdRotation = Quaternion.Inverse(pawn.transform.rotation) * transform.rotation;
            Vector3 angles = localHmdRotation.eulerAngles;
            transform.rotation = pawn.transform.rotation * Quaternion.Euler(xAngle, angles.y, angles.z);

            transform.position = neckPosition + transform.rotation * neck2eyes;
        }

        /// <summary>Sets the rotation of the HMD around the Y axis</summary>
        public void RotationY(float yAngle) {
            Vector3 neckPosition = transform.TransformPoint(-neck2eyes);

            Quaternion localHmdRotation = Quaternion.Inverse(pawn.transform.rotation) * transform.rotation;
            Vector3 angles = localHmdRotation.eulerAngles;
            transform.rotation = pawn.transform.rotation * Quaternion.Euler(angles.x, yAngle, angles.z);

            transform.position = neckPosition + transform.rotation * neck2eyes;
        }
        #endregion
    }
}