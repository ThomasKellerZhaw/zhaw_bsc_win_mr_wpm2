﻿using UnityEngine;
#if UNITY_2017_2_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
#endif

namespace Passer {

    public class ControllerTarget : Target {
        public PawnControl pawn;

        public Rigidbody controllerRigidbody;
        protected ControllerPhysics controllerPhysics;

        public Side side;
        public bool isLeft {
            get { return side == Side.Left; }
        }
        public bool isRight {
            get { return side == Side.Right; }
        }
        public ControllerTarget otherController {
            get {
                return isLeft ? pawn.rightControllerTarget : pawn.leftControllerTarget;
            }
        }

        public bool twoHandedGrab = false;

        public Vector3 outward;

        public static ControllerTarget Create(PawnControl pawn, bool isLeft) {
            GameObject targetObject = new GameObject();
            if (isLeft)
                targetObject.name = "Left Controller";
            else
                targetObject.name = "Right Controller";
            Transform targetTransform = targetObject.transform;

            targetTransform.parent = pawn.transform;

            if (pawn.characterController != null)
                targetTransform.localPosition = pawn.transform.position + new Vector3(isLeft ? -pawn.characterController.radius : pawn.characterController.radius, pawn.characterController.height * 0.5F, 0);
            else
                targetTransform.localPosition = new Vector3(isLeft ? -0.3F : 0.3F, 1, 0);
            targetTransform.localRotation = Quaternion.identity;

            ControllerTarget controllerTarget = targetTransform.gameObject.AddComponent<ControllerTarget>();
            controllerTarget.pawn = pawn;

            if (isLeft) {
                controllerTarget.side = Side.Left;
                controllerTarget.outward = Vector3.left;
                pawn.leftControllerTarget = controllerTarget;
            }
            else {
                controllerTarget.side = Side.Right;
                controllerTarget.outward = Vector3.right;
                pawn.rightControllerTarget = controllerTarget;
            }

            return controllerTarget;
        }

        #region Sensors
        public UnityControllerComponent unity;

        public override void InitSensors() {
            if (pawn == null)
                return;

            if (!pawn.isRemote) {
                if (unity == null) {
                    unity = UnityControllerComponent.Get(pawn.unityComponent, isLeft);
                    unity.transform.position = transform.position;
                    unity.transform.rotation = transform.rotation;
                }
                if (isLeft)
                    pawn.unityComponent.leftController = unity;
                else
                    pawn.unityComponent.rightController = unity;
            }
        }

        public void ShowSensors(bool shown) {
            if (unity != null)
                unity.Show(shown && showRealObjects);
        }
        #endregion

        #region Settings

        public bool physics;

        #endregion

        #region Start
        public void InitTarget() {
            InitSensors();
            if (grabSocket == null)
                grabSocket = ControllerInteraction.CreateGrabSocket(this);
        }

        public override void StartTarget() {
            side = isLeft ? Side.Left : Side.Right;

#if UNITY_2017_2_OR_NEWER
            if (!XRSettings.enabled) { 
#else
            if (!VRSettings.enabled) {
#endif
                this.gameObject.SetActive(false);
                return;
            }

            InitSensors();
            ShowSensors(pawn.showRealObjects);

            CheckRigidbody();
            CheckColliders();

            if (grabSocket == null)
                grabSocket = ControllerInteraction.CreateGrabSocket(this);

            if (physics) {
                controllerPhysics = GetComponent<ControllerPhysics>();
                if (controllerPhysics == null)
                    gameObject.AddComponent<ControllerPhysics>();
            }
        }
        #endregion

        #region Update

        public override void UpdateTarget() {
#if UNITY_2017_2_OR_NEWER
            if (!XRSettings.enabled)
                return;
#else
            if (!VRSettings.enabled)
                return;
#endif
            if (pawn.isRemote)
                return;

            if (physics) {
                if (controllerRigidbody == null)
                    controllerRigidbody = GetComponent<Rigidbody>();
                return;
            }

            transform.position = unity.transform.position;

            if (grabbedObject != null) {

                // Kinematic Limitations
                Handle handle = grabSocket.attachedHandle;
                if (handle != null) {
                    Rigidbody handleRigidbody = handle.GetComponentInParent<Rigidbody>();
                    if (handleRigidbody != null) {
                        KinematicLimitations kinematicLimitations = handleRigidbody.GetComponent<KinematicLimitations>();
                        if (kinematicLimitations != null && kinematicLimitations.enabled) {
                            Vector3 correctionVector = kinematicLimitations.GetCorrectionVector();
                            transform.position += correctionVector;
                            Quaternion correctionRotation = kinematicLimitations.GetCorrectionRotation();
                            handleRigidbody.transform.rotation *= correctionRotation;
                            return;
                        }
                    }
                }

                // two handed grab
                if (twoHandedGrab) {
                    // this is the primary grabbing controller
                    // otherController is the secondary grabbing controller

                    // This assumes the socket is a child of the controllertarget
                    Vector3 otherSocketLocalPosition = otherController.grabSocket.transform.localPosition;
                    // Calculate socket position from unity tracker
                    Vector3 otherSocketPosition = otherController.unity.transform.TransformPoint(otherSocketLocalPosition);

                    Vector3 handlePosition = otherController.grabSocket.attachedHandle.worldPosition;
                    Vector3 toHandlePosition = handlePosition - transform.position;
                    Quaternion rotateToHandlePosition = Quaternion.FromToRotation(toHandlePosition, transform.forward);

                    transform.LookAt(otherSocketPosition, unity.transform.up);
                    transform.rotation *= rotateToHandlePosition;
                }
                else
                    transform.rotation = unity.transform.rotation;
            }
            else
                transform.rotation = unity.transform.rotation;
        }
        #endregion

        #region Interaction

        public Socket grabSocket;

        /// <summary>Calculate socket position from tracking position</summary>
        protected Vector3 GetSocketPosition() {
            // Assums that the grabsocket is a child of the target transform
            Vector3 localSocketPosition = grabSocket.transform.localPosition;

            Vector3 socketPosition = unity.transform.TransformPoint(localSocketPosition);
            return socketPosition;
        }

        //[System.NonSerialized]
        public GameObject touchedObject = null;
        public GameObject grabbedObject = null;
        public Handle grabbedHandle = null;
        public bool grabbedRigidbody = false;
        //public RigidbodyData grabbedRigidbodyData;

        /// <summary>Grab the object</summary>
        public void Grab(GameObject obj) {
            ControllerInteraction.NetworkedGrab(this, obj, false);
        }

        /// <summary>Try to grab the object we touch</summary>
        public void GrabTouchedObject() {
            if (touchedObject != null)
                Grab(touchedObject);
        }

        public void LetGo() {
            ControllerInteraction.NetworkedLetGo(this);
        }

        #endregion

        #region Collisions

        protected void CheckRigidbody() {
            controllerRigidbody = GetComponent<Rigidbody>();
            if (controllerRigidbody == null)
                controllerRigidbody = gameObject.AddComponent<Rigidbody>();
            controllerRigidbody.isKinematic = true;
            controllerRigidbody.useGravity = false;
        }

        private void CheckColliders() {
            Collider[] colliders = transform.GetComponentsInChildren<Collider>();
            // Does not work if the hand has grabbed an object with colliders...
            if (colliders.Length == 0)
                colliders = GenerateColliders();

            if (!physics) {
                foreach (Collider collider in colliders)
                    collider.isTrigger = true;
            }
        }

        private Collider[] GenerateColliders() {
            SphereCollider collider = gameObject.AddComponent<SphereCollider>();
            collider.center = new Vector3(0, -0.06F, -0.06F);
            collider.radius = 0.1F;

            return new Collider[] { collider };
        }

        public void OnTriggerStay(Collider other) {
            if (physics)
                return;

            if (other.attachedRigidbody != null)
                touchedObject = other.attachedRigidbody.gameObject;
            //else
            //    touchedObject = other.gameObject;
        }

        public void OnTriggerExit(Collider other) {
            if (physics)
                return;

            touchedObject = null;
        }
        #endregion
    }
}