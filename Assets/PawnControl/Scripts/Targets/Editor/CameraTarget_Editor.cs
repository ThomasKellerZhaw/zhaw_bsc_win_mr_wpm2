﻿using UnityEditor;
using UnityEngine;

namespace Passer {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(CameraTarget))]
    public class CameraTarget_Editor : Editor {
        private CameraTarget cameraTarget;

        private TargetProps[] allProps;

        #region Enable
        public void OnEnable() {
            cameraTarget = (CameraTarget)target;
            cameraTarget.pawn = GetPawn(cameraTarget);

            InitEditors();

            cameraTarget.InitSensors();
            InitSensors();
            InitEvents();
        }

        private void InitEditors() {
            allProps = new TargetProps[] {
            };
        }
        #endregion

        #region Disable
        public void OnDisable() {
            if (cameraTarget.pawn == null) {
                // This target is not connected to a pawn, so we delete it
                DestroyImmediate(cameraTarget, true);
                return;
            }

            if (!Application.isPlaying) {
                SetSensor2Target();
            }
        }

        private void SetSensor2Target() {
            if (allProps != null)
                foreach (TargetProps props in allProps)
                    props.SetSensor2Target();
        }
        #endregion

        #region Inspector
        public static CameraTarget Inspector(string name, PawnControl pawn) {
            EditorGUILayout.BeginHorizontal();
            CameraTarget cameraTarget = pawn.cameraTarget;
            if (cameraTarget == null || cameraTarget.transform == null) {
                if (!Application.isPlaying) {
                    EditorGUILayout.LabelField(name);
                    if (GUILayout.Button("Show", GUILayout.MinWidth(60))) {
                        cameraTarget = CameraTarget.Create(pawn);
                        cameraTarget.InitSensors();
                        cameraTarget.ShowSensors(pawn.showRealObjects);
                    }
                }
            }
            else {
                EditorGUI.BeginDisabledGroup(true);
                GUIContent text = new GUIContent(
                    name,
                    "The transform controlling the " + name
                    );
                EditorGUILayout.ObjectField(text, cameraTarget.transform, typeof(Transform), true);
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
            return cameraTarget;
        }

        public override void OnInspectorGUI() {
            if (cameraTarget == null || cameraTarget.pawn == null)
                return;

            serializedObject.Update();

            SensorInspectors(cameraTarget);

            SettingsInspector(cameraTarget);

            EventsInspector();
            GazeInteractionButton(cameraTarget);

            serializedObject.ApplyModifiedProperties();
        }

        private static PawnControl GetPawn(CameraTarget target) {
            PawnControl[] pawns = FindObjectsOfType<PawnControl>();
            PawnControl foundPawn = null;

            for (int i = 0; i < pawns.Length; i++)
                if (pawns[i].cameraTarget.transform == target.transform)
                    foundPawn = pawns[i];

            return foundPawn;
        }

        #region Sensors
        private void InitSensors() {
        }

        //private bool showSensors = true;
        private void SensorInspectors(CameraTarget cameraTarget) {
            //showSensors = EditorGUILayout.Foldout(showSensors, "Sensors", true);
            //if (showSensors) {
            //    EditorGUI.indentLevel++;
            //    //FirstPersonCameraInspector(cameraTarget);

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Tracker Transform", cameraTarget.unity.transform, typeof(Transform), true);
            EditorGUI.EndDisabledGroup();

            //    foreach (TargetProps props in allProps)
            //        props.Inspector();

            //    EditorGUI.indentLevel--;
            //}
        }

        //private void FirstPersonCameraInspector(CameraTarget cameraTarget) {
        //    if (cameraTarget.unity == null)
        //        return;

        //    bool wasEnabled = cameraTarget.unity.enabled;

        //    EditorGUILayout.BeginHorizontal();
        //    EditorGUI.BeginChangeCheck();

        //    bool enabled = EditorGUILayout.ToggleLeft(cameraTarget.unity.name, cameraTarget.unity.enabled, GUILayout.MinWidth(80));
        //    if (EditorGUI.EndChangeCheck()) {
        //        Undo.RecordObject(cameraTarget, enabled ? "Enabled " : "Disabled " + cameraTarget.unity.name);
        //        cameraTarget.unity.enabled = enabled;
        //    }
        //    EditorGUILayout.EndHorizontal();

        //    if (!Application.isPlaying) {
        //        UnityCamera.CheckCamera(cameraTarget);
        //        if (!wasEnabled && cameraTarget.unity.enabled) {
        //            UnityCamera.AddCamera(cameraTarget);
        //        }
        //        else if (wasEnabled && !cameraTarget.unity.enabled) {
        //            UnityCamera.RemoveCamera(cameraTarget);
        //        }
        //    }
        //}

        #endregion

        #region Settings
        protected bool showSettings;
        protected virtual void SettingsInspector(CameraTarget camearTarget) {
            //showSettings = EditorGUILayout.Foldout(showSettings, "Settings", true);
            //if (showSettings) {
            //    EditorGUI.indentLevel++;

            //    EditorGUI.indentLevel--;
            //}
        }
        #endregion

        #region Events
        //protected SerializedProperty audioEventProp;
        protected virtual void InitEvents() {
            //audioEventProp = serializedObject.FindProperty("audioEvent");
        }

        protected int selectedEvent;

        protected bool showEvents;
        protected virtual void EventsInspector() {
            //showEvents = EditorGUILayout.Foldout(showEvents, "Events", true);
            //if (showEvents) {
            //    EditorGUI.indentLevel++;

            //    EditorGUILayout.BeginHorizontal();

            //    // Labels
            //    EditorGUILayout.BeginVertical(GUILayout.MinWidth(110));

            //    GUILayout.Space(3);
            //    EditorGUILayout.LabelField("Audio", GUILayout.Width(110));
            //    EditorGUILayout.EndVertical();

            //    // Buttons
            //    string[] buttonTexts = new string[1];
            //    buttonTexts[0] = Event.GetInputButtonLabel(cameraTarget.audioEvent.floatEvent);

            //    int oldFontSize = GUI.skin.button.fontSize;
            //    GUI.skin.button.fontSize = 9;
            //    selectedEvent = GUILayout.SelectionGrid(selectedEvent, buttonTexts, 1);
            //    GUI.skin.button.fontSize = oldFontSize;

            //    EditorGUILayout.EndHorizontal();

            //    // Details
            //    GUIStyle style = new GUIStyle(GUI.skin.label) {
            //        fontStyle = FontStyle.Bold
            //    };
            //    EditorGUILayout.LabelField("Details", style, GUILayout.ExpandWidth(true));

            //    EditorGUI.indentLevel++;
            //    EventDetails(selectedEvent);
            //    EditorGUI.indentLevel--;

            //    EditorGUI.indentLevel--;
            //}
        }

        protected void EventDetails(int selectedEvent) {
            //switch (selectedEvent) {
            //    case 0:
            //        FloatEvent_Editor.DetailsInspector(audioEventProp, "Audio");
            //        break;
            //}
        }

        #endregion

        #region Buttons
        private void GazeInteractionButton(CameraTarget cameraTarget) {
            InteractionPointer interactionPointer = cameraTarget.transform.GetComponentInChildren<InteractionPointer>();
            if (interactionPointer != null)
                return;

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Interaction Pointer"))
                AddInteractionPointer();
            //if (GUILayout.Button("Add Teleporter"))
            //    AddTeleporter();
            GUILayout.EndHorizontal();
        }

        private void AddInteractionPointer() {
            InteractionPointer pointer = InteractionPointer.Add(cameraTarget.transform, InteractionPointer.PointerType.FocusPoint);
            //Camera fpCamera = UnityCamera.GetCamera(cameraTarget);
            //if (fpCamera != null) {
                pointer.transform.position = cameraTarget.transform.position;
                pointer.transform.rotation = cameraTarget.transform.rotation;
            //}
            pointer.focusPointObj.transform.localPosition = new Vector3(0, 0, 2);

            ControllerInput controllerInput = cameraTarget.pawn.GetComponent<ControllerInput>();
            if (controllerInput != null)
                //ControllerEvent_Editor.SetBoolMethod(controllerInput.gameObject, controllerInput.GetInputEvent(true, ControllerInput.SideButton.Button1), EventHandler.Type.OnStart, pointer.Click);
                controllerInput.SetEventHandler(true, ControllerInput.SideButton.Button1, pointer.Click);
        }

        //private void AddTeleporter() {
        //    Teleporter teleporter = Teleporter.Add(cameraTarget.transform, InteractionPointer.PointerType.FocusPoint);
        //    if (cameraTarget.unityVRHead.cameraTransform != null) {
        //        teleporter.transform.position = cameraTarget.unityVRHead.cameraTransform.position;
        //        teleporter.transform.rotation = cameraTarget.unityVRHead.cameraTransform.rotation;
        //    }
        //    teleporter.focusPointObj.transform.localPosition = new Vector3(0, 0, 2);

        //    ControllerInput controllerInput = cameraTarget.humanoid.GetComponent<ControllerInput>();
        //    if (controllerInput != null)
        //        ControllerEvent_Editor.SetBoolMethod(controllerInput.GetInputEvent(true, ControllerInput.SideButton.Button1), Event.Type.OnStart, teleporter.Click);

        //}
        #endregion

        #endregion

        public abstract class TargetProps {
            public SerializedProperty enabledProp;
            public SerializedProperty sensorTransformProp;
            public SerializedProperty sensor2TargetPositionProp;
            public SerializedProperty sensor2TargetRotationProp;

            public CameraTarget cameraTarget;
            public CameraSensor sensor;

            public TargetProps(SerializedObject serializedObject, CameraSensor _sensor, CameraTarget _cameraTarget, string unitySensorName) {
                enabledProp = serializedObject.FindProperty(unitySensorName + ".enabled");
                sensorTransformProp = serializedObject.FindProperty(unitySensorName + ".sensorTransform");
                sensor2TargetPositionProp = serializedObject.FindProperty(unitySensorName + ".sensor2TargetPosition");
                sensor2TargetRotationProp = serializedObject.FindProperty(unitySensorName + ".sensor2TargetRotation");

                cameraTarget = _cameraTarget;
                sensor = _sensor;

                sensor.target = cameraTarget;
            }

            public virtual void SetSensor2Target() {
                //if (sensor.sensorTransform == null)
                //    return;

                //sensor2TargetRotationProp.quaternionValue = Quaternion.Inverse(sensor.sensorTransform.rotation) * headTarget.head.target.transform.rotation;
                //sensor2TargetPositionProp.vector3Value = -headTarget.head.target.transform.InverseTransformPoint(sensor.sensorTransform.position);
            }

            public abstract void Inspector();
        }
    }
}