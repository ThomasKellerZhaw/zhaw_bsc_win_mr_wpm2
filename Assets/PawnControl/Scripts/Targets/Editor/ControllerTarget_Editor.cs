﻿using UnityEditor;
using UnityEngine;

namespace Passer {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(ControllerTarget))]
    public class ControllerTarget_Editor : Editor {
        protected ControllerTarget controllerTarget;

        private TargetProps[] allProps;

        #region Enable
        public void OnEnable() {
            controllerTarget = (ControllerTarget)target;

            controllerTarget.pawn = GetPawn(controllerTarget);
            if (controllerTarget.pawn == null)
                return;

            InitEditors();

            controllerTarget.InitTarget();

            InitSettings();
            InitOther();
            //InitEvents();

            if (!Application.isPlaying)
                SetSensor2Target();
        }

        protected virtual void InitEditors() {
            allProps = new TargetProps[] {
            };
        }
        #endregion

        #region Disable
        public void OnDisable() {
            if (controllerTarget.pawn == null) {
                // This target is not connected to a pawn, so we delete it
                DestroyImmediate(controllerTarget, true);
                return;
            }

            if (!Application.isPlaying) {
                SetSensor2Target();
            }
        }

        private void SetSensor2Target() {
            foreach (TargetProps props in allProps)
                props.SetSensor2Target();
        }
        #endregion

        #region Inspector

        public override void OnInspectorGUI() {
            if (controllerTarget == null || controllerTarget.pawn == null)
                return;

            serializedObject.Update();

            SensorInspectors(controllerTarget);
            SettingsInspector(controllerTarget);

            TouchedObjectInspector(controllerTarget);
            GrabbedObjectInspector(controllerTarget);

            //EventsInspector();

            InteractionPointerButton(controllerTarget);

            serializedObject.ApplyModifiedProperties();
        }

        public static ControllerTarget Inspector(string name, PawnControl pawn, bool isLeft) {
            EditorGUILayout.BeginHorizontal();
            ControllerTarget controllerTarget = isLeft ? pawn.leftControllerTarget : pawn.rightControllerTarget;
            if (controllerTarget == null || controllerTarget.transform == null) {
                if (!Application.isPlaying) {
                    EditorGUILayout.LabelField(name);
                    if (GUILayout.Button("Show", GUILayout.MinWidth(60))) {
                        controllerTarget = ControllerTarget.Create(pawn, isLeft);
                        controllerTarget.InitSensors();
                        controllerTarget.ShowSensors(pawn.showRealObjects);
                    }
                }
            }
            else {
                EditorGUI.BeginDisabledGroup(true);
                GUIContent text = new GUIContent(
                    name,
                    "The transform controlling the " + name
                    );
                EditorGUILayout.ObjectField(text, controllerTarget.transform, typeof(Transform), true);
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
            return controllerTarget;
        }

        public static PawnControl GetPawn(ControllerTarget target) {
            PawnControl[] pawns = FindObjectsOfType<PawnControl>();
            PawnControl foundPawn = null;

            for (int i = 0; i < pawns.Length; i++)
                if ((pawns[i].leftControllerTarget != null && pawns[i].leftControllerTarget.transform == target.transform) ||
                    (pawns[i].rightControllerTarget != null && pawns[i].rightControllerTarget.transform == target.transform))
                    foundPawn = pawns[i];

            return foundPawn;
        }

        #region Sensors
        //public bool showSensors = true;
        private void SensorInspectors(ControllerTarget controllerTarget) {
            //showSensors = EditorGUILayout.Foldout(showSensors, "Sensors", true);
            //if (showSensors) {
            //    EditorGUI.indentLevel++;

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Tracker Transform", controllerTarget.unity.transform, typeof(Transform), true);
            EditorGUI.EndDisabledGroup();

            //    foreach (TargetProps props in allProps)
            //        props.Inspector();

            //    EditorGUI.indentLevel--;
            //}
        }

        #endregion

        #region Settings

        private SerializedProperty showRealObjectsProp;
        private SerializedProperty physicsProp;
        //private SerializedProperty touchInteractionProp;

        private void InitSettings() {
            showRealObjectsProp = serializedObject.FindProperty("showRealObjects");

            physicsProp = serializedObject.FindProperty("physics");
            //touchInteractionProp = serializedObject.FindProperty("touchInteraction");
        }

        public bool showSettings;
        private void SettingsInspector(ControllerTarget controllerTarget) {
            showSettings = EditorGUILayout.Foldout(showSettings, "Settings", true);
            if (showSettings) {
                EditorGUI.indentLevel++;

                ShowRealObjectsInspector();

                //touchInteractionProp.boolValue = EditorGUILayout.Toggle("Touch Interaction", touchInteractionProp.boolValue);

                PhysicsInspector();

                EditorGUI.indentLevel--;
            }
        }
        
        protected void ShowRealObjectsInspector() {
            GUIContent text = new GUIContent(
                "Show Real Objects",
                "Shows real physical controllers at their actual location"
                );
            bool showRealObjects = EditorGUILayout.Toggle(text, showRealObjectsProp.boolValue);
            if (showRealObjects != controllerTarget.showRealObjects) {
                showRealObjectsProp.boolValue = showRealObjects;
                controllerTarget.ShowSensors(showRealObjects);
            }
        }

        protected void PhysicsInspector() {
            GUIContent text = new GUIContent(
                "Physics",
                "Enables collisions and physics on grabbed objects"
                );
            physicsProp.boolValue = EditorGUILayout.Toggle(text, physicsProp.boolValue);
        }

        #endregion

        #region Other
        private SerializedProperty grabbedObjectProp;
        private void InitOther() {
            grabbedObjectProp = serializedObject.FindProperty("grabbedObject");
        }

        private void TouchedObjectInspector(ControllerTarget handTarget) {
            handTarget.touchedObject = (GameObject)EditorGUILayout.ObjectField("Touched Object", handTarget.touchedObject, typeof(GameObject), true);
        }

        private void GrabbedObjectInspector(ControllerTarget handTarget) {
            GameObject grabbedObject = (GameObject)EditorGUILayout.ObjectField("Grabbed Object", grabbedObjectProp.objectReferenceValue, typeof(GameObject), true);

            if (grabbedObject != grabbedObjectProp.objectReferenceValue) {
                if (grabbedObject != null)
                    GrabObject(handTarget, grabbedObject);
                else
                    LetGoObject(handTarget);
            }
        }

        private void GrabObject(ControllerTarget handTarget, GameObject obj) {
            if (handTarget.otherController.grabbedObject == obj)
                LetGoObject(handTarget.otherController);

            ControllerInteraction.NetworkedGrab(handTarget, obj, false);
            if (handTarget.grabbedObject == null)
                Debug.LogWarning("Could not grab object");
            else {
                Handle handle = obj.GetComponent<Handle>();
                if (handle == null) {
                    Handle.Create(obj, handTarget);
                }
            }
        }

        private void LetGoObject(ControllerTarget handTarget) {
            ControllerInteraction.LetGo(handTarget);
            grabbedObjectProp.objectReferenceValue = null;
        }

        #endregion

        #region Events
        /*
        protected SerializedProperty touchEventProp;
        protected SerializedProperty grabEventProp;
        protected SerializedProperty poseEventProp;

        protected virtual void InitEvents() {
            touchEventProp = serializedObject.FindProperty("touchEvent");
            grabEventProp = serializedObject.FindProperty("grabEvent");
            poseEventProp = serializedObject.FindProperty("poseEvent");
        }

        protected int selectedEvent;

        protected bool showEvents;
        protected virtual void EventsInspector() {
            showEvents = EditorGUILayout.Foldout(showEvents, "Events", true);
            if (showEvents) {
                EditorGUI.indentLevel++;

                EditorGUILayout.BeginHorizontal();

                //Labels
                EditorGUILayout.BeginVertical(GUILayout.MinWidth(110));

                GUILayout.Space(3);
                EditorGUILayout.LabelField("Touch", GUILayout.Width(110));
                GUILayout.Space(1);
                EditorGUILayout.LabelField("Grab", GUILayout.Width(110));
                GUILayout.Space(1);
                EditorGUILayout.LabelField("Pose", GUILayout.Width(110));

                EditorGUILayout.EndVertical();

                // Buttons
                string[] buttonTexts = new string[3];
                buttonTexts[0] = Event.GetInputButtonLabel(controllerTarget.touchEvent.gameObjectEvent);
                buttonTexts[1] = Event.GetInputButtonLabel(controllerTarget.grabEvent.gameObjectEvent);
                buttonTexts[2] = Event.GetInputButtonLabel(controllerTarget.poseEvent.poseEvent);

                int oldFontSize = GUI.skin.button.fontSize;
                GUI.skin.button.fontSize = 9;
                selectedEvent = GUILayout.SelectionGrid(selectedEvent, buttonTexts, 1);
                GUI.skin.button.fontSize = oldFontSize;

                EditorGUILayout.EndHorizontal();

                // Details
                GUIStyle style = new GUIStyle(GUI.skin.label) {
                    fontStyle = FontStyle.Bold
                };
                EditorGUILayout.LabelField("Details", style, GUILayout.ExpandWidth(true));

                EditorGUI.indentLevel++;
                EventDetails(selectedEvent);
                EditorGUI.indentLevel--;

                EditorGUI.indentLevel--;
            }
        }

        protected void EventDetails(int selectedEvent) {
            switch (selectedEvent) {
                case 0:
                    GameObjectEvent_Editor.DetailsInspector(touchEventProp, "Touch");
                    break;
                case 1:
                    GameObjectEvent_Editor.DetailsInspector(grabEventProp, "Grab");
                    break;
                case 2:
                    PoseEvent_Editor.DetailsInspector(poseEventProp, "Pose");
                    break;
            }
        }
        */
        #endregion

        #region Buttons
        private void InteractionPointerButton(ControllerTarget handTarget) {
            InteractionPointer interactionPointer = handTarget.transform.GetComponentInChildren<InteractionPointer>();
            if (interactionPointer != null)
                return;

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Interaction Pointer"))
                AddInteractionPointer();
            if (GUILayout.Button("Add Teleporter"))
                AddTeleporter();
            GUILayout.EndHorizontal();
        }

        private void AddInteractionPointer() {
            InteractionPointer pointer = InteractionPointer.Add(controllerTarget.transform, InteractionPointer.PointerType.Ray);
            pointer.transform.localPosition = Vector3.zero;
            pointer.transform.localRotation = Quaternion.identity;
            pointer.active = false;

            ControllerInput controllerInput = controllerTarget.pawn.GetComponent<ControllerInput>();
            if (controllerInput != null) {
                //ControllerEventHandlers button1Input = controllerInput.GetInputEvent(controllerTarget.isLeft, ControllerInput.SideButton.Button1);
                //ControllerEvent_Editor.SetBoolMethod(controllerInput.gameObject, button1Input, EventHandler.Type.OnChange, pointer.Activation);
                controllerInput.SetEventHandler(controllerTarget.isLeft, ControllerInput.SideButton.Button1, pointer.Activation);

                //ControllerEventHandlers trigger1Input = controllerInput.GetInputEvent(controllerTarget.isLeft, ControllerInput.SideButton.Trigger1);
                //ControllerEvent_Editor.SetBoolMethod(controllerInput.gameObject, trigger1Input, EventHandler.Type.OnChange, pointer.Click);
                controllerInput.SetEventHandler(controllerTarget.isLeft, ControllerInput.SideButton.Trigger1, pointer.Click);
            }
        }

        private void AddTeleporter() {
            Teleporter teleporter = Teleporter.Add(controllerTarget.transform);
            teleporter.transform.localPosition = Vector3.zero;
            teleporter.transform.localRotation = Quaternion.identity;
            teleporter.active = false;

            ControllerInput controllerInput = controllerTarget.pawn.GetComponent<ControllerInput>();
            if (controllerInput != null) {
                //ControllerEventHandlers button1Input = controllerInput.GetInputEvent(controllerTarget.isLeft, ControllerInput.SideButton.Button1);
                //ControllerEvent_Editor.SetBoolMethod(controllerInput.gameObject, button1Input, EventHandler.Type.OnChange, teleporter.Activation);
                controllerInput.SetEventHandler(controllerTarget.isLeft, ControllerInput.SideButton.Button1, teleporter.Activation);

                //ControllerEventHandlers trigger1Input = controllerInput.GetInputEvent(controllerTarget.isLeft, ControllerInput.SideButton.Trigger1);
                //ControllerEvent_Editor.SetBoolMethod(controllerInput.gameObject, trigger1Input, EventHandler.Type.OnChange, teleporter.Click);
                controllerInput.SetEventHandler(controllerTarget.isLeft, ControllerInput.SideButton.Trigger1, teleporter.Click);
            }
        }
        #endregion

        #endregion

        public abstract class TargetProps {
            public SerializedProperty enabledProp;
            public SerializedProperty sensorTransformProp;
            public SerializedProperty sensor2TargetPositionProp;
            public SerializedProperty sensor2TargetRotationProp;

            public ControllerTarget controllerTarget;
            public ControllerSensor sensor;

            public TargetProps(SerializedObject serializedObject, ControllerSensor _sensor, ControllerTarget _controllerTarget, string unitySensorName) {
                enabledProp = serializedObject.FindProperty(unitySensorName + ".enabled");
                sensorTransformProp = serializedObject.FindProperty(unitySensorName + ".sensorTransform");
                sensor2TargetPositionProp = serializedObject.FindProperty(unitySensorName + ".sensor2TargetPosition");
                sensor2TargetRotationProp = serializedObject.FindProperty(unitySensorName + ".sensor2TargetRotation");

                controllerTarget = _controllerTarget;
                sensor = _sensor;

                sensor.target = controllerTarget;
            }

            public virtual void SetSensor2Target() {
                //sensor.SetSensor2Target();
            }

            public abstract void Inspector();
        }
    }
}
