﻿using UnityEngine;

namespace Passer {

    public enum TrackerId {
        Oculus,
        SteamVR,
        WindowsMR,
        LeapMotion,
        Kinect1,
        Kinect2,
        OrbbecAstra,
        Realsense,
        RazerHydra,
        Optitrack,
        Tobii
    };

    public class TrackerComponent : MonoBehaviour {

        public Tracker.Status status;

        protected Transform realWorld;

        void Start() {

        }

        void Update() {

        }
    }

}