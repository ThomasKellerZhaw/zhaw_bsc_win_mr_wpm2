﻿namespace Passer {

    public class CameraSensor : Sensor {
        protected CameraTarget cameraTarget {
            get { return (CameraTarget)target; }
        }
    }

}