﻿namespace Passer {

    public class ControllerSensor : Sensor {
        protected ControllerTarget controllerTarget {
            get { return (ControllerTarget)target; }
        }
    }

}