﻿using UnityEngine;
#if UNITY_2017_2_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
#endif

namespace Passer {

    [System.Serializable]
    public class UnityController : ControllerSensor {
        public override void Update() {
            if (!enabled)
                return;

            UpdateTargetTransform();
        }

        protected virtual void UpdateTargetTransform() {
#if UNITY_2017_2_OR_NEWER
            if (!XRSettings.enabled)
                return;

            XRNode vrNode = controllerTarget.isLeft ? XRNode.LeftHand : XRNode.RightHand;
#else
            if (!VRSettings.enabled)
                return;

            VRNode vrNode = controllerTarget.isLeft ? VRNode.LeftHand : VRNode.RightHand;
#endif

            Quaternion localControllerRotation = InputTracking.GetLocalRotation(vrNode);
            controllerTarget.transform.rotation = controllerTarget.pawn.unityTracker.trackerTransform.rotation * localControllerRotation;

            Vector3 localControllerPosition = InputTracking.GetLocalPosition(vrNode);
            controllerTarget.transform.position = controllerTarget.pawn.unityTracker.trackerTransform.TransformPoint(localControllerPosition);
        }
    }
}
