﻿using UnityEngine;

namespace Passer {

    public class UnityTrackerComponent : TrackerComponent {

        public UnityCameraComponent unityCamera;
        public UnityControllerComponent leftController;
        public UnityControllerComponent rightController;

        /// <summary>Create a new Unity Tracker</summary>
        public static UnityTrackerComponent Get(Transform realWorld) {
            Transform trackerTransform = realWorld.Find("Unity");
            if (trackerTransform == null) {
                GameObject trackerObject = new GameObject("Unity");
                trackerTransform = trackerObject.transform;

                trackerTransform.parent = realWorld;
                trackerTransform.localPosition = Vector3.zero;
                trackerTransform.localRotation = Quaternion.identity;
            }

            UnityTrackerComponent tracker = trackerTransform.GetComponent<UnityTrackerComponent>();
            if (tracker == null) {
                tracker = trackerTransform.gameObject.AddComponent<UnityTrackerComponent>();
                tracker.realWorld = realWorld;
            }

            return tracker;
        }
    }

}