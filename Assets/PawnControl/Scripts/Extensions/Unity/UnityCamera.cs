﻿using UnityEngine;
#if UNITY_2017_2_OR_NEWER
using UnityEngine.XR;
#endif

namespace Passer {
    /*
    [System.Serializable]
    public class UnityCamera : CameraSensor {        
        public override string name {
            get { return "First Person Camera"; }
        }

        new public UnityTracker tracker;

        //public Transform sensorTransform;
        protected Camera camera;

        #region Start
        public override void Start(Transform targetTransform) {
            base.Start(targetTransform);
            if (cameraTarget.pawn != null)
                tracker = cameraTarget.pawn.unity;

            //UnityVRDevice.Start();

            camera = cameraTarget.GetComponentInChildren<Camera>();
            if (enabled) {
                camera = CheckCamera(cameraTarget);

                if (camera == null && sensorTransform != null) {
                    sensorTransform.gameObject.SetActive(true);
                    camera = cameraTarget.GetComponentInChildren<Camera>();
                }
                else if (camera != null) {
                    sensorTransform = camera.transform;
                }

                CheckCameraLocation();
            }
            else if (camera != null) {
                camera.gameObject.SetActive(false);
            }
            InitFader();
        }

        #region Camera
        public static Camera GetCamera(CameraTarget cameraTarget) {
            if (cameraTarget.unity.camera != null)
                return cameraTarget.unity.camera;

            Camera camera = cameraTarget.GetComponentInChildren<Camera>();
            if (cameraTarget.unity.enabled) {
                camera = CheckCamera(cameraTarget);

                if (camera == null)
                    camera = cameraTarget.GetComponentInChildren<Camera>();

            }
            return camera;
        }

        public static Camera CheckCamera(CameraTarget cameraTarget) {
            if (cameraTarget.unity.enabled) {
                return AddCamera(cameraTarget);
            }
            else {
                RemoveCamera(cameraTarget);
                return null;
            }
        }

        public void CheckCameraLocation() {
            sensorTransform.SetParent(tracker.trackerTransform, true);
            sensorTransform.localPosition = Vector3.zero;
            sensorTransform.localRotation = Quaternion.identity;
        }

        public static Camera AddCamera(CameraTarget hmdTarget) {
            Camera camera = hmdTarget.transform.GetComponentInChildren<Camera>();
            if (camera == null) {
                GameObject cameraObj = new GameObject("First Person Camera");
                camera = cameraObj.AddComponent<Camera>();
                camera.tag = "MainCamera";

                camera.nearClipPlane = 0.1F;

                hmdTarget.unity.sensorTransform = camera.transform;
                camera.transform.SetParent(hmdTarget.transform, false);
                //camera.transform.position = hmdTarget.player.transform.position + Vector3.up * 1.6F;
                camera.transform.rotation = Quaternion.Euler(0, hmdTarget.pawn.transform.eulerAngles.y, 0);

                cameraObj.AddComponent<AudioListener>();
            }

            //if (headTarget.collisionFader)
            //    AddScreenFader(camera);
            //else
            //    RemoveScreenFader(camera.transform);

            return camera;
        }
        public static Camera AddCamera(Transform targetTransform) {
            Camera camera = targetTransform.GetComponentInChildren<Camera>();
            if (camera == null) {
                GameObject cameraObj = new GameObject("First Person Camera");
                camera = cameraObj.AddComponent<Camera>();
                camera.tag = "MainCamera";

                camera.nearClipPlane = 0.1F;

                camera.transform.SetParent(targetTransform, false);
                camera.transform.localPosition = Vector3.zero;
                camera.transform.rotation = Quaternion.Euler(0, targetTransform.eulerAngles.y, 0);

                cameraObj.AddComponent<AudioListener>();
            }
            //AddScreenFader(camera);

            return camera;
        }

        public static void RemoveCamera(CameraTarget hmdTarget) {
            RemoveCamera(hmdTarget.transform);
        }
        public static void RemoveCamera(Transform targetTransform) {
            Camera camera = targetTransform.GetComponentInChildren<Camera>();
            if (camera != null) {
                if (Application.isPlaying)
                    Object.Destroy(camera.gameObject);
                else
                    Object.DestroyImmediate(camera.gameObject);
            }
        }
        #endregion

        #endregion

        #region Update
        bool calibrated = false;

        public override void Update() {
            if (!enabled)
                return;

            UpdateTargetTransform();

            if (!calibrated && cameraTarget.pawn.calibrateAtStart) {
                cameraTarget.pawn.Calibrate();
                calibrated = true;
            }
        }

        protected void UpdateTargetTransform() {
            if (UnityTracker.xrDevice == UnityTracker.XRDeviceType.None)
                UpdateCamera();
            else
                UpdateTransform();
        }

        protected virtual void UpdateCamera() {
            sensorTransform.rotation = cameraTarget.transform.rotation;
            sensorTransform.position = cameraTarget.transform.position;
        }

        protected virtual void UpdateTransform() {
            cameraTarget.transform.rotation = sensorTransform.rotation;
            cameraTarget.transform.position = sensorTransform.position;
        }
        #endregion

        #region Fader
        protected Material fadeMaterial;

        protected void InitFader() {
            Transform planeTransform = camera.transform.Find("Fader");
            if (planeTransform != null)
                return;

            GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            plane.transform.name = "Fader";
            plane.transform.parent = camera.transform;
            plane.transform.localEulerAngles = new Vector3(-90, 0, 0);
            plane.transform.localPosition = new Vector3(0, 0, camera.nearClipPlane + 0.01F);

            Renderer renderer = plane.GetComponent<Renderer>();
            if (renderer != null) {
                Shader fadeShader = Shader.Find("Standard");
                fadeMaterial = new Material(fadeShader);
                fadeMaterial.name = "FadeMaterial";
                fadeMaterial.SetFloat("_Mode", 2);
                fadeMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                fadeMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                fadeMaterial.SetInt("_ZWrite", 0);
                fadeMaterial.DisableKeyword("_ALPHATEST_ON");
                fadeMaterial.EnableKeyword("_ALPHABLEND_ON");
                fadeMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                fadeMaterial.renderQueue = 3000;
                Color color = Color.black;
                color.a = 0.0F;
                fadeMaterial.SetColor("_Color", new Color(0, 0, 0, 0));
                renderer.material = fadeMaterial;
                renderer.enabled = true;
            }

            Collider c = plane.GetComponent<Collider>();
            Object.DestroyImmediate(c);
        }

        public void Fader(float f) {
            //float elapsedTime = 0.0f;
            //Color color = Color.black;
            //color.a = 0.0f;
            //fadeMaterial.color = color;
            //while (elapsedTime < fadeTime) {
            //    yield return new WaitForEndOfFrame();
            //    elapsedTime += Time.deltaTime;
            Color color = Color.black;
            color.a = Mathf.Clamp01(f);
            fadeMaterial.color = color;
            //}

        }
        #endregion
    }
    */
}