﻿using UnityEngine;

namespace Passer {

    public class SensorComponent : MonoBehaviour {
        protected Transform trackerTransform;

        public Tracker.Status status;

        public float rotationConfidence;
        public float positionConfidence;

        public bool autoUpdate = true;

        virtual protected void Awake() {
            if (trackerTransform == null)
                trackerTransform = transform.parent;
        }

        virtual protected void Start() {
            //if (autoUpdate)
            //    StartComponent(trackerTransform);
        }

        public virtual void StartComponent(Transform trackerTransform) {
            // When this function has been called, the sensor will no longer update from Unity Updates.
            // Instead, UpdateComponent needs to be called to update the sensor data
            autoUpdate = false;
            this.trackerTransform = trackerTransform;
        }

        private void Update() {
            if (autoUpdate)
                UpdateComponent();
        }

        public virtual void UpdateComponent() {
            status = Tracker.Status.Unavailable;
            positionConfidence = 0;
            rotationConfidence = 0;
            //gameObject.SetActive(showRealObjects);
        }
    }

}
