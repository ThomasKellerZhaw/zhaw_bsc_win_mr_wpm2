﻿using System.Collections;
using UnityEngine;

namespace Passer {

    /// <summary>Sockets can hold a Handles.</summary>
    /// When a handle is within the range of the socket, it will be attached to the socket.
    /// The position and rotation of the Transform with the Handle will be changed
    /// such that the handle fits the position and rotation of the socket.
    public class Socket : MonoBehaviour {
        public bool isOccupied {
            get {
                return this.attachedTransform != null;
            }
        }
        /// <summary>The Transform attached to this socket</summary>
        /// If the socket holds a Handle, this will contain the Transform of the Handle.
        /// It will be null otherwise
        public Transform attachedTransform;
        protected Transform releasingTransform;

        public Handle attachedHandle;

        /// <summary>The parent of the attached transform before it was attached</summary>
        protected Transform attachedTransformParent;

        /// <summary>A tag for limiting which handles can be held by the socket</summary>
        /// If set (not null or empty) only Handles with the given tag will fit in the socket.
        public string socketTag;

        public virtual Vector3 worldPosition {
            get {
                return transform.position;
            }
        }

        protected virtual void MoveHandleToSocket(Transform socketTransform, Rigidbody handleRigidbody, Handle handle) {
            //Debug.Log("MoveHandleToSocket");
            Transform handleTransform = handle.GetComponent<Transform>();
            //Rigidbody handleRigidbody = handle.GetComponentInParent<Rigidbody>();
            if (handleRigidbody != null)
                handleTransform = handleRigidbody.transform;

            handleTransform.rotation = handle.RotationTo(socketTransform.rotation) * handleTransform.rotation;
            handleTransform.position += handle.TranslationTo(socketTransform.position);
        }

        protected virtual void MoveHandleToSocket(Transform socketTransform, Handle handle) {
            //Debug.Log("MoveHandleToSocket");
            Transform handleTransform = handle.GetComponent<Transform>();
            Rigidbody handleRigidbody = handle.GetComponentInParent<Rigidbody>();
            if (handleRigidbody != null) {
                handleTransform = handleRigidbody.transform;

                KinematicLimitations handleLimitations = handle.GetComponent<KinematicLimitations>();
                if (handleRigidbody.isKinematic && handleLimitations != null && handleLimitations.enabled) {
                    //handleTransform.position += handle.TranslationTo(socketTransform.position);
                    // No rotations for kinematic Limitations
                    return;
                }
            }

            handleTransform.rotation = handle.RotationTo(socketTransform.rotation) * handleTransform.rotation;
            handleTransform.position += handle.TranslationTo(socketTransform.position);
        }

        protected virtual void MoveSocketToHandle(Transform socketTransform, Handle handle) {
            socketTransform.rotation *= Quaternion.Inverse(handle.RotationTo(socketTransform.rotation));
            socketTransform.position -= handle.TranslationTo(socketTransform.position);
        }

        #region Attach

        /// <summary>Tries to attach the given Transform to this socket</summary>
        /// If the Transform has a Handle with the right Socket Tag it will be attached to the socket.
        /// Static and Kinematic Rigidbodies will be attached by parenting.
        /// Non-Kinematic Rigidbodies will be attached using a joint.
        /// <param name="transformToAttach">The Transform to attach to this socket</param>
        /// <returns>Boolean indicating whether attachment succeeded</returns>
        public virtual bool Attach(Transform transformToAttach, bool rangeCheck = true) {
            Handle handle = transformToAttach.GetComponentInChildren<Handle>();
            if (handle == null) {
                // Transform does not have a handle
                //Debug.Log(gameObject.name + ": Attach failed. Transform " + transformToAttach + " does not have a handle"); ;
                return false;
            }

            if (handle.socket != null) {
                // Handle is already in a socket
                return false;
            }

            if (socketTag != null && socketTag != "" && !transformToAttach.gameObject.CompareTag(socketTag)) {
                // Object did not have the right tag
                Debug.Log(gameObject.name + ": Attach failed. Object " + transformToAttach + " does not have the right tag"); ;
                return false;
            }

            //Debug.Log(gameObject.name + ": Attach " + transformToAttach);

            Rigidbody rigidbodyToAttach = transformToAttach.GetComponentInParent<Rigidbody>();
            if (rigidbodyToAttach == null)
                return AttachStaticObject(transformToAttach, handle);
            else {
                return AttachRigidbody(rigidbodyToAttach, handle, rangeCheck);
            }
        }

        /// <summary>Tries to attach the given Transform to this socket</summary>
        /// If the Handle has the right Socket Tag it will be attached to the socket.
        /// Static and Kinematic Rigidbodies will be attached by parenting.
        /// Non-Kinematic Rigidbodies will be attached using a joint.
        /// <param name="handle">The Handle to attach to this socket</param>
        /// <returns>Boolean indicating whether attachment succeeded</returns>
        public virtual bool Attach(Handle handle, bool rangeCheck = true) {
            if (handle == null) {
                // Transform does not have a handle
                //Debug.Log(gameObject.name + ": Attach failed. Transform " + transformToAttach + " does not have a handle"); ;
                return false;
            }

            if (handle.socket != null) {
                // Handle is already in a socket
                return false;
            }

            if (socketTag != null && socketTag != "" && !handle.gameObject.CompareTag(socketTag)) {
                // Object did not have the right tag
                Debug.Log(gameObject.name + ": Attach failed. Handle " + handle + " does not have the right tag"); ;
                return false;
            }

            //Debug.Log(gameObject.name + ": Attach " + transformToAttach);

            Rigidbody rigidbodyToAttach = handle.GetComponentInParent<Rigidbody>();
            if (rigidbodyToAttach == null)
                return AttachStaticObject(handle.transform, handle);
            else {
                return AttachRigidbody(rigidbodyToAttach, handle, rangeCheck);
            }
        }

        #region Rigidbody

        protected virtual bool AttachRigidbody(Rigidbody objRigidbody, Handle handle, bool rangeCheck = true) {
            //Debug.Log("AttachRigidbody");

            float grabDistance = Vector3.Distance(this.transform.position, handle.worldPosition);
            if (rangeCheck && handle.range > 0 && grabDistance > handle.range) {
                //Debug.Log("Socket is outside range of handle");
                return false;
            }

            Transform objTransform = objRigidbody.transform;

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            Joint joint = objRigidbody.GetComponent<Joint>();
            // See if these joints are being destroyed
            DestroyedJoints destroyedJoints = objRigidbody.GetComponent<DestroyedJoints>();

            if (objRigidbody.isKinematic) {
                if (thisRigidbody == null)
                    AttachRigidbodyParenting(objRigidbody, handle);
                else if (thisRigidbody.isKinematic)
                    AttachTransformParenting(objRigidbody.transform, handle);
                else
                    AttachSocketParenting(objRigidbody, handle, thisRigidbody);
            }
            else if (thisRigidbody == null) {
                AttachRigidbodyReverseJoint(objRigidbody, handle);
            }
            else if (
                (joint != null && destroyedJoints == null) ||
                objRigidbody.constraints != RigidbodyConstraints.None
                ) {
                //|| otherHandPhysics != null) {

                AttachRigidbodyJoint(objRigidbody, handle);
            }
            else {
                AttachRigidbodyParenting(objRigidbody, handle);
            }

            releasingTransform = null;
            attachedTransform = objTransform;
            handle.socket = this;
            return true;
        }

        protected void AttachTransformParenting(Transform objTransform, Handle handle) {
            //Debug.Log("AttachTransformParenting: " + objTransform);
            attachedTransformParent = objTransform.parent;

            Rigidbody handleRigidbody = handle.GetComponentInParent<Rigidbody>();
            KinematicLimitations handleLimitations = handleRigidbody.GetComponent<KinematicLimitations>();
            if (handleRigidbody.isKinematic && handleLimitations != null && handleLimitations.enabled) {
                MoveSocketToHandle(this.transform, handle);
            }
            else
                MoveHandleToSocket(this.transform, handle);

            objTransform.parent = this.transform;

            attachedTransform = objTransform;
            attachedHandle = handle;
            handle.socket = this;
        }

        protected void AttachRigidbodyParenting(Rigidbody objRigidbody, Handle handle) {
            //Debug.Log("AttachRigidbodyParenting");

            MoveHandleToSocket(this.transform, objRigidbody, handle);

            attachedTransform = objRigidbody.transform;

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            if (thisRigidbody != null)
                MassRedistribution(thisRigidbody, objRigidbody);

            RigidbodyDisabled.ParentRigidbody(this.transform, objRigidbody);

#if pHUMANOID
            Humanoid.HumanoidNetworking.DisableNetworkSync(attachedTransform.gameObject);
#endif

            attachedHandle = handle;
            handle.socket = this;
        }

        protected virtual void AttachRigidbodyJoint(Rigidbody objRigidbody, Handle handle) {
            //Debug.Log("AttachRigidbodyJoint " + objRigidbody);

            //MassRedistribution(thisRididbody, objRigidbody);

            MoveHandleToSocket(this.transform, handle);

            ConfigurableJoint joint = this.gameObject.AddComponent<ConfigurableJoint>();
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;

            joint.angularXMotion = ConfigurableJointMotion.Locked;
            joint.angularYMotion = ConfigurableJointMotion.Locked;
            joint.angularZMotion = ConfigurableJointMotion.Locked;

            joint.projectionMode = JointProjectionMode.PositionAndRotation;
            joint.projectionDistance = 0.01F;
            joint.projectionAngle = 1;

            Collider c = objRigidbody.transform.GetComponentInChildren<Collider>();
            joint.connectedBody = c.attachedRigidbody;

            attachedTransform = objRigidbody.transform;
            attachedHandle = handle;
            handle.socket = this;
        }

        /// <summary>Attach handle to socket using a static joint on the handle</summary>
        protected virtual void AttachRigidbodyReverseJoint(Rigidbody objRigidbody, Handle handle) {
            //Debug.Log("AttachRigidbodyReverseJoint " + objRigidbody);
            MoveHandleToSocket(this.transform, handle);

            ConfigurableJoint joint = objRigidbody.gameObject.AddComponent<ConfigurableJoint>();
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;

            joint.angularXMotion = ConfigurableJointMotion.Locked;
            joint.angularYMotion = ConfigurableJointMotion.Locked;
            joint.angularZMotion = ConfigurableJointMotion.Locked;

            joint.projectionMode = JointProjectionMode.PositionAndRotation;
            joint.projectionDistance = 0.01F;
            joint.projectionAngle = 1;

            joint.breakForce = 100;
            joint.breakTorque = 100;

            attachedTransform = objRigidbody.transform;
            attachedHandle = handle;
            handle.socket = this;
        }

        protected virtual void AttachSocketParenting(Rigidbody objRigidbody, Handle handle, Rigidbody socketRigidbody) {
            //Debug.Log("AttachSocketParenting");

            RigidbodyDisabled.ParentRigidbody(objRigidbody, socketRigidbody);

            MoveSocketToHandle(this.transform, handle);

            attachedTransform = objRigidbody.transform;
            attachedHandle = handle;
            handle.socket = this;
        }

        protected float originalMass = 1;
        protected bool originalUseGravity = false;
        protected virtual void MassRedistribution(Rigidbody socketRigidbody, Rigidbody objRigidbody) {
            originalMass = socketRigidbody.mass;
            socketRigidbody.mass = HybridPhysics.CalculateTotalMass(objRigidbody);

            originalUseGravity = socketRigidbody.useGravity;
            // This gives an issue when attaching things to a socket which is already in the hand
            // It will enable gravity on the hand in that case (not on the object in the hand)
            // Which gives less stable hand tracking.
            // Update: added additional check such that gravity is only enabled when
            // holding heavy objects
            if (objRigidbody.useGravity && objRigidbody.mass > HybridPhysics.kinematicMass)
                socketRigidbody.useGravity = true;
        }

        #endregion Rigidbody

        #region Static Object

        virtual protected bool AttachStaticObject(Transform objTransform, Handle handle) {
            Debug.Log("AttachStaticObject");

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            if (thisRigidbody == null) {
                Debug.LogWarning("Cannot attach static handle to static socket");
                return false;
            }

            // To do: rangecheck

            if (thisRigidbody.isKinematic) {
                AttachSocketParenting(objTransform, handle, thisRigidbody);
            }
            else {
                MoveSocketToHandle(this.transform, handle);
                AttachStaticJoint(objTransform);
            }

            releasingTransform = null;
            attachedTransform = objTransform;
            attachedHandle = handle;
            handle.socket = this;
            return true;
        }

        virtual protected void AttachSocketParenting(Transform objTransform, Handle handle, Rigidbody thisRigidbody) {
            //Debug.Log("AttachSocketParenting");

            RigidbodyDisabled.ParentRigidbody(objTransform, thisRigidbody);

            MoveSocketToHandle(this.transform, handle);
        }

        virtual protected void AttachStaticJoint(Transform objTransform) {
            //Debug.Log("AttachStaticJoint");
            FixedJoint joint = this.transform.gameObject.AddComponent<FixedJoint>();

            Collider c = objTransform.GetComponentInChildren<Collider>();
            if (c == null)
                c = objTransform.GetComponentInParent<Collider>();
            joint.connectedBody = c.attachedRigidbody;
        }

        #endregion Static Object

        #endregion Attach

        #region Release

        /// <summary>Releases a Transform from the socket</summary>
        /// Note that if the Transform is not taken out of the range of the socket
        /// or held by another Socket, it will automatically snap back to the Socket.
        public void Release() {
            //Debug.Log("Release " + attachedTransform);
            if (attachedTransform == null) {
                attachedHandle = null;
                return;
            }

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            Rigidbody objRigidbody = attachedTransform.GetComponentInParent<Rigidbody>();
            RigidbodyDisabled objRigidbodyDisabled = attachedTransform.GetComponent<RigidbodyDisabled>();
            RigidbodyDisabled thisRigidbodyDisabled = this.GetComponentInParent<RigidbodyDisabled>();

            if (objRigidbodyDisabled != null)
                ReleaseRigidbodyParenting(objRigidbody);
            else if (objRigidbody == null)
                ReleaseStaticObject();
            else if (thisRigidbodyDisabled != null)
                ReleaseSocketParenting(objRigidbody, thisRigidbodyDisabled.transform);
            else if (thisRigidbody != null && thisRigidbody.isKinematic)
                ReleaseTransformParenting();
            else
                ReleaseRigidbodyJoint();

            Handle handle = attachedHandle; // attachedTransform.GetComponentInChildren<Handle>();
            if (handle != null) {
                handle.socket = null;
            }

            this.releasingTransform = this.attachedTransform;
            this.attachedTransform = null;
            this.attachedHandle = null;

            StartCoroutine(ClearReleasingTransform());
        }

        // Released objects should be gone within 1 second or they will be reattached
        private IEnumerator ClearReleasingTransform() {
            yield return new WaitForSeconds(1);
            this.releasingTransform = null;
        }

        #region Rigidbody

        protected virtual void ReleaseRigidbodyParenting(Rigidbody objRigidbody) {
            //Debug.Log("Release Rigidbody from Parenting");

            Rigidbody thisRigidbody = this.GetComponentInParent<Rigidbody>();
            if (thisRigidbody == null)
                objRigidbody = RigidbodyDisabled.UnparentRigidbody(this.transform, attachedTransform);
            else
                objRigidbody = RigidbodyDisabled.UnparentRigidbody(thisRigidbody, attachedTransform);

            MassRestoration(thisRigidbody, objRigidbody);

#if pHUMANOID
            Humanoid.HumanoidNetworking.ReenableNetworkSync(objRigidbody.gameObject);
#endif

            if (thisRigidbody != null) {
                objRigidbody.velocity = thisRigidbody.velocity;
                objRigidbody.angularVelocity = thisRigidbody.angularVelocity;
            }

#if pHUMANOID
            // check if the object is released from an hybrid physics rigidbody (just hands for now)
            Humanoid.AdvancedHandPhysics handPhysics = this.transform.GetComponentInParent<Humanoid.AdvancedHandPhysics>();
            if (handPhysics != null) {
                Collider[] objColliders = objRigidbody.GetComponentsInChildren<Collider>();
                foreach (Collider objCollider in objColliders)
                    Target.UnsetColliderToTrigger(handPhysics.handTarget.colliders, objCollider);
            }
#endif
        }

        protected virtual void ReleaseRigidbodyJoint() {
            Debug.Log("Release from Joint");

            Joint[] joints = this.gameObject.GetComponents<Joint>();
            foreach (Joint joint in joints) {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    DestroyImmediate(joint, true);
                else
#endif
                    Destroy(joint);
            }
            //MassRestoration(..., ...);

            // Trick: when released and immediately attached to anther socket (e.g. grabbing)
            // the joints are not yet destroyed, because Destroy is executed with a delay.
            // Adding the DestroyedJoints component indicates that the joints which may
            // still be there are to be destroyed.
            attachedTransform.gameObject.AddComponent<DestroyedJoints>();
        }

        protected void ReleaseRigidbodyReverseJoint() {
            Debug.Log("Release from Joint");

            Joint[] joints = attachedTransform.GetComponents<Joint>();
            foreach (Joint joint in joints) {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    DestroyImmediate(joint, true);
                else
#endif
                    Destroy(joint);
            }
            //MassRestoration(..., ...);

            // Trick: when released and immediately attached to anther socket (e.g. grabbing)
            // the joints are not yet destroyed, because Destroy is executed with a delay.
            // Adding the DestroyedJoints component indicates that the joints which may
            // still be there are to be destroyed.
            attachedTransform.gameObject.AddComponent<DestroyedJoints>();
        }
        protected virtual void ReleaseSocketParenting(Rigidbody objRigidbody, Transform socketTransform) {
            //Debug.Log("ReleaseSocketParenting");
            RigidbodyDisabled.UnparentRigidbody(objRigidbody, socketTransform);
        }

        protected virtual void MassRestoration(Rigidbody socketRigidbody, Rigidbody objRigidbody) {
            if (socketRigidbody != null) {
                socketRigidbody.mass = originalMass;
                socketRigidbody.useGravity = originalUseGravity;
            }
        }

        #endregion Rigidbody

        #region Static Object

        protected virtual void ReleaseStaticObject() {
            //Debug.Log("ReleaseStaticObject");

            Rigidbody thisRigidbody = this.GetComponent<Rigidbody>();
            RigidbodyDisabled thisDisabledRigidbody = this.GetComponent<RigidbodyDisabled>();

            if (thisRigidbody != null)
                ReleaseStaticJoint();
            else if (thisDisabledRigidbody != null)
                ReleaseSocketParenting(attachedTransform);
            else
                ReleaseTransformParenting();
        }

        protected virtual void ReleaseStaticJoint() {
            //Debug.Log("ReleaseStaticJoint");
            Joint[] joints = this.GetComponents<Joint>();
            foreach (Joint joint in joints) {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    DestroyImmediate(joint, true);
                else
#endif
                    Destroy(joint);
            }
        }

        protected void ReleaseTransformParenting() {
            //Debug.Log("Release Transform from Parenting");
            attachedTransform.parent = attachedTransformParent;
        }

        protected void ReleaseSocketParenting(Transform objTransform) {
            //Debug.Log("ReleaseSocketParenting");
            RigidbodyDisabled.UnparentRigidbody(objTransform, this.transform);
        }

        #endregion

        #endregion Release

        #region Update
        protected virtual void Update() {
            UpdateHolding();
        }
        #endregion

        #region Events

        protected void OnCollisionEnter(Collision collision) {
            //Debug.Log("CollisionEnter");
            if (this.attachedTransform != null)
                // Socket is already occupied
                return;

            Transform enteredTransform = collision.transform;

            Rigidbody enteredRigidbody = collision.rigidbody;
            if (enteredRigidbody == null)
                //    return;

                if (enteredRigidbody != null)
                    enteredTransform = enteredRigidbody.transform;
            //if (enteredTransform.parent != null)
            //    // Transform is not free
            //    return;

            Attach(enteredTransform);
        }

        protected void OnTriggerEnter(Collider other) {
            //Debug.Log("Trigger Enter");

            if (this.attachedTransform != null)
                // Socket is already occupied
                return;

            if (other.attachedRigidbody != null &&
                other.attachedRigidbody.transform == this.releasingTransform) {

                // We are releasing the transform, do not Attach it now
                return;
            }

            Transform enteredTransform = other.transform;

            Rigidbody enteredRigidbody = other.attachedRigidbody;
            if (enteredRigidbody == null)
                return;

            enteredTransform = enteredRigidbody.transform;

            Attach(enteredTransform);
        }

        protected void OnTriggerStay(Collider other) {
            if (this.attachedTransform != null)
                // Socket is already occupied
                return;

            if (other.attachedRigidbody != null &&
                other.attachedRigidbody.transform == this.releasingTransform) {

                // We are releasing the transform, do not Attach it now
                return;
            }

            Rigidbody enteredRigidbody = other.attachedRigidbody;
            if (enteredRigidbody == null)
                return;

            Transform enteredTransform = enteredRigidbody.transform;

            Attach(enteredTransform);
        }

        protected void OnTriggerExit(Collider other) {
            //Debug.Log("Trigger Exit");
            if (this.attachedTransform == null)
                // Socket is not occupied
                return;

            if (other.attachedRigidbody != null &&
                other.attachedRigidbody.transform == this.releasingTransform) {

                this.releasingTransform = null;
                return;
            }

            Rigidbody exitedRigidbody = other.attachedRigidbody;
            if (exitedRigidbody == null)
                return;

            Transform exitedTransform = exitedRigidbody.transform;

            if (attachedTransform != exitedTransform)
                // it was not the handle in the socket which exited
                return;

            Release();
        }

        protected void OnCollisionExit(Collision collision) {
            //Debug.Log("OnCollisionExit");
        }

        protected static string[] attachEventTypeLabels = {
                "Never",
                "On Attach",
                "On Release",
                "While Attached",
                "While Released",
                "When Changed",
                "Always"
        };

        /// <summary>A GameObject Event for triggering changes in the Transform held by the Socket</summary>
        public GameObjectEventList attachEvent = new GameObjectEventList() {
            label = "Hold Event",
            tooltip =
                "Call functions using what the socket is holding\n" +
                "Parameter: the GameObject held by the socket",
            eventTypeLabels = attachEventTypeLabels
        };

        protected void UpdateHolding() {
            if (attachedTransform != null)
                attachEvent.value = attachedTransform.gameObject;
            else
                attachEvent.value = null;
        }

        #endregion

        #region Gizmos
        protected Mesh gizmoMesh;

        public void OnDrawGizmosSelected() {
            if (gizmoMesh == null)
                gizmoMesh = GenerateGizmoMesh();
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireMesh(gizmoMesh, transform.position, transform.rotation);
        }

        public static Mesh GenerateGizmoMesh1() {
            Vector3 p0 = new Vector3(-0.01F, -0.03F, -0.01F);
            Vector3 p1 = new Vector3(-0.01F, 0.03F, -0.01F);
            Vector3 p2 = new Vector3(0.01F, 0.03F, -0.01F);
            Vector3 p3 = new Vector3(0.01F, -0.01F, -0.01F);
            Vector3 p4 = new Vector3(0.03F, -0.01F, -0.01F);
            Vector3 p5 = new Vector3(0.03F, -0.03F, -0.01F);

            Vector3 p6 = new Vector3(-0.01F, -0.03F, 0.01F);
            Vector3 p7 = new Vector3(-0.01F, 0.03F, 0.01F);
            Vector3 p8 = new Vector3(0.01F, 0.03F, 0.01F);
            Vector3 p9 = new Vector3(0.01F, -0.01F, 0.01F);
            Vector3 p10 = new Vector3(0.03F, -0.01F, 0.01F);
            Vector3 p11 = new Vector3(0.03F, -0.03F, 0.01F);

            Vector3[] vertices = {
                p0, p1, p6, p7,
                p1, p2, p7, p8,
                p2, p3, p8, p9,
                p3, p4, p9, p10,
                p4, p5, p10, p11,
                p5, p0, p11, p6,

                p0, p1, p2, p3, p4, p5,
                p6, p7, p8, p9, p10, p11,
            };
            int[] triangles = {
                0, 1, 2,     3, 2, 1,
                4, 5, 6,     7, 6, 5,
                8, 9, 10,    11, 10, 9,
                12, 13, 14,  15, 14, 13,
                16, 17, 18,  19, 18, 17,
                20, 21, 22,  23, 22, 21,
                27,25,24,   27,26,25,   29,28,27,   27,24,29,
                30,31,33,   31,32,33,   33,34,35,   35,30,33,
            };
            Vector3[] normals = {
                Vector3.left, Vector3.left, Vector3.left, Vector3.left,

                Vector3.up, Vector3.up, Vector3.up, Vector3.up,
                Vector3.right, Vector3.right, Vector3.right, Vector3.right,
                Vector3.up, Vector3.up, Vector3.up, Vector3.up,
                Vector3.right, Vector3.right, Vector3.right, Vector3.right,
                Vector3.down, Vector3.down, Vector3.down, Vector3.down,
                Vector3.back, Vector3.back, Vector3.back, Vector3.back, Vector3.back, Vector3.back,
                Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward
            };
            Mesh gizmoMesh = new Mesh() {
                vertices = vertices,
                triangles = triangles,
                normals = normals
            };
            return gizmoMesh;
        }

        public static Mesh GenerateGizmoMesh() {
            Vector3 p0 = new Vector3(-0.01F, 0.03F, -0.01F);
            Vector3 p1 = new Vector3(-0.01F, -0.03F, -0.01F);
            Vector3 p2 = new Vector3(-0.01F, -0.03F, 0.01F);
            Vector3 p3 = new Vector3(-0.01F, 0.01F, 0.01F);
            Vector3 p4 = new Vector3(-0.01F, 0.01F, 0.03F);
            Vector3 p5 = new Vector3(-0.01F, 0.03F, 0.03F);

            Vector3 p6 = new Vector3(0.01F, 0.03F, -0.01F);
            Vector3 p7 = new Vector3(0.01F, -0.03F, -0.01F);
            Vector3 p8 = new Vector3(0.01F, -0.03F, 0.01F);
            Vector3 p9 = new Vector3(0.01F, 0.01F, 0.01F);
            Vector3 p10 = new Vector3(0.01F, 0.01F, 0.03F);
            Vector3 p11 = new Vector3(0.01F, 0.03F, 0.03F);

            Vector3[] vertices = {
                p0, p1, p6, p7,     // 0, 1, 2, 3
                p1, p2, p7, p8,     // 4, 5, 6, 7
                p2, p3, p8, p9,     // 8, 9, 10, 11
                p3, p4, p9, p10,    // 12, 13, 14, 15
                p4, p5, p10, p11,   // 16, 17, 18, 19
                p5, p0, p11, p6,    // 20, 21, 22, 23

                p0, p1, p2, p3, p4, p5,     // 24, 25, 26, 27, 28, 29
                p6, p7, p8, p9, p10, p11,   // 30, 31, 32, 33, 34, 35
            };
            int[] triangles = {
                0, 2, 1,     3, 1, 2,
                4, 6, 5,     7, 5, 6,
                8, 10, 9,    11, 9, 10,
                12, 14, 13,  15, 13, 14,
                16, 18, 17,  19, 17, 18,
                20, 22, 21,  23, 21, 22,
                27,24,25,   27,25,26,   29,27,28,   27,29,24,
                30,33,31,   31,33,32,   33,35,34,   35,33,30,
            };
            Vector3[] normals = {
                Vector3.back, Vector3.back, Vector3.back, Vector3.back, //back
                Vector3.down, Vector3.down, Vector3.down, Vector3.down, //down  
                Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward, //forward
                Vector3.down, Vector3.down, Vector3.down, Vector3.down, //down  
                Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward, //forward
                Vector3.up, Vector3.up, Vector3.up, Vector3.up, //up

                Vector3.left, Vector3.left, Vector3.left, Vector3.left, Vector3.left, Vector3.left,
                Vector3.right, Vector3.right, Vector3.right, Vector3.right, Vector3.right, Vector3.right,
            };
            Mesh gizmoMesh = new Mesh() {
                vertices = vertices,
                triangles = triangles,
                normals = normals
            };
            return gizmoMesh;
        }
        #endregion
    }

    /// <summary>Component to indicate that the joints are being destroyed</summary>
    // When a rigidbody is released and immediately attached to anther socket (e.g. grabbing)
    // the joints may not yet be destroyed when they are grabbed, because Destroy is executed with a delay.
    // Adding the DestroyedJoints component indicates that the joints which may
    // still be there are to be destroyed.
    public class DestroyedJoints : MonoBehaviour {
        private void Update() {
            Destroy(this);
        }
    }
}