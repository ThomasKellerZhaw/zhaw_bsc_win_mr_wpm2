﻿using UnityEngine;
using UnityEditor;

namespace Passer.Humanoid {

    [CustomEditor(typeof(Socket), true)]
    public class Socket_Editor : Editor {

        protected Socket socket;

        #region Enable
        public void OnEnable() {
            socket = (Socket)target;

            InitEvents();
        }
        #endregion

        #region Inspector
        public override void OnInspectorGUI() {
            serializedObject.Update();

            socket.attachedTransform = (Transform)EditorGUILayout.ObjectField("Attached Transform", socket.attachedTransform, typeof(Transform), true);
            socket.socketTag = EditorGUILayout.TextField("Socket Tag", socket.socketTag);

            EventsInspector();

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Events
        protected SerializedProperty attachEventProp;

        protected void InitEvents() {
            attachEventProp = serializedObject.FindProperty("attachEvent");
            socket.attachEvent.id = 0;
        }

        protected bool showEvents;
        protected int selectedEventSource = -1;
        protected int selectedEvent;

        protected void EventsInspector() {
            showEvents = EditorGUILayout.Foldout(showEvents, "Events", true);
            if (showEvents) {
                EditorGUI.indentLevel++;

                GameObjectEvent_Editor.EventInspector(attachEventProp, socket.attachEvent, ref selectedEventSource, ref selectedEvent);

                EditorGUI.indentLevel--;
            }
        }


        #endregion
    }

}