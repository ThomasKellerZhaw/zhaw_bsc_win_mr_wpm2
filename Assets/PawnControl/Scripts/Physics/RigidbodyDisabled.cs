﻿using UnityEngine;
namespace Passer {

    public class RigidbodyDisabled : MonoBehaviour {
        public float mass = 1;
        public float drag;
        public float angularDrag = 0.05F;
        public bool useGravity = true;
        public bool isKinematic;
        public RigidbodyInterpolation interpolation = RigidbodyInterpolation.None;
        public CollisionDetectionMode collisionDetectionMode = CollisionDetectionMode.Discrete;
        public RigidbodyConstraints constraints = RigidbodyConstraints.None;
        public Transform parent;

        public void CopyFromRigidbody(Rigidbody rb) {
            mass = rb.mass;
            drag = rb.drag;
            angularDrag = rb.angularDrag;
            useGravity = rb.useGravity;
            isKinematic = rb.isKinematic;
            interpolation = rb.interpolation;
            collisionDetectionMode = rb.collisionDetectionMode;
            constraints = rb.constraints;

            parent = rb.transform.parent;
        }

        public void CopyToRigidbody(Rigidbody rb) {
            rb.mass = mass;
            rb.drag = drag;
            rb.angularDrag = angularDrag;
            rb.useGravity = useGravity;
            rb.isKinematic = isKinematic;
            rb.interpolation = interpolation;
            rb.collisionDetectionMode = collisionDetectionMode;
            rb.constraints = constraints;

            rb.transform.parent = parent;
        }

        public static RigidbodyDisabled DisableRigidbody(Rigidbody rigidbody) {
            RigidbodyDisabled disabledRigidbody = rigidbody.gameObject.AddComponent<RigidbodyDisabled>();
            disabledRigidbody.CopyFromRigidbody(rigidbody);

            if (Application.isPlaying)
                Destroy(rigidbody);
            else
                DestroyImmediate(rigidbody, true);

            return disabledRigidbody;
        }

        public static Rigidbody EnableRigidbody(Transform rigidbodyTransform) {
            Rigidbody rigidbody = rigidbodyTransform.GetComponent<Rigidbody>();

            RigidbodyDisabled disabledRigidbody = rigidbodyTransform.GetComponent<RigidbodyDisabled>();
            if (disabledRigidbody != null) {
                if (!rigidbodyTransform.gameObject.isStatic && rigidbody == null) {
                    rigidbody = rigidbodyTransform.gameObject.AddComponent<Rigidbody>();
                    disabledRigidbody.CopyToRigidbody(rigidbody);
                }
            }

            if (Application.isPlaying)
                Destroy(disabledRigidbody);
            else
                DestroyImmediate(disabledRigidbody, true);

            return rigidbody;
        }


        public static void ParentRigidbody(Transform parentTransform, Rigidbody childRigidbody) {
            Transform childTransform = childRigidbody.transform;
            DisableRigidbody(childRigidbody);
            childTransform.parent = parentTransform;
        }

        public static void ParentRigidbody(Rigidbody parentRigidbody, Rigidbody childRigidbody) {
            //parentRigidbody.mass += childRigidbody.mass;

            Transform childTransform = childRigidbody.transform;
            DisableRigidbody(childRigidbody);
            childTransform.parent = parentRigidbody.transform;
        }

        public static Rigidbody UnparentRigidbody(Transform parentTransform, Transform childRigidbodyTransform) {
            Rigidbody childRigidbody = EnableRigidbody(childRigidbodyTransform);

            // To do: copy velocity of parent to child
            return childRigidbody;
        }

        public static Rigidbody UnparentRigidbody(Rigidbody parentRigidbody, Transform childRigidbodyTransform) {
            Rigidbody childRigidbody = EnableRigidbody(childRigidbodyTransform);
            //parentRigidbody.mass -= childRigidbody.mass;

            // To do: copy velocity of parent to child
            return childRigidbody;
        }
    }
}