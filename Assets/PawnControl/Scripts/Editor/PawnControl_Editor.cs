﻿using UnityEditor;
using UnityEngine;

namespace Passer {

    [CustomEditor(typeof(PawnControl), true)]
    public class PawnControl_Editor : Editor {
        protected PawnControl pawn;

        #region Enable

        public virtual void OnEnable() {
            pawn = (PawnControl)target;

            // Maybe switch his to an OnInitialize???
            if (!pawn.isRemote) {
                if (pawn.realWorld == null)
                    pawn.realWorld = PawnControl.GetRealWorld(pawn.transform);
                if (pawn.unityComponent == null) {
                    pawn.unityComponent = UnityTrackerComponent.Get(pawn.realWorld);
                    pawn.trackerComponents.Add(pawn.unityComponent);
                }
            }

            InitPawn();
            if (pawn.leftControllerTarget != null)
                pawn.leftControllerTarget.InitSensors();
            if (pawn.rightControllerTarget != null)
                pawn.rightControllerTarget.InitSensors();

            InitInput();
            InitMovement();
            InitSettings();
        }
        
        #endregion

        #region Disable

        public virtual void OnDisable() {
        }
        
        #endregion

        #region Inspector

        public override void OnInspectorGUI() {
            serializedObject.Update();

            PawnInspector();
            TargetsInspector();
            InputInspector();
            MovementInspector();
            SettingsInspector();
            Buttons();

            serializedObject.ApplyModifiedProperties();
        }

        #region Pawn
        SerializedProperty animatorParamForwardIxProp;
        SerializedProperty animatorParamForwardProp;

        protected virtual void InitPawn() {
            if (pawn.characterController == null)
                pawn.characterController = pawn.GetComponentInChildren<CharacterController>();
            //if (pawn.animatorController == null)
            //    pawn.animatorController = pawn.GetComponentInChildren<Animator>();

            //animatorParameterNames = GetAnimatorParameters(pawn);
            //animatorParamForwardProp = serializedObject.FindProperty("animatorParameterForward");
            //animatorParamForwardIxProp = serializedObject.FindProperty("animatorParameterForwardIndex");
        }

        
        /*
        protected GUIContent[] animatorParameterNames;        
        public GUIContent[] GetAnimatorParameters(PawnControl pawn) {
            if (pawn == null || pawn.animatorController == null)
                return null;

            AnimatorControllerParameter[] animatorParameters = pawn.animatorController.parameters;
            GUIContent[] fullAnimatorParameterNames = new GUIContent[animatorParameters.Length + 1];
            fullAnimatorParameterNames[0] = new GUIContent(" ");
            int j = 1;
            for (int i = 0; i < animatorParameters.Length; i++)
                if (animatorParameters[i].type == AnimatorControllerParameterType.Float)
                    fullAnimatorParameterNames[j++] = new GUIContent(animatorParameters[i].name);

            GUIContent[] truncatedParameterNames = new GUIContent[j];
            for (int i = 0; i < j; i++)
                truncatedParameterNames[i] = fullAnimatorParameterNames[i];
            return truncatedParameterNames;
        }
        */
        protected void PawnInspector() {
            if (pawn.characterController != null) {
                EditorGUILayout.ObjectField("Character Controller", pawn.characterController, typeof(CharacterController), true);
                EditorGUI.EndDisabledGroup();
            }
            //else if (pawn.animatorController != null) {
            //    EditorGUILayout.ObjectField("Animator", pawn.animatorController, typeof(Animator), true);
            //    EditorGUI.EndDisabledGroup();
            //    EditorGUI.indentLevel++;
            //    GUIContent forwardSpeedText = new GUIContent(
            //        "Forward Speed",
            //        "Animator parameter controlling the forward motion animation"
            //        );
            //    animatorParamForwardIxProp.intValue = SetAnimatorInput(forwardSpeedText, animatorParamForwardIxProp.intValue, animatorParamForwardProp);

            //    EditorGUI.indentLevel--;
            //}
        }

        //protected int SetAnimatorInput(GUIContent label, int parameterIndex, SerializedProperty parameterName) {
        //    if (parameterIndex > animatorParameterNames.Length)
        //        parameterIndex = 0;
        //    int newParameterIndex = EditorGUILayout.Popup(label, parameterIndex, animatorParameterNames, GUILayout.MinWidth(80));

        //    if (newParameterIndex > 0 && newParameterIndex < animatorParameterNames.Length) {
        //        parameterName.stringValue = animatorParameterNames[newParameterIndex].text;
        //    }
        //    else {
        //        parameterName.stringValue = null;
        //    }
        //    return newParameterIndex;
        //}
        #endregion

        #region Targets
        protected bool showTargets;

        protected void TargetsInspector() {
            GUIContent text = new GUIContent(
                "Targets",
                "The target transforms controlling the Pawn"
                );
            showTargets = EditorGUILayout.Foldout(showTargets, text, true);
            if (showTargets) {
                EditorGUI.indentLevel++;
                pawn.cameraTarget = CameraTarget_Editor.Inspector("Camera Target", pawn);
                if (PlayerSettings.virtualRealitySupported) {
                    pawn.leftControllerTarget = ControllerTarget_Editor.Inspector("Left Controller Target", pawn, true);
                    pawn.rightControllerTarget = ControllerTarget_Editor.Inspector("Right Controller Target", pawn, false);
                }
                EditorGUI.indentLevel--;
            }
        }
        #endregion

        #region Input
        protected SerializedProperty gameControllerProp;

        protected void InitInput() {
            gameControllerProp = serializedObject.FindProperty("gameController");
        }

        protected void InputInspector() {
            if (!pawn.isRemote) {

                UnityTracker.XRDeviceType device = UnityTracker.DetermineLoadedDevice();
                switch (device) {
                    case UnityTracker.XRDeviceType.Oculus:
                        gameControllerProp.intValue = (int)GameControllers.Oculus;
                        break;
                    case UnityTracker.XRDeviceType.OpenVR:
                        gameControllerProp.intValue = (int)GameControllers.OpenVR;
                        break;
                }

                GUIContent text = new GUIContent(
                    "Game Controller",
                    "The type of Game Controller used."
                    );
                gameControllerProp.intValue = (int)(GameControllers)EditorGUILayout.EnumPopup(text, (GameControllers)gameControllerProp.intValue);
            }

        }
        #endregion

        #region Movement

        protected virtual void InitMovement() {
        }

        protected bool showMovement = false;
        protected virtual void MovementInspector() {
            GUIContent text = new GUIContent(
                "Movement",
                "Settings related to moving the pawn around"
                );
            showMovement = EditorGUILayout.Foldout(showMovement, text, true);
            if (showMovement) {
                EditorGUI.indentLevel++;
                ForwardSpeedInspector();
                BackwardSpeedInspector();
                SidewardSpeedInspector();
                MaxAccelerationInspector();
                RotationSpeedInspector();
                ProximitySpeedInspector();
                EditorGUI.indentLevel--;
            }
        }

        protected virtual void ForwardSpeedInspector() {
            SerializedProperty forwardSpeedProp = serializedObject.FindProperty("forwardSpeed");
            GUIContent text = new GUIContent(
                "Forward Speed",
                "Maximum forward speed in units(meters)/second"
                );
            forwardSpeedProp.floatValue = EditorGUILayout.FloatField(text, forwardSpeedProp.floatValue);
        }

        protected virtual void BackwardSpeedInspector() {
            SerializedProperty backwardSpeedProp = serializedObject.FindProperty("backwardSpeed");
            GUIContent text = new GUIContent(
                "Backward Speed",
                "Maximum backward speed in units(meters)/second"
                );
            backwardSpeedProp.floatValue = EditorGUILayout.FloatField(text, backwardSpeedProp.floatValue);
        }

        protected virtual void SidewardSpeedInspector() {
            SerializedProperty sidewardSpeedPop = serializedObject.FindProperty("sidewardSpeed");
            GUIContent text = new GUIContent(
                "Sideward Speed",
                "Maximum sideward speed in units(meters)/second"
                );
            sidewardSpeedPop.floatValue = EditorGUILayout.FloatField(text, sidewardSpeedPop.floatValue);
        }

        protected virtual void MaxAccelerationInspector() {
            SerializedProperty maxAccelerationProp = serializedObject.FindProperty("maxAcceleration");
            GUIContent text = new GUIContent(
                "Maximum Acceleration",
                "Maximum acceleration in units(meters)/second/second, 0 = no maximum acceleration"
                );
            maxAccelerationProp.floatValue = EditorGUILayout.FloatField(text, maxAccelerationProp.floatValue);
        }

        protected virtual void RotationSpeedInspector() {
            SerializedProperty rotationSpeedProp = serializedObject.FindProperty("rotationSpeed");
            GUIContent text = new GUIContent(
                "Rotation Speed",
                "Maximum rotational speed in degrees/second"
                );
            rotationSpeedProp.floatValue = EditorGUILayout.FloatField(text, rotationSpeedProp.floatValue);
        }

        protected virtual void ProximitySpeedInspector() {
            SerializedProperty proximitySpeedProp = serializedObject.FindProperty("proximitySpeed");
            GUIContent text = new GUIContent(
                "Proximity Speed",
                "Decreases movement speed when the humanoid is close to static objects to reduce motion sickness"
                );
            proximitySpeedProp.boolValue = EditorGUILayout.Toggle(text, proximitySpeedProp.boolValue);
        }



        #endregion

        #region Settings

        protected SerializedProperty showRealObjectsProp;

        protected virtual void InitSettings() {
            showRealObjectsProp = serializedObject.FindProperty("showRealObjects");

            ShowRealWorldObjects();
        }

        protected bool showSettings = false;
        protected virtual void SettingsInspector() {
            GUIContent text = new GUIContent(
                "Settings",
                "To contract various aspects of the script"
                );
            showSettings = EditorGUILayout.Foldout(showSettings, text, true);

            if (showSettings) {
                EditorGUI.indentLevel++;

                if (!pawn.isRemote) {
                    RealWorldObjects(pawn);

                    PhysicsSetting();
                    UseGravitySetting();
                    BodyPullSetting();
                    //HapticsSetting();
                    //ProximitySpeedSetting();
                    CalibrateAtStartSetting();
                    StartPositionSetting();
                    ScalingSetting();
                }
                DontDestroySetting();
                //IsRemoteSetting();
                //if (IsPrefab(pawn))
                //    DisconnectInstancesSetting();

                EditorGUI.indentLevel--;
            }
        }

        protected void RealWorldObjects(PawnControl pawn) {
            SerializedProperty showRealObjectsProp = serializedObject.FindProperty("showRealObjects");
            bool lastShowRealObjects = showRealObjectsProp.boolValue;

            GUIContent text = new GUIContent(
                "Show Real Objects",
                "Shows real physical objects like trackers, controllers and camera's at their actual location"
                );
            showRealObjectsProp.boolValue = EditorGUILayout.Toggle(text, lastShowRealObjects);

            if (!lastShowRealObjects && showRealObjectsProp.boolValue) { // we turned real world objects on
                ShowRealWorldObjects();
            }
        }

        protected virtual void ShowRealWorldObjects() {
            if (pawn.leftControllerTarget != null)
                pawn.leftControllerTarget.ShowSensors(pawn.showRealObjects);
            if (pawn.rightControllerTarget != null)
                pawn.rightControllerTarget.ShowSensors(pawn.showRealObjects);
        }

        protected virtual void UseGravitySetting() {
            SerializedProperty useGravityProp = serializedObject.FindProperty("useGravity");
            GUIContent text = new GUIContent(
                "Use Gravity",
                "Implements downward motion when the character is not on solid ground"
                );
            useGravityProp.boolValue = EditorGUILayout.Toggle(text, useGravityProp.boolValue);
        }

        protected virtual void PhysicsSetting() {
            // Physics cannot bet changes during runtime
            EditorGUI.BeginDisabledGroup(Application.isPlaying);

            SerializedProperty physicsProp = serializedObject.FindProperty("physics");
            GUIContent text = new GUIContent(
                "Physics",
                "Enables collisions of the humanoid with the environment using the physics engine"
                );
            physicsProp.boolValue = EditorGUILayout.Toggle(text, physicsProp.boolValue);

            if (physicsProp.boolValue && PlayerSettings.virtualRealitySupported && Time.fixedDeltaTime > 0.01F) {
                EditorGUILayout.HelpBox("Project Settings->Time->Fixed Timestep is too high.\nPlease set to 0.01 or smaller for stable physics in VR.", MessageType.Warning);
            }

            EditorGUI.EndDisabledGroup();
        }

        protected virtual void BodyPullSetting() {
            SerializedProperty bodyPullProp = serializedObject.FindProperty("bodyPull");
            GUIContent text = new GUIContent(
                "Body Pull",
                "Moves the body when grabbing static objects"
                );
            bodyPullProp.boolValue = EditorGUILayout.Toggle(text, bodyPullProp.boolValue);
        }

        protected void HapticsSetting() {
            SerializedProperty hapticsProp = serializedObject.FindProperty("haptics");
            GUIContent text = new GUIContent(
                "Haptics",
                "Uses haptic feedback when colliding with objects"
                );
            hapticsProp.boolValue = EditorGUILayout.Toggle(text, hapticsProp.boolValue);
        }

        protected virtual void CalibrateAtStartSetting() {
            SerializedProperty calibrateAtStartProp = serializedObject.FindProperty("calibrateAtStart");
            GUIContent text = new GUIContent(
                "Calibrate at Start",
                "Will calibrate the humanoid when the tracking starts."
                );
            calibrateAtStartProp.boolValue = EditorGUILayout.Toggle(text, calibrateAtStartProp.boolValue);
        }

        protected virtual void ScalingSetting() {
            SerializedProperty scalingProp = serializedObject.FindProperty("avatarMatching");
            GUIContent text = new GUIContent(
                "Scaling",
                "Determines how differences between the player size and the character are resolved"
                );
            scalingProp.intValue = (int)(PawnControl.MatchingType)EditorGUILayout.EnumPopup(text, (PawnControl.MatchingType)scalingProp.intValue);
        }

        protected virtual void StartPositionSetting() {
            SerializedProperty startPositionProp = serializedObject.FindProperty("startPosition");
            GUIContent text = new GUIContent(
                "Start Position",
                "Determines the start position of the player"
                );
            startPositionProp.intValue = (int)(PawnControl.StartPosition)EditorGUILayout.EnumPopup(text, (PawnControl.StartPosition)startPositionProp.intValue);
        }

        protected void DontDestroySetting() {
            SerializedProperty dontDestroyProp = serializedObject.FindProperty("dontDestroyOnLoad");
            GUIContent text = new GUIContent(
                "Don't Destroy on Load",
                "Ensures that the Humanoid survives a scene change"
                );
            dontDestroyProp.boolValue = EditorGUILayout.Toggle(text, dontDestroyProp.boolValue);
        }

        protected void DisconnectInstancesSetting() {
            SerializedProperty disconnectInstancesProp = serializedObject.FindProperty("disconnectInstances");
            GUIContent text = new GUIContent(
                "Disconnect Instances",
                "Disconnects instances from prefab when instantiating"
                );
            disconnectInstancesProp.boolValue = EditorGUILayout.Toggle(text, disconnectInstancesProp.boolValue);
        }

        protected virtual void IsRemoteSetting() {
            SerializedProperty isRemoteProp = serializedObject.FindProperty("isRemote");
            GUIContent text = new GUIContent(
                "Is Remote",
                "This pawn is used for remote clients"
                );
            isRemoteProp.boolValue = EditorGUILayout.Toggle(text, isRemoteProp.boolValue);
        }

        #endregion

        #region Buttons
        private void Buttons() {
            GUILayout.BeginHorizontal();
            if (Application.isPlaying && GUILayout.Button("Calibrate"))
                pawn.Calibrate();
            GUILayout.EndHorizontal();
        }
        #endregion

        #endregion

    }
}