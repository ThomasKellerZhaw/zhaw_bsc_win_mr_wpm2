﻿using UnityEngine;

namespace Passer {

    public class ControllerInteraction {

        public static float maxGrabbingMass = 10; // max mass you can grab

        #region Grab

        public static Socket CreateGrabSocket(ControllerTarget controllerTarget) {
            GameObject socketObj = new GameObject("Grab Socket");
            Transform socketTransform = socketObj.transform;
            socketTransform.parent = controllerTarget.transform;
            socketTransform.localPosition = new Vector3(0, -0.04F, -0.04F);
            socketTransform.localRotation = Quaternion.AngleAxis(45, Vector3.right);

            ControllerSocket grabSocket = socketObj.AddComponent<ControllerSocket>();
            grabSocket.controllerTarget = controllerTarget;
            return grabSocket;
        }

        public static void NetworkedGrab(ControllerTarget controllerTarget, GameObject obj, bool rangeCheck = true) {
            Grab(controllerTarget, obj, rangeCheck);
        }

        public static void Grab(ControllerTarget controllerTarget, GameObject obj, bool rangeCheck) {
            Debug.Log(controllerTarget + " grabs " + obj);

            if (AlreadyGrabbedWithOtherController(controllerTarget, obj))
                Grab2(controllerTarget, obj);

            else {
                Rigidbody objRigidbody = obj.GetComponentInParent<Rigidbody>();
//                Handle handle = obj.GetComponentInChildren<Handle>();
                Handle handle = Handle.GetClosestHandle(obj.transform, controllerTarget.transform.position);
                if (handle != null) {
                    GrabHandle(controllerTarget, objRigidbody, handle, rangeCheck);
                }
                else if (objRigidbody != null) {
                    GrabRigidbodyWithoutHandle(controllerTarget, objRigidbody);
                }
            }
            if (controllerTarget.grabbedObject != null) {
                if (controllerTarget.physics) {
                    ControllerPhysics controllerPhysics = controllerTarget.GetComponent<ControllerPhysics>();
                    if (controllerPhysics != null) {
                        controllerPhysics.SetCollidersToTrigger();
                        //controllerPhysics.DeterminePhysicsMode();
                    }
                }
                if (Application.isPlaying) {
                    controllerTarget.SendMessage("OnGrabbing", controllerTarget.grabbedObject, SendMessageOptions.DontRequireReceiver);
                    controllerTarget.grabbedObject.SendMessage("OnGrabbed", controllerTarget, SendMessageOptions.DontRequireReceiver);
                    if (controllerTarget.grabbedHandle != null)
                        controllerTarget.grabbedHandle.SendMessage("OnGrabbed", controllerTarget, SendMessageOptions.DontRequireReceiver);
                }
            }
        }

        private static bool AlreadyGrabbedWithOtherController(ControllerTarget controllerTarget, GameObject obj) {
            if (controllerTarget.otherController == null)
                return false;

            if (obj.transform == controllerTarget.otherController.transform)
                return true;

            Rigidbody objRigidbody = obj.GetComponent<Rigidbody>();
            if (objRigidbody != null && objRigidbody.isKinematic) {
                Transform parent = objRigidbody.transform.parent;
                if (parent == null)
                    return false;

                Rigidbody parentRigidbody = parent.GetComponentInParent<Rigidbody>();
                if (parentRigidbody == null)
                    return false;

                return AlreadyGrabbedWithOtherController(controllerTarget, parentRigidbody.gameObject);
            }

            return false;
        }

        protected static void Grab2(ControllerTarget controllerTarget, GameObject obj) {
            Debug.Log("Grab2 " + obj);

            Handle handle = Handle.GetClosestHandle(obj.transform, controllerTarget.transform.position);// obj.GetComponentInChildren<Handle>();
            if (handle == null)
                return;

            Rigidbody objRigidbody = handle.GetComponentInParent<Rigidbody>();
            if (objRigidbody == null)
                return;

            if (handle != null)
                GrabHandle(controllerTarget, objRigidbody, handle, false);
            else
                GrabRigidbodyParenting(controllerTarget, objRigidbody);

            controllerTarget.otherController.twoHandedGrab = true;
            return;
        }

        private static void GrabHandle(ControllerTarget controllerTarget, Rigidbody objRigidbody, Handle handle, bool rangeCheck) {
            Debug.Log("GrabHandle " + handle);

            GameObject obj = (objRigidbody != null) ? objRigidbody.gameObject : handle.gameObject;

            if (objRigidbody != null && objRigidbody.isKinematic) {
                Debug.Log("Grab Kinematic Rigidbody Handle");
                // When grabbing a kinematic rigidbody, the controller should change
                // to a non-kinematic rigidbody first
                ControllerPhysics controllerPhysics = controllerTarget.GetComponent<ControllerPhysics>();
                if (controllerPhysics != null)
                    controllerPhysics.SetNonKinematic();
            }

            if (handle.socket != null) {
                Debug.Log("Grab from socket");
                handle.socket.Release();
            }
            controllerTarget.grabSocket.Attach(handle.transform, rangeCheck);
            controllerTarget.grabbedHandle = handle;

            controllerTarget.grabbedObject = obj;
        }

        private static void GrabRigidbodyWithoutHandle(ControllerTarget controllerTarget, Rigidbody objRigidbody) {

            if (objRigidbody.mass > maxGrabbingMass) {
                Debug.Log("Object is too heavy, mass > " + maxGrabbingMass);
                return;
            }

            GrabRigidbodyParenting(controllerTarget, objRigidbody);
            controllerTarget.grabbedRigidbody = true;
        }

        private static void GrabRigidbodyParenting(ControllerTarget controllerTarget, Rigidbody objRigidbody) {
            Debug.Log("GrabRigidbodyParenting " + objRigidbody);

            GameObject obj = objRigidbody.gameObject;

            Rigidbody controllerRigidbody = controllerTarget.GetComponent<Rigidbody>();
            RigidbodyDisabled.ParentRigidbody(controllerRigidbody, objRigidbody);

            controllerTarget.grabbedObject = obj;
        }

        #endregion

        #region Let Go

        public static void NetworkedLetGo(ControllerTarget controllerTarget) {
            if (controllerTarget.grabbedObject == null)
                return;

            LetGo(controllerTarget);
        }

        public static void LetGo(ControllerTarget controllerTarget) {
            Debug.Log("LetGo");
            if (controllerTarget.grabSocket.attachedHandle != null)
                controllerTarget.grabSocket.Release();
            else
                LetGoWithoutHandle(controllerTarget);

            if (controllerTarget.grabbedRigidbody)
                LetGoRigidbody(controllerTarget);

            if (controllerTarget.physics) {
                ControllerPhysics controllerPhysics = controllerTarget.GetComponent<ControllerPhysics>();
                if (controllerPhysics != null) {
                    controllerPhysics.UnsetCollidersToTrigger();
                    controllerPhysics.SetHybridKinematic();
                }
            }

            if (Application.isPlaying) {
                controllerTarget.SendMessage("OnLettingGo", null, SendMessageOptions.DontRequireReceiver);
                controllerTarget.grabbedObject.SendMessage("OnLetGo", controllerTarget, SendMessageOptions.DontRequireReceiver);
            }

            controllerTarget.grabbedObject = null;
            controllerTarget.grabbedHandle = null;
            controllerTarget.twoHandedGrab = false;
            if (controllerTarget.otherController != null)
                controllerTarget.otherController.twoHandedGrab = false;
        }

        protected static void LetGoWithoutHandle(ControllerTarget controllerTarget) {
            Rigidbody controllerRigidbody = controllerTarget.GetComponent<Rigidbody>();
            Rigidbody objRigidbody = RigidbodyDisabled.UnparentRigidbody(controllerRigidbody, controllerTarget.grabbedObject.transform);

            if (controllerRigidbody != null) {
                objRigidbody.velocity = controllerRigidbody.velocity;
                objRigidbody.angularVelocity = controllerRigidbody.angularVelocity;
            }
        }

        private static void LetGoRigidbody(ControllerTarget controllerTarget) {
            Debug.Log("LetGoRigidbody");

            controllerTarget.grabbedRigidbody = false;
        }

        #endregion

    }
}