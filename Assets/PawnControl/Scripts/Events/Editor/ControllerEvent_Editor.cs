﻿using UnityEditor;
using UnityEngine.Events;

namespace Passer {

    public class ControllerEvent_Editor : FloatEvent_Editor {

        public static void EventInspector(
            SerializedProperty eventHandlerProp, /*ControllerEventHandlers eventSource,*/
            ref int selectedEventIx, ref int selectedEventHandlerIx) {

            EventInspector(eventHandlerProp, ref selectedEventIx, ref selectedEventHandlerIx,
                EventMethodCheck, InitControllerEvent);

            SetParameterOnEvents(eventHandlerProp);
        }

        protected static void SetParameterOnEvents(SerializedProperty eventHandlersProp) {
            SerializedProperty defaultParameterPropertyProp = eventHandlersProp.FindPropertyRelative("defaultParameterProperty");
            string defaultParameterProperty = defaultParameterPropertyProp.stringValue;

            SerializedProperty eventsProp = eventHandlersProp.FindPropertyRelative("events");
            int eventCount = eventsProp.arraySize;
            for (int i = 0; i < eventCount; i++) {
                SerializedProperty eventHandlerProp = eventsProp.GetArrayElementAtIndex(i);
                SetParameterOnEvent(eventHandlerProp, defaultParameterProperty);
            }
        }

        protected static void SetParameterOnEvent(SerializedProperty eventHandlerProp, string defaultParameterProperty) {

            SerializedProperty parametersProp = eventHandlerProp.FindPropertyRelative("functionCall.parameters");
            int parameterCount = parametersProp.arraySize;
            if (parameterCount != 1)
                return; // no support for more than 1 parameter yet, 0 parameters: nothing to do

            SerializedProperty parameterProp = parametersProp.GetArrayElementAtIndex(0);
            SerializedProperty parameterTypeProp = parameterProp.FindPropertyRelative("type");
            SerializedProperty propertyNameProp = parameterProp.FindPropertyRelative("localProperty");
            if (propertyNameProp.stringValue == "") {
                switch ((FunctionCall.ParameterType)parameterTypeProp.intValue) {
                    case FunctionCall.ParameterType.Float:
                        propertyNameProp.stringValue = defaultParameterProperty;
                        break;
                }
            }
        }

        protected static void InitControllerEvent(SerializedProperty eventProp) {
            InitFloatEvent(eventProp);
        }

    }
}
