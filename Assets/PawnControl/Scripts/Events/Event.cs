﻿using System.Collections.Generic;
#if hNW_UNET
#endif

namespace Passer {

    /// <summary>An event for an input</summary>
    public abstract class EventHandler {

        public enum Type {
            Never,
            OnStart,
            OnEnd,
            WhileActive,
            WhileInactive,
            OnChange,
            Continuous
        }
        public Type eventType = Type.Continuous;

        public bool eventNetworking = false;

        public FunctionCall functionCall;

        public bool initialized;

        #region Bool Parameter

        protected bool _boolValue;
        public virtual bool boolValue {
            get { return _boolValue; }
            set {
                boolChanged = (value != _boolValue);
                if (!initialized) {
                    boolChanged = true;
                    initialized = true;
                }
                _boolValue = boolInverse ? !value : value;

                _floatValue = value ? 1 : 0;

                intChanged = boolChanged;
                _intValue = value ? 1 : 0;

                Update();
            }
        }
        protected bool boolChanged = true;
        /// <summary>Negate the boolean state before calling event trigger</summary>
        public bool boolInverse = false;

        #endregion

        #region Int Paramter

        protected int _intValue;
        protected bool intChanged;

        #endregion

        #region Float Parameter

        protected float _floatValue;
        protected bool floatChanged;

        #endregion

        #region Update Value

        virtual protected void Update() {
            if (functionCall.parameters == null || functionCall.parameters.Length == 0)
                UpdateVoid();
            else {
                switch (functionCall.parameters[0].type) {
                    case FunctionCall.ParameterType.Void:
                        UpdateVoid();
                        break;
                    case FunctionCall.ParameterType.Bool:
                        UpdateBool();
                        break;
                    case FunctionCall.ParameterType.Int:
                        UpdateInt();
                        break;
                    case FunctionCall.ParameterType.Float:
                        UpdateFloat();
                        break;
                    case FunctionCall.ParameterType.Vector3:
                        UpdateVector3();
                        break;
                    case FunctionCall.ParameterType.GameObject:
                        UpdateGameObject();
                        break;
                    case FunctionCall.ParameterType.Rigidbody:
                        UpdateRigidbody();
                        break;
                }
            }
        }

        virtual protected void UpdateVoid() {
            if (CheckCondition(boolValue, boolChanged, boolChanged)) {
                //FunctionCall.Networking networking = eventNetworking ? FunctionCall.Networking.Yes : FunctionCall.Networking.No;
                //functionCall.Execute(networking);
                functionCall.Execute();
            }
        }

        virtual protected void UpdateBool() {
            FunctionCall.Networking networking = eventNetworking ? FunctionCall.Networking.Yes : FunctionCall.Networking.No;
            if (CheckCondition(boolValue, boolChanged, boolChanged)) {
                if (functionCall.parameters[0].fromEvent)
                    functionCall.Execute(boolValue, networking);
                else
                    functionCall.Execute(functionCall.parameters[0].boolConstant, networking);
            }
        }

        virtual protected void UpdateInt() {
            if (CheckCondition(boolValue, boolChanged, intChanged)) {
                functionCall.Execute(functionCall.parameters[0].intConstant);
            }
        }

        virtual protected void UpdateFloat() {
            if (CheckCondition(boolValue, boolChanged, floatChanged)) {
                functionCall.Execute(functionCall.parameters[0].floatConstant);
            }
        }

        virtual protected void UpdateVector3() {
            if (CheckCondition(boolValue, boolChanged, true)) // valueChanged is not yet implemented
                functionCall.Execute(functionCall.parameters[0].vector3Constant);
        }

        protected virtual void UpdateGameObject() {
            if (CheckCondition(boolValue, boolChanged, true)) // valueChanged is not yet implemented
                functionCall.Execute(functionCall.parameters[0].gameObjectConstant);
        }

        protected virtual void UpdateRigidbody() {
            if (CheckCondition(boolValue, boolChanged, true)) // valueChanged is not yet implemented
                functionCall.Execute(functionCall.parameters[0].rigidbodyConstant);
        }

        #endregion

        protected bool CheckCondition(bool active, bool changed, bool valueChanged) {
            switch (eventType) {
                case Type.Never:
                    return false;
                case Type.WhileActive:
                    return active;
                case Type.WhileInactive:
                    return !active;
                case Type.OnStart:
                    return active && changed;
                case Type.OnEnd:
                    return !active && changed;
                case Type.OnChange:
                    return valueChanged;
                case Type.Continuous:
                default:
                    return true;
            }
        }
    }

    /// <summary>A list of events for an input</summary>
    /// For each input, one or more events can be defined when the input changes.
    [System.Serializable]
    public class EventHandlers<T> {
        public int id;
        public string label; ///< The label or name for the input
        public string tooltip;
        public string[] eventTypeLabels;
        public string fromEventLabel;
        public string fromEventLabelBool;
        public List<T> events = new List<T>(); ///< The eventlist for this input

        /// <summary>Gets the total number of evnets for all inputEventLists in the array</summary>
        public static int GetEventCount(EventHandlers<T>[] inputEvents) {
            int n = 0;
            foreach (EventHandlers<T> inputEvent in inputEvents) {
                n += inputEvent.events.Count;
            }
            return n;
        }
    }

}