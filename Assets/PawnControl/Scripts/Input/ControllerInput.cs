﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Passer {
#if pHUMANOID
    using Humanoid;
#endif

    /// <summary>A unified method for using controller input</summary>
    /// ControllerInput can be used to access the state and change of input buttons, touchpads and thumbsticks
    /// on any kind of input controller including game controllers and VR/XR controllers.
    public class ControllerInput : MonoBehaviour {

        public enum SideButton {
            StickVertical,
            StickHorizontal,
            StickButton,
            Button1,
            Button2,
            Button3,
            Button4,
            Trigger1,
            Trigger2,
            Option,
        }

#if pHUMANOID
        public HumanoidControl humanoid;
        protected HandTarget leftHandTarget;
        protected HandTarget rightHandTarget;

        public bool fingerMovements = true;
#endif

        public float leftStickVertical { get { return controller.left.stickVertical; } }
        public float leftStickHorizontal { get { return controller.left.stickHorizontal; } }

        public float leftTrigger1 { get { return controller.left.trigger1; } }
        public bool leftTrigger1Touched { get { return controller.left.trigger1 > 0; } }
        public bool leftTrigger1Pressed { get { return controller.left.trigger1 > 0.8F; } }

        public float rightStickVertical {  get { return controller.right.stickVertical; } }
        public float rightStickHorizontal {  get { return controller.right.stickHorizontal; } }

        public float rightTrigger1 { get { return controller.right.trigger1; } }
        public bool rightTrigger1Touched { get { return controller.right.trigger1 > 0; } }
        public bool rightTrigger1Pressed { get { return controller.right.trigger1 > 0.8F; } }

        protected static string[] eventTypeLabels = new string[] {
                "Never",
                "On Press",
                "On Release",
                "While Down",
                "While Up",
                "On Change",
                "Continuous"
        };

        public ControllerEventHandlers[] leftInputEvents = new ControllerEventHandlers[] {
            new ControllerEventHandlers() { label = "Left Vertical", id = 0, defaultParameterProperty = "leftStickVertical" },
            new ControllerEventHandlers() { label = "Left Horizontal", id = 1, defaultParameterProperty = "leftStickHorizontal" },
            new ControllerEventHandlers() { label = "Left Stick Button", id = 2, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Button 1", id = 3, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Button 2", id = 4, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Button 3", id = 5, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Button 4", id = 6, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Trigger 1", id = 7, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Trigger 2", id = 8, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Left Option", id = 9, eventTypeLabels = eventTypeLabels },
        };
        public ControllerEventHandlers[] rightInputEvents = new ControllerEventHandlers[] {
            new ControllerEventHandlers() { label = "Right Vertical", id = 0, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Horizontal", id = 1, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Stick Button", id = 2, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Button 1", id = 3, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Button 2", id = 4, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Button 3", id = 5, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Button 4", id = 6, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Trigger 1", id = 7, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Trigger 2", id = 8, eventTypeLabels = eventTypeLabels },
            new ControllerEventHandlers() { label = "Right Option", id = 9, eventTypeLabels = eventTypeLabels },
        };

        public ControllerEventHandlers[] mouseInputEvents = new ControllerEventHandlers[] {
            new ControllerEventHandlers() { label = "Mouse Vertical", id = 0 },
            new ControllerEventHandlers() { label = "Mouse Horizontal", id = 1 },
            new ControllerEventHandlers() { label = "Mouse Scroll", id = 2 },
            new ControllerEventHandlers() { label = "Left Button", id = 3 },
            new ControllerEventHandlers() { label = "Middle button", id = 4 },
            new ControllerEventHandlers() { label = "Right Button", id = 5 },
        };

        //public List<ControllerEventHandlers>[] eventHandlers = new List<ControllerEventHandlers>[20];

        protected Controller controller;

        public ControllerEventHandlers GetInputEvent(bool isLeft, SideButton controllerButton) {
            if (isLeft)
                return leftInputEvents[(int)controllerButton];
            else
                return rightInputEvents[(int)controllerButton];
        }

        public void Awake() {
#if pHUMANOID
            humanoid = GetComponent<HumanoidControl>();
            if (humanoid != null) {
                leftHandTarget = humanoid.leftHandTarget;
                rightHandTarget = humanoid.rightHandTarget;
            }
#endif
        }

        void Start() {
            controller = Controllers.GetController(0);

            //for (int i = 0; i < 10; i++) {
            //    eventHandlers[i] = new List<ControllerEventHandlers>();
            //    eventHandlers[i].Insert(0, leftInputEvents[i]);
            //}
            //for (int i = 10; i < 20; i++) {
            //    eventHandlers[i] = new List<ControllerEventHandlers>();
            //    eventHandlers[i].Insert(0, rightInputEvents[i - 10]);
            //}

        }

        protected virtual void Update() {
            UpdateInputList(leftInputEvents[0], controller.left.stickVertical);
            UpdateInputList(leftInputEvents[1], controller.left.stickHorizontal);
            UpdateInputList(leftInputEvents[2], (controller.left.stickTouch ? 0 : -1) + (controller.left.stickButton ? 1 : 0));
            UpdateInputList(leftInputEvents[3], controller.left.buttons[0] ? 1 : 0);
            UpdateInputList(leftInputEvents[4], controller.left.buttons[1] ? 1 : 0);
            UpdateInputList(leftInputEvents[5], controller.left.buttons[2] ? 1 : 0);
            UpdateInputList(leftInputEvents[6], controller.left.buttons[3] ? 1 : 0);
            UpdateInputList(leftInputEvents[7], controller.left.trigger1);
            UpdateInputList(leftInputEvents[8], controller.left.trigger2);
            UpdateInputList(leftInputEvents[9], controller.left.option ? 1 : 0);

            UpdateInputList(rightInputEvents[0], controller.right.stickVertical);
            UpdateInputList(rightInputEvents[1], controller.right.stickHorizontal);
            UpdateInputList(rightInputEvents[2], (controller.right.stickTouch ? 0 : -1) + (controller.right.stickButton ? 1 : 0));
            UpdateInputList(rightInputEvents[3], controller.right.buttons[0] ? 1 : 0);
            UpdateInputList(rightInputEvents[4], controller.right.buttons[1] ? 1 : 0);
            UpdateInputList(rightInputEvents[5], controller.right.buttons[2] ? 1 : 0);
            UpdateInputList(rightInputEvents[6], controller.right.buttons[3] ? 1 : 0);
            UpdateInputList(rightInputEvents[7], controller.right.trigger1);
            UpdateInputList(rightInputEvents[8], controller.right.trigger2);
            UpdateInputList(rightInputEvents[9], controller.right.option ? 1 : 0);

            mouseInputEvents[0].floatValue -= Input.GetAxis("Mouse Y");
            mouseInputEvents[1].floatValue += Input.GetAxis("Mouse X");
            mouseInputEvents[2].floatValue += Input.GetAxis("Mouse ScrollWheel");
            mouseInputEvents[3].floatValue = Input.GetMouseButton(0) ? 1 : 0;
            mouseInputEvents[4].floatValue = Input.GetMouseButton(1) ? 1 : 0;
            mouseInputEvents[5].floatValue = Input.GetMouseButton(2) ? 1 : 0;

#if pHUMANOID
            if (fingerMovements)
                UpdateFingerMovements();
#endif
        }

        protected void UpdateInputList(ControllerEventHandlers inputEventList, float value) {
            foreach (ControllerEventHandler inputEvent in inputEventList.events) {
                if (inputEvent.eventType == EventHandler.Type.WhileActive || inputEvent.eventType == EventHandler.Type.WhileInactive)
                    inputEvent.floatValue2 = value;
                else
                    inputEvent.floatValue = value;
            }
        }

        public void testFloat(float x) {
            Debug.Log("Float " + x);
        }

        public void testInt(int x) {
            Debug.Log("Int " + x);
        }

        public void testBool(bool b) {
            Debug.Log("Bool " + b);
        }

        public void SetEventHandler(bool isLeft, SideButton sideButton, UnityAction<bool> boolEvent) {
            ControllerEventHandlers eventHandlers = GetInputEvent(isLeft, sideButton);
            SetBoolMethod(gameObject, eventHandlers, EventHandler.Type.OnChange, boolEvent);

        }

        public static void SetBoolMethod(GameObject gameObject, ControllerEventHandlers eventHandlers, EventHandler.Type eventType, UnityAction<bool> boolEvent) {
            Object target = (Object)boolEvent.Target;
            string methodName = boolEvent.Method.Name;
            methodName = target.GetType().Name + "/" + methodName;

            if (eventHandlers.events == null || eventHandlers.events.Count == 0)
                eventHandlers.events.Add(new ControllerEventHandler(gameObject, eventType));
            else
                eventHandlers.events[0].eventType = eventType;

            ControllerEventHandler eventHandler = eventHandlers.events[0];
            //eventHandler.functionCall.target = target;
            eventHandler.functionCall.targetGameObject = FunctionCall.GetGameObject(target);
            eventHandler.functionCall.methodName = methodName;
            eventHandler.functionCall.SetParameterType(FunctionCall.ParameterType.Bool);
            eventHandler.functionCall.parameters[0].localProperty = "From Event";
            eventHandler.functionCall.parameters[0].fromEvent = true;
        }

#if pHUMANOID
        protected void UpdateFingerMovements() {
            if (leftHandTarget != null)
                UpdateFingerMovementsSide(leftHandTarget.fingers, controller.left);
            if (rightHandTarget != null)
                UpdateFingerMovementsSide(rightHandTarget.fingers, controller.right);
        }

        protected void UpdateFingerMovementsSide(FingersTarget fingers, ControllerSide controllerSide) {
            //float thumbCurl = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);
            //fingers.thumb.curl = controllerSide.stickTouch ? thumbCurl + 0.3F : thumbCurl;
            //fingers.index.curl = controllerSide.trigger1;
            //fingers.middle.curl = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);
            //fingers.ring.curl = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);
            //fingers.little.curl = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);

            float thumbCurl = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);
            fingers.thumb.curl = !controllerSide.stickTouch ? -0.5F : thumbCurl;

            float indexValue = controllerSide.trigger1;
            SetFingerCurl(fingers.index, indexValue);

            float fingersValue = Mathf.Max(controllerSide.trigger2, controllerSide.trigger1);
            SetFingerCurl(fingers.middle, fingersValue);
            SetFingerCurl(fingers.ring, fingersValue);
            SetFingerCurl(fingers.little, fingersValue);
        }

        private void SetFingerCurl(FingersTarget.TargetedFinger finger, float inputValue) {
            if (inputValue < 0)
                finger.curl = 0.1F * inputValue;
            else
                finger.curl = inputValue;
        }

#endif
    }
}