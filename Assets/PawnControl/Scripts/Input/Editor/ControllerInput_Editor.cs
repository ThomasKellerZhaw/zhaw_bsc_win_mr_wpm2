﻿using UnityEditor;
using UnityEngine;

namespace Passer {

    [CustomEditor(typeof(ControllerInput))]
    public class ControllerInput_Editor : Editor {
        protected ControllerInput controllerInput;

#if pHUMANOID
        protected Humanoid.HumanoidControl humanoid;
#endif
        protected PawnControl pawn;

        //        public enum ControllerType {
        //            GenericController = 0,
        //            Xbox = 1,
        //            //PS4 = 2,
        //            //Steelseries = 3,
        //#if hOPENVR
        //            SteamVR = 4,
        //#endif
        //            Oculus = 5,
        //#if hWINDOWSMR
        //            WindowsMR = 7,
        //#endif
        //#if hHYDRA
        //            RazerHydra = 8,
        //#endif
        //            Keyboard = 9,
        //            Mouse = 10
        //        }
        //        public static ControllerType viewControllerType = ControllerType.GenericController;

        //        protected ControllerType PreselectControllerType(GameControllers gameController) {
        //            switch (gameController) {
        //                case GameControllers.Xbox:
        //                    return ControllerType.Xbox;
        //                case GameControllers.Oculus:
        //                    return ControllerType.Oculus;
        //            }
        //            return viewControllerType;
        //        }

        #region Enable

        protected virtual void OnEnable() {
            controllerInput = (ControllerInput)target;

            for (int i = 0; i < controllerInput.leftInputEvents.Length; i++) {
                if (controllerInput.leftInputEvents[i].events == null || controllerInput.leftInputEvents[i].events.Count == 0) {
                    controllerInput.leftInputEvents[i].events.Add(new ControllerEventHandler(controllerInput.gameObject, EventHandler.Type.Never));
                }
            }
            for (int i = 0; i < controllerInput.rightInputEvents.Length; i++) {
                if (controllerInput.rightInputEvents[i].events == null || controllerInput.rightInputEvents[i].events.Count == 0) {
                    controllerInput.rightInputEvents[i].events.Add(new ControllerEventHandler(controllerInput.gameObject, EventHandler.Type.Never));
                }
            }

            pawn = controllerInput.GetComponent<PawnControl>();
#if pHUMANOID
            humanoid = controllerInput.GetComponent<Humanoid.HumanoidControl>();
#endif
            //if (pawn != null) {
            //    viewControllerType = PreselectControllerType(pawn.gameController);
            //}

            InitController();
            InitMouse();
        }

        #endregion

        #region Disable

        protected virtual void OnDisable() {
            ControllerEventHandlers.Cleanup(controllerInput.leftInputEvents);
            ControllerEventHandlers.Cleanup(controllerInput.rightInputEvents);
        }

        #endregion

        #region Inspector
        protected int selectedLeft = -1;
        protected int selectedRight = -1;
        protected int selectedSub = -1;

        protected bool showLeft = true;
        protected bool showRight = true;

        public override void OnInspectorGUI() {
            serializedObject.Update();

            //viewControllerType = (ControllerType)EditorGUILayout.EnumPopup(viewControllerType);

            //switch (viewControllerType) {
            //    case ControllerType.Mouse:
            //        MouseInspector();
            //        break;
            //    case ControllerType.Keyboard:
            //        KeyboardInspector();
            //        break;
            //    default:
            ControllerInspector();
            //        break;
            //}

            serializedObject.ApplyModifiedProperties();
        }

        #region Controller
#if pHUMANOID
        protected SerializedProperty fingerMovementsProp;
#endif
        protected SerializedProperty leftEventsProp;
        protected SerializedProperty rightEventsProp;
        //string[] leftControllerLabels;
        //string[] rightControllerLabels;

        protected virtual void InitController() {
#if pHUMANOID
            fingerMovementsProp = serializedObject.FindProperty("fingerMovements");
#endif

            leftEventsProp = serializedObject.FindProperty("leftInputEvents");
            rightEventsProp = serializedObject.FindProperty("rightInputEvents");
        }

        protected virtual void ControllerInspector() {
#if pHUMANOID
            GUIContent text = new GUIContent(
                "Finger Movements",
                "Implements finger movements using controller input"
                );
            fingerMovementsProp.boolValue = EditorGUILayout.Toggle(text, fingerMovementsProp.boolValue);
#endif
            //            GameControllers gameController = GameControllers.Generic;
            //            if (pawn != null)
            //                gameController = pawn.gameController;
            //#if pHUMANOID
            //            if (humanoid != null)
            //                gameController = humanoid.gameController;
            //#endif

            showLeft = EditorGUILayout.Foldout(showLeft, "Left", true);
            if (showLeft) {
                for (int i = 0; i < controllerInput.leftInputEvents.Length; i++) {
                    ControllerEvent_Editor.EventInspector(
                        leftEventsProp.GetArrayElementAtIndex(i), /*controllerInput.leftInputEvents[i],*/
                        ref selectedLeft, ref selectedSub
                        );
                }
                if (selectedLeft >= 0)
                    selectedRight = -1;
            }

            showRight = EditorGUILayout.Foldout(showRight, "Right");
            if (showRight) {
                for (int i = 0; i < controllerInput.rightInputEvents.Length; i++) {
                    ControllerEvent_Editor.EventInspector(
                        rightEventsProp.GetArrayElementAtIndex(i), /*controllerInput.rightInputEvents[i],*/
                        ref selectedRight, ref selectedSub
                        );
                }
                if (selectedRight >= 0)
                    selectedLeft = -1;
            }
        }

        #endregion

        #region Keyboard

        protected virtual void KeyboardInspector() {

        }

        #endregion

        #region Mouse
        protected SerializedProperty mouseEventsProp;
        protected string[] mouseLabelList;

        protected void InitMouse() {
            mouseEventsProp = serializedObject.FindProperty("mouseInputEvents");
            mouseLabelList = new string[] {
                "Mouse Vertical",
                "Mouse Horizontal",
                "Mouse Scroll",
                "Left Button",
                "Middle button",
                "Right Button",
            };
        }

        protected int selectedMouse = -1;

        protected virtual void MouseInspector() {
            //ControllerEvent_Editor.ControllerEventsInspector(mouseEventsProp, controllerInput.mouseInputEvents, mouseLabelList, pawn.gameController, ref selectedMouse, ref selectedSub);
        }
        #endregion

        #endregion
    }

    [InitializeOnLoad]
    class InputManager {
        static InputManager() {
            EnforceInputManagerBindings();
        }

        private static void EnforceInputManagerBindings() {
            try {
                BindAxis(new Axis() { name = "Axis 3", axis = 2, });
                BindAxis(new Axis() { name = "Axis 4", axis = 3, });
                BindAxis(new Axis() { name = "Axis 5", axis = 4, });
                BindAxis(new Axis() { name = "Axis 6", axis = 5, });
                BindAxis(new Axis() { name = "Axis 7", axis = 6, });
                BindAxis(new Axis() { name = "Axis 8", axis = 7, });
                BindAxis(new Axis() { name = "Axis 9", axis = 8, });
                BindAxis(new Axis() { name = "Axis 10", axis = 9, });
                BindAxis(new Axis() { name = "Axis 11", axis = 10, });
                BindAxis(new Axis() { name = "Axis 12", axis = 11, });
                BindAxis(new Axis() { name = "Axis 13", axis = 12, });
            }
            catch {
                Debug.LogError("Failed to apply Humanoid input manager bindings.");
            }
        }

        private class Axis {
            public string name = System.String.Empty;
            public string descriptiveName = System.String.Empty;
            public string descriptiveNegativeName = System.String.Empty;
            public string negativeButton = System.String.Empty;
            public string positiveButton = System.String.Empty;
            public string altNegativeButton = System.String.Empty;
            public string altPositiveButton = System.String.Empty;
            public float gravity = 0.0f;
            public float dead = 0.001f;
            public float sensitivity = 1.0f;
            public bool snap = false;
            public bool invert = false;
            public int type = 2;
            public int axis = 0;
            public int joyNum = 0;
        }

        private static void BindAxis(Axis axis) {
            SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

            SerializedProperty axisIter = axesProperty.Copy();
            axisIter.Next(true);
            axisIter.Next(true);
            while (axisIter.Next(false)) {
                if (axisIter.FindPropertyRelative("m_Name").stringValue == axis.name) {
                    // Axis already exists. Don't create binding.
                    return;
                }
            }

            axesProperty.arraySize++;
            serializedObject.ApplyModifiedProperties();

            SerializedProperty axisProperty = axesProperty.GetArrayElementAtIndex(axesProperty.arraySize - 1);
            axisProperty.FindPropertyRelative("m_Name").stringValue = axis.name;
            axisProperty.FindPropertyRelative("descriptiveName").stringValue = axis.descriptiveName;
            axisProperty.FindPropertyRelative("descriptiveNegativeName").stringValue = axis.descriptiveNegativeName;
            axisProperty.FindPropertyRelative("negativeButton").stringValue = axis.negativeButton;
            axisProperty.FindPropertyRelative("positiveButton").stringValue = axis.positiveButton;
            axisProperty.FindPropertyRelative("altNegativeButton").stringValue = axis.altNegativeButton;
            axisProperty.FindPropertyRelative("altPositiveButton").stringValue = axis.altPositiveButton;
            axisProperty.FindPropertyRelative("gravity").floatValue = axis.gravity;
            axisProperty.FindPropertyRelative("dead").floatValue = axis.dead;
            axisProperty.FindPropertyRelative("sensitivity").floatValue = axis.sensitivity;
            axisProperty.FindPropertyRelative("snap").boolValue = axis.snap;
            axisProperty.FindPropertyRelative("invert").boolValue = axis.invert;
            axisProperty.FindPropertyRelative("type").intValue = axis.type;
            axisProperty.FindPropertyRelative("axis").intValue = axis.axis;
            axisProperty.FindPropertyRelative("joyNum").intValue = axis.joyNum;
            serializedObject.ApplyModifiedProperties();
        }
    }

}