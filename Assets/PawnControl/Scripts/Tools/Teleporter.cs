﻿using UnityEngine;

namespace Passer {
#if pHUMANOID
    using Humanoid;
#endif

    /// <summary>The teleporter is a simple tool to teleport transforms</summary>
    [HelpURLAttribute("https://passervr.com/documentation/humanoid-control/tools/teleport/")]
    public class Teleporter : InteractionPointer {
        ///  <summary>Determines how the Transform is moved to the Target Point.</summary>
        public enum TransportType {
            Teleport,   //< Direct placement on the target point
            Dash        //< A quick movement in a short time from the originating point to the target point
        }

        /// <summary>The TransportType to use when teleporting.</summary>
        public TransportType transportType = TransportType.Teleport;
        /// <summary>The transform which will be teleported</summary>
        public Transform transformToTeleport;

        /// <summary>Teleport the transform</summary>
        public virtual void TeleportTransform() {
#if pHUMANOID
            HumanoidControl humanoid = transformToTeleport.GetComponent<HumanoidControl>();
            if (humanoid == null)
#endif
                transformToTeleport.Teleport(focusPointObj.transform.position);
#if pHUMANOID
            else {
                Vector3 interactionPointerPosition = humanoid.GetHumanoidPosition() - transformToTeleport.position;

                switch (transportType) {
                    case TransportType.Teleport:
                        transformToTeleport.Teleport(focusPointObj.transform.position - interactionPointerPosition);
                        break;
                    case TransportType.Dash:
                        StartCoroutine(TransformMovements.DashCoroutine(transformToTeleport, focusPointObj.transform.position - interactionPointerPosition));
                        break;
                    default:
                        break;
                }
            }
#endif
        }

        /// <summary>Adds a default Teleporter to the transform</summary>
        /// <param name="parentTransform">The transform to which the Teleporter will be added</param>
        /// <param name="pointerType">The interaction pointer type for the Teleporter</param>
        public static new Teleporter Add(Transform parentTransform, PointerType pointerType = PointerType.Ray) {
            GameObject pointerObj = new GameObject("Teleporter");
            pointerObj.transform.SetParent(parentTransform, false);

            GameObject destinationObj = new GameObject("Destination");
            destinationObj.transform.SetParent(pointerObj.transform);
            destinationObj.transform.localPosition = Vector3.zero;
            destinationObj.transform.localRotation = Quaternion.identity;

            if (pointerType == PointerType.FocusPoint) {
                GameObject focusPointSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                focusPointSphere.transform.SetParent(destinationObj.transform);
                focusPointSphere.transform.localPosition = Vector3.zero;
                focusPointSphere.transform.localRotation = Quaternion.identity;
                focusPointSphere.transform.localScale = Vector3.one * 0.1F;
            }
            else {
                LineRenderer pointerRay = destinationObj.AddComponent<LineRenderer>();
                pointerRay.startWidth = 0.01F;
                pointerRay.endWidth = 0.01F;
                pointerRay.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                pointerRay.receiveShadows = false;
                pointerRay.useWorldSpace = false;
            }

            Teleporter teleporter = pointerObj.AddComponent<Teleporter>();
            teleporter.focusPointObj = destinationObj;
            teleporter.rayType = RayType.Bezier;

            return teleporter;
        }

        public override void Click(bool clicking) {
            if (clicking)
                TeleportTransform();
        }
    }
}
