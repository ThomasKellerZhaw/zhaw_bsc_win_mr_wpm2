﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Passer {

    /// <summary>
    /// Component for spawning objects
    /// </summary>
    public class Spawner : SpawnPoint {

        /// <summary>
        /// The prefab which will be spawned
        /// </summary>
        public GameObject prefab;

        /// <summary>
        /// The available SpawnPoints.
        /// </summary>
        /// When the list is empty, the scene will be searched for available spawn points when it becomes enabled.
        /// If no spawn points can be found, this transform will be used as a SpawnPoint
        public SpawnPoint[] spawnPoints;

        /// <summary>
        /// The SpawnMethod to use for spawning the humanoids.
        /// </summary>
        public SpawnMethod spawnMethod;
        /// <summary>
        /// Spawning methods.
        /// </summary>
        public enum SpawnMethod {
            SinglePlayer,   ///< Only one humanoid will be spawned. It will be located at the first SpawnPoint.
            Random,         ///< A SpawnPoint is chosen randomly.
            RoundRobin      ///< Spawn points are chosen in round robin manner.
        }
        protected int spawnIndex = 0;

        /// <summary>
        /// Spawn an object when the scene starts
        /// </summary>
        public bool spawnAtStart;

        protected virtual void OnEnable() {
            if (spawnPoints != null && spawnPoints.Length > 0)
                return;

            spawnPoints = FindObjectsOfType<SpawnPoint>();
            if (spawnPoints.Length == 0) {
                SpawnPoint thisSpawnPoint = this.gameObject.AddComponent<SpawnPoint>();
                spawnPoints = new SpawnPoint[] { thisSpawnPoint };
            }
        }

        protected virtual void Start() {
            if (spawnAtStart)
                SpawnObject();
        }

        /// <summary>
        /// Spawn one GameObject using the Spawner settings. 
        /// </summary>
        /// <returns>The instantiated GameObject. Will be null when the prefab could not be instantiated.</returns>
        public virtual GameObject SpawnObject() {
            if (prefab == null)
                return null;

            return Instantiate(prefab, transform.position, transform.rotation);
        }

        /// <summary>
        /// Spawn a GameObject
        /// </summary>
        /// <param name="prefab">The GameObject prefab to spawn.</param>
        /// <returns>The instantiated GameObject. Will be null when the prefab could not be spawned.</returns>
        public virtual GameObject Spawn(GameObject prefab) {
            return Spawn(prefab, spawnPoints, spawnMethod);
        }

        /// <summary>
        /// Spawn a GameObject
        /// </summary>
        /// <param name="prefab">The GameObject prefab to spawn.</param>
        /// <param name="spawnPoints">The array of possible spawn points.</param>
        /// <param name="spawnMethod">The SpawnMethod to use for spawning.</param>
        /// <returns>The instantiated GameObject. Will be null when the prefab could not be spawned.</returns>
        public virtual GameObject Spawn(GameObject prefab, SpawnPoint[] spawnPoints, SpawnMethod spawnMethod = SpawnMethod.RoundRobin) {
            if (prefab == null)
                return null;

            GameObject newGameObject = null;
            SpawnPoint spawnPoint = ChooseSpawnPoint(spawnPoints, spawnMethod);
            if (spawnPoint == null)
                Debug.Log("Could not find an empty SpawnPoint");
            else {
                NetworkedTransform networkedTransform = prefab.GetComponent<NetworkedTransform>();
                if (networkedTransform != null)
                    newGameObject = NetworkedTransform.Instantiate(prefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
                else
                    newGameObject = Instantiate(prefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }

            return newGameObject;
        }

        protected SpawnPoint ChooseSpawnPoint(SpawnPoint[] spawnPoints, SpawnMethod spawnMethod) {
            if (spawnPoints == null)
                return null;

            List<SpawnPoint> availablePoints = new List<SpawnPoint>();
            foreach (SpawnPoint spawnPoint in spawnPoints) {
                if (CheckSpawnPointIsFree(spawnPoint.transform.position))
                    availablePoints.Add(spawnPoint);
            }

            int spawnPointIndex = GetSpawnPointIndex(availablePoints.Count, spawnMethod);
            if (spawnPointIndex == -1)
                return null;

            return availablePoints[spawnPointIndex];
        }

        private int GetSpawnPointIndex(int nSpawnPoints, SpawnMethod spawnMethod) {
            if (nSpawnPoints <= 0)
                return -1;

            switch (spawnMethod) {
                case SpawnMethod.RoundRobin:
                    return spawnIndex++ & nSpawnPoints;

                case SpawnMethod.Random:
                default:
                    return Random.Range(0, nSpawnPoints - 1);
            }
        }

        //private int GetSpawnPointIndex(SpawnPoint[] spawnPoints, SpawnMethod spawnMethod) {
        //    if (spawnPoints.Length <= 0)
        //        return -1;

        //    switch (spawnMethod) {
        //        case SpawnMethod.RoundRobin:
        //            return spawnIndex++ & spawnPoints.Length;

        //        case SpawnMethod.Random:
        //        default:
        //            return Random.Range(0, spawnPoints.Length - 1);
        //    }
        //}

        protected static bool CheckSpawnPointIsFree(Vector3 location) {
            return !Physics.CheckCapsule(location + Vector3.up * 0.3F, location + Vector3.up * 2, 0.2F);
        }

    }

}