﻿using System;
using System.Reflection;
using UnityEngine;

namespace Passer {

    [Serializable]
    public class FunctionCall {
        public GameObject targetGameObject;
        public string methodName;

        protected NetworkObject networkObject;

        protected Delegate targetDelegate;

        protected delegate void Method();
        protected delegate void MethodBool(bool value);
        protected delegate void MethodFloat(float value);
        protected delegate void MethodInt(int value);
        protected delegate void MethodVector3(Vector3 value);
        protected delegate void MethodGameObject(GameObject value);
        protected delegate void MethodRigidbody(Rigidbody value);

        public enum ParameterType {
            Void,
            Float,
            Int,
            Bool,
            Vector3,
            GameObject,
            Rigidbody,
        }
        public static Type ToSystemType(ParameterType parameterType) {
            switch (parameterType) {
                default:
                case ParameterType.Void:
                    return typeof(void);
                case ParameterType.Bool:
                    return typeof(bool);
                case ParameterType.Int:
                    return typeof(int);
                case ParameterType.Float:
                    return typeof(float);
                case ParameterType.Vector3:
                    return typeof(Vector3);
                case ParameterType.GameObject:
                    return typeof(GameObject);
                case ParameterType.Rigidbody:
                    return typeof(Rigidbody);
            }
        }

        [Serializable]
        public class Parameter {
            public bool fromEvent = false;
            public string localProperty;
            public ParameterType type;
            public float floatConstant;
            public int intConstant;
            public bool boolConstant;
            public Vector3 vector3Constant;
            public GameObject gameObjectConstant;
            public Rigidbody rigidbodyConstant;
        }
        public Parameter[] parameters;

        public class FloatParameter : Parameter {
            public float newFloatConstant;
        }
        public class IntParameter : Parameter {
            public int newIntConstant;
        }

        public void SetParameterType(ParameterType parameterType, int parameterIx = 0) {
            Debug.Log("SetParameterType " + parameterType);
            if (parameters == null || parameters.Length == 0)
                parameters = new Parameter[parameterIx + 1];
            if (parameterIx < 0 || parameterIx >= parameters.Length)
                return;

            if (parameters[parameterIx] == null)
                parameters[parameterIx] = new Parameter();

            parameters[parameterIx].type = parameterType;
        }

        public static void Execute(GameObject target, string methodName) {
            Component component = GetComponent(target, methodName);
            Method method = CreateMethod(component, methodName);
            method();
        }

        public enum Networking {
            No,
            Yes
        }

        public virtual void Execute(Networking networking = Networking.No) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((Method)targetDelegate)();

#if hNW_UNET
            if (networking == Networking.Yes) {
                if (networkObject == null)
                    networkObject = NetworkObject.GetNetworkObject(this);
                if (networkObject != null)
                    networkObject.RPC(this);
            }
#endif
        }

        public static void Execute(GameObject target, string methodName, bool boolValue) {
            Component component = GetComponent(target, methodName);
            MethodBool method = CreateMethodBool(component, methodName);
            method(boolValue);
        }

        public void Execute(bool value, Networking networking = Networking.No) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodBool)targetDelegate)(value);


#if hNW_UNET
            if (networking == Networking.Yes) {
                if (networkObject == null)
                    networkObject = NetworkObject.GetNetworkObject(this);
                if (networkObject != null)
                    networkObject.RPC(this, value);
            }
#endif
        }

        public void Execute(int value) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodInt)targetDelegate)(value);

        }

        public virtual void Execute(float value) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodFloat)targetDelegate)(value);
        }

        public void Execute(Vector3 value) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodVector3)targetDelegate)(value);
        }

        public void Execute(GameObject value) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodGameObject)targetDelegate)(value);
        }

        protected void Execute(Rigidbody value) {
            if (targetDelegate == null) {
                GetTargetMethod();
                if (targetDelegate == null)
                    return;
            }
            ((MethodRigidbody)targetDelegate)(value);
        }

        public virtual void GetTargetMethod() {
            if (targetGameObject == null)
                return;

            string localMethodName;
            Component targetComponent = GetComponent(targetGameObject, methodName, out localMethodName);
            if (targetComponent is Script) {
                Script script = (Script)targetComponent;
                targetDelegate = (Method)(() => script.Execute());
                return;
            }

            if (parameters == null || parameters.Length == 0) {
                targetDelegate = CreateMethod(targetGameObject, methodName);
                return;
            }

            switch (parameters[0].type) {
                case ParameterType.Void:
                    targetDelegate = CreateMethod(targetGameObject, methodName);
                    break;
                case ParameterType.Bool:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].boolConstant);
                    break;
                case ParameterType.Int:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].intConstant);
                    break;
                case ParameterType.Float:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].floatConstant);
                    break;
                case ParameterType.Vector3:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].vector3Constant);
                    break;
                case ParameterType.GameObject:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].gameObjectConstant);
                    break;
                case ParameterType.Rigidbody:
                    CreateTargetMethod(targetComponent, localMethodName, parameters[0].rigidbodyConstant);
                    break;
                default:
                    return;

            }
        }

        protected static Component GetComponent(GameObject target, string fullMethodName) {
            string methodName;
            return GetComponent(target, fullMethodName, out methodName);
        }

        protected static Component GetComponent(GameObject target, string fullMethodName, out string methodName) {
            methodName = fullMethodName;
            string componentName = "";
            int slashPos = methodName.LastIndexOf("/");
            if (slashPos >= 0) {
                methodName = methodName.Substring(slashPos + 1);
                componentName = fullMethodName.Substring(0, slashPos);
            }
            if (componentName == "")
                return null;

            return target.GetComponent(componentName);
        }

        public static GameObject GetGameObject(UnityEngine.Object obj) {
            if (obj is GameObject)
                return (GameObject)obj;
            else if (obj is Component)
                return ((Component)obj).gameObject;
            else
                return null;
        }

        protected static Method CreateMethod(GameObject target, string fullMethodName) {
            string methodName;
            Component component = GetComponent(target, fullMethodName, out methodName);
            if (component == null)
                return null;

            Method method = CreateMethod(component, methodName);
            return method;
        }

        protected static Method CreateMethod(Component target, string methodName) {
            if (target is Script) {
                Script script = (Script)target;
                Method targetMethod = () => script.Execute();
                return targetMethod;
            }
            else {
                Type targetComponentType = target.GetType();
                MethodInfo methodInfo = targetComponentType.GetMethod(methodName, new Type[] { });
                if (methodInfo == null)
                    return null;

                Method targetMethod = (Method)Delegate.CreateDelegate(typeof(Method), target, methodInfo);
                return targetMethod;
            }
        }

        protected static MethodBool CreateMethod(GameObject target, string fullMethodName, bool boolConstant) {
            string methodName;
            Component component = GetComponent(target, fullMethodName, out methodName);
            MethodBool method = CreateMethodBool(component, methodName);
            return method;
        }

        protected static MethodBool CreateMethodBool(Component target, string methodName) {
            if (target is Script) {
                //Script script = (Script)target;
                // Scripts do not yet support parameters
                //MethodBool targetMethod = (bool x) => script.Execute(x);
                //return targetMethod;
                return null;
            }
            else {
                Type targetComponentType = target.GetType();
                MethodInfo methodInfo = targetComponentType.GetMethod(methodName, new Type[] { });
                if (methodInfo == null)
                    return null;

                MethodBool targetMethod = (MethodBool)Delegate.CreateDelegate(typeof(MethodBool), target, methodInfo);
                return targetMethod;
            }
        }

        protected void CreateTargetMethod(Component target, string methodName, bool boolConstant) {
            Type targetType = target.GetType();
            MethodInfo methodInfo = targetType.GetMethod(methodName, new Type[] { typeof(bool) });
            if (methodInfo != null) {
                targetDelegate = (MethodBool)Delegate.CreateDelegate(typeof(MethodBool), target, methodInfo);
            }
        }

        protected void CreateTargetMethod(Component target, string methodName, int intConstant) {
            Type targetType = target.GetType();
            MethodInfo methodInfo = targetType.GetMethod(methodName, new Type[] { typeof(int) });
            if (methodInfo != null) {
                targetDelegate = (MethodInt)Delegate.CreateDelegate(typeof(MethodInt), target, methodInfo);
            }
        }

        protected void CreateTargetMethod(Component target, string methodName, float floatConstant) {
            Type targetType = target.GetType();
            MethodInfo methodInfo = targetType.GetMethod(methodName, new Type[] { typeof(float) });
            if (methodInfo != null) {
                targetDelegate = (MethodFloat)Delegate.CreateDelegate(typeof(MethodFloat), target, methodInfo);
            }
        }

        protected void CreateTargetMethod(Component target, string methodName, Vector3 vectorConstant) {
            Type targetType = target.GetType();
            MethodInfo methodInfo = targetType.GetMethod(methodName, new Type[] { typeof(Vector3) });
            if (methodInfo != null) {
                targetDelegate = (MethodVector3)Delegate.CreateDelegate(typeof(MethodVector3), target, methodInfo);
            }
        }

        protected void CreateTargetMethod(Component target, string methodName, Rigidbody rigidbodyConstant) {
            Type targetType = target.GetType();
            MethodInfo methodInfo = targetType.GetMethod(methodName, new Type[] { typeof(Rigidbody) });
            if (methodInfo != null) {
                targetDelegate = (MethodRigidbody)Delegate.CreateDelegate(typeof(MethodRigidbody), target, methodInfo);
            }
        }

    }

}