﻿using UnityEngine;

namespace Passer {

    public class CollisionEventHandler : MonoBehaviour {

        #region Events

        public GameObjectEventList collisionEvent = new GameObjectEventList() {
            label = "Collision Event",
            tooltip =
                "Call functions using the collider state\n" +
                "Parameter: the GameObject colliding with the collider",
            eventTypeLabels = new string[] {
                "Never",
                "On Collision Start",
                "On Collision End",
                "While Colliding",
                "While not Colliding",
                "On Collision Change",
                "Always"
            },
        };

        private void OnCollisionEnter(Collision collision) {
            collisionEvent.value = collision.gameObject;
        }

        private void OnCollisionExit(Collision collision) {
            collisionEvent.value = null;
        }

        #endregion

    }

}