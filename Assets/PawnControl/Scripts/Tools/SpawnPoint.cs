﻿using UnityEngine;

namespace Passer {

    /// <summary>
    /// A Transform represeting a place where objects can spawn
    /// </summary>
    public class SpawnPoint : MonoBehaviour {

        #region Gizmos

        private void OnDrawGizmos() {
            Gizmos.color = new Color(244F / 255F, 122F / 255F, 0);
            Gizmos.DrawCube(transform.position, new Vector3(0.2F, 0.01F, 0.2F));
            GizmosDrawCircle(transform.position, 0.2F);
        }

        private void GizmosDrawCircle(Vector3 position, float radius) {
            float theta = 0;
            float x = radius * Mathf.Cos(theta);
            float y = radius * Mathf.Sin(theta);
            Vector3 pos = position + new Vector3(x, 0, y);
            Vector3 newPos = pos;
            Vector3 lastPos = pos;
            for (theta = 0.1f; theta < Mathf.PI * 2; theta += 0.1f) {
                x = radius * Mathf.Cos(theta);
                y = radius * Mathf.Sin(theta);
                newPos = position + new Vector3(x, 0, y);
                Gizmos.DrawLine(pos, newPos);
                pos = newPos;
            }
            Gizmos.DrawLine(pos, lastPos);
        }
        #endregion  
    }
}
