﻿using UnityEngine;

namespace Passer {

    public class Counter : MonoBehaviour {
        [SerializeField]
        protected int _value;
        public int value {
            get { return _value; }
            set { _value = value; }
        }

        public int max = 10;
        public int min = 0;

        public void Decrement() {
            if (_value > min) {
                _value--;
                counterEvent.value = _value;
            }
        }

        public void Increment() {
            if (_value < max) {
                _value++;
                counterEvent.value = _value;
            }
        }

        #region Events

        public IntEventHandlers counterEvent = new IntEventHandlers() {
            label = "Value Change Event",
            tooltip = 
                "Call functions using counter values\n" +
                "Parameter: the counter value"   ,
            eventTypeLabels = new string[] {
                "Never",
                "On Min",
                "On Max",
                "While Min",
                "While Max",
                "When Changed",
                "Always"
            }
        };

        #endregion
    }

}