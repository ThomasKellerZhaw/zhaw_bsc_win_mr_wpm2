﻿using UnityEditor;

namespace Passer {

    [CustomEditor(typeof(CollisionEventHandler))]
    public class CollisionEventHandler_Editor : Editor {
        protected CollisionEventHandler eventHandler;

        #region Enable
        protected virtual void OnEnable() {
            eventHandler = (CollisionEventHandler)target;

            InitEvents();
        }
        #endregion

        #region Disable
        protected virtual void OnDisable() {
            CleanupEvents();
        }
        #endregion

        #region Inspector
        public override void OnInspectorGUI() {
            serializedObject.Update();

            EventsInspector();

            serializedObject.ApplyModifiedProperties();
        }

        #region Events

        protected SerializedProperty collisionEventProp;

        protected void InitEvents() {
            collisionEventProp = serializedObject.FindProperty("collisionEvent");
            eventHandler.collisionEvent.id = 0;
        }

        protected int selectedEventSource = -1;
        protected int selectedEvent;

        protected void EventsInspector() {
            GameObjectEvent_Editor.EventInspector(collisionEventProp, eventHandler.collisionEvent, ref selectedEventSource, ref selectedEvent);
            EditorGUILayout.Space();
        }

        protected void CleanupEvents() {
            GameObjectEvent_Editor.Cleanup(eventHandler.collisionEvent);
        }
        #endregion

        #endregion
    }

}